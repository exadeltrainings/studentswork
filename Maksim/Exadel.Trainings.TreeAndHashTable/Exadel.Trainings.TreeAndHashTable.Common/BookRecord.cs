﻿using System;
using System.Runtime.Serialization;

namespace Exadel.Trainings.TreeAndHashTable.Common
{
    /// <summary>
    /// Represents phone book record
    /// </summary>
    [Serializable]
    [DataContract]
    public class BookRecord
    {
        /// <summary>
        /// Person's phone number
        /// </summary>
        [DataMember]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Person's first name
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }

        /// <summary>
        /// Person's middle name
        /// </summary>
        [DataMember]
        public string MiddleName { get; set; }

        /// <summary>
        /// Person's last name
        /// </summary>
        [DataMember]
        public string LastName { get; set; }

        /// <summary>
        /// Person's address
        /// </summary>
        [DataMember]
        public string Address { get; set; }

        public BookRecord()
        {
        }

        public BookRecord(BookRecord source)
        {
            PhoneNumber = source.PhoneNumber;
            FirstName = source.PhoneNumber;
            MiddleName = source.MiddleName;
            LastName = source.LastName;
            Address = source.Address;
        }

        public BookRecord(string newPhoneNumber, string newFirstName, string newMiddleName, string newLastName,
            string newAddress)
        {
            PhoneNumber = newPhoneNumber;
            FirstName = newFirstName;
            MiddleName = newMiddleName;
            LastName = newLastName;
            Address = newAddress;
        }
    }
}
