﻿using System;
using System.Collections.Generic;

namespace Exadel.Trainings.TreeAndHashTable.Common.BinaryTree
{
    /// <summary>
    /// Represents binary tree.
    /// </summary>
    public class BinaryTree
    {
        /// <summary>
        /// Tree root element.
        /// </summary>
        public Node Root { get; private set; }

        /// <summary>
        /// BinaryTree constructor.
        /// </summary>
        /// <param name="newRoot">New binary tree root element.</param>
        public BinaryTree(Node newRoot)
        {
            Root = newRoot;
        }

        /// <summary>
        /// Clears the bunary tree.
        /// </summary>
        public void Clear()
        {
            Root = null;
        }

        /// <summary>
        /// Returns the depth of the binary tree.
        /// </summary>
        /// <returns>The depth of the binary tree.</returns>
        public int Depth()
        {
            return CalculateHeight(Root, 0) - 1;
        }

        /// <summary>
        /// Returns the height of the binary tree.
        /// </summary>
        /// <returns>The height of the binary tree.</returns>
        public int Height()
        {
            return CalculateHeight(Root, 0);
        }

        /// <summary>
        /// Calculates height of the binary tree.
        /// </summary>
        /// <param name="localRoot">Tree root.</param>
        /// <returns>Height if the tree. (Depth = Height -1).</returns>
        private int CalculateHeight(Node localRoot, int height)
        {
            if (localRoot == null)
            {
                return height;
            }
            return Math.Max(CalculateHeight(localRoot.LeftChild, height + 1),
                CalculateHeight(localRoot.RightChild, height + 1));
        }

        /// <summary>
        /// Returns binary tree width.
        /// </summary>
        /// <returns>The width.</returns>
        public int GetMaxWidth()
        {
            var maxWidth = 0;
            var height = Height();
            for (int i = 1; i <= height; i++)
            {
                var width = GetWidth(Root, i);
                if (width > maxWidth)
                {
                    maxWidth = width;
                }
            }

            return maxWidth;
        }

        private int GetWidth(Node localRoot, int level)
        {
            if (localRoot == null)
            {
                return 0;
            }
            if (level == 1)
            {
                return 1;
            }
            else 
            {
                return GetWidth(localRoot.LeftChild, level - 1) + GetWidth(localRoot.RightChild, level - 1);
            }
        }

        /// <summary>
        /// Returns the count of the nodes in the binary tree.
        /// </summary>
        /// <param name="localRoot">Binary tree root.</param>
        /// <returns>Count of the nodes.</returns>
        public int Count(Node localRoot)
        {
            if (localRoot == null)
            {
                return 0;
            }
            else
            {
                return Count(localRoot.LeftChild) + 1 + Count(localRoot.RightChild);
            }
        }

        /// <summary>
        /// Finds BookRecord with the specify number in the binary tree.
        /// </summary>
        /// <param name="number">Phone number of the record.</param>
        /// <returns>BookRecord if it's was found, otherwise null.</returns>
        public BookRecord Find(string number)
        {
            var current = Root;
            while (current.Value.PhoneNumber != number)
            {
                if (number.CompareTo(current.Value.PhoneNumber) < 0)
                {
                    current = current.LeftChild;
                }
                else
                {
                    current = current.RightChild;
                }
                if (current == null)
                {
                    return null;
                }
            }
            return current.Value;
        }

        /// <summary>
        /// Adds an specify book record into the tree.
        /// </summary>
        /// <param name="record">BookRecord to insert.</param>
        public void Insert(BookRecord record)
        {
            var newNode = new Node(record);
            if (Root == null)
            {
                Root = newNode;
            }
            else
            {
                var current = Root;
                while (true)
                {
                    var parent = current;
                    if (record.PhoneNumber.CompareTo(current.Value.PhoneNumber) < 0)
                    {
                        current = current.LeftChild;
                        if (current == null)
                        {
                            parent.LeftChild = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.RightChild;
                        if (current == null)
                        {
                            parent.RightChild = newNode;
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Removes the node with specify phone number from the binary tree.
        /// </summary>
        /// <param name="number">Element's phone number.</param>
        /// <returns>True if node was removed, otherwise false.</returns>
        public bool Remove(string number)
        {
            if (Root == null)
            {
                return false;
            }

            var current = Root;
            var parent = Root;
            var isLeftChild = true;
            // Find element with specify phone number and his parent.
            while (current.Value.PhoneNumber != number)
            {
                parent = current;
                if (number.CompareTo(current.Value.PhoneNumber) < 0)
                {
                    isLeftChild = true;
                    current = current.LeftChild;
                }
                else
                {
                    isLeftChild = false;
                    current = current.RightChild;
                }

                if (current == null)
                {
                    return false;
                }
            }

            // If current node doesn't have childs.
            if (current.LeftChild == null && current.RightChild == null)
            {
                if (current == Root)
                {
                    Root = null;
                }
                else if (isLeftChild)
                {
                    parent.LeftChild = null;
                }
                else
                {
                    parent.RightChild = null;
                }
            }
            // If current node has one child.
            else if (current.RightChild == null)
            {
                if (current == Root)
                {
                    Root = current.LeftChild;
                }
                else if (isLeftChild)
                {
                    parent.LeftChild = current.LeftChild;
                }
                else
                {
                    parent.RightChild = current.LeftChild;
                }
            }
            else if (current.LeftChild == null)
            {
                if (current == Root)
                {
                    Root = current.RightChild;
                }
                else if (isLeftChild)
                {
                    parent.LeftChild = current.RightChild;
                }
                else
                {
                    parent.RightChild = current.RightChild;
                }
            }
            // If current node has two childs.
            else
            {
                var successor = GetSuccessor(current);

                if (current == Root)
                {
                    Root = successor;
                }
                else if (isLeftChild)
                {
                    parent.LeftChild = successor;
                }
                else
                {
                    parent.RightChild = successor;
                }

                successor.LeftChild = current.LeftChild;
            }

            return true;
        }

        /// <summary>
        /// Returns the successor of the delNode.
        /// </summary>
        /// <param name="delNode"></param>
        /// <returns>The node with value closest to current.</returns>
        private Node GetSuccessor(Node delNode)
        {
            var successorParent = delNode;
            var successor = delNode;
            var current = delNode.RightChild;
            while (current != null)
            {
                successorParent = successor;
                successor = current;
                current = current.LeftChild;
            }
            // If successor - RightChild
            if (successor != delNode.RightChild)
            {
                successorParent.LeftChild = successor.RightChild;
                successor.RightChild = delNode.RightChild;
            }

            return successor;
        }

        /// <summary>
        /// Returns values of the all tree nodes.
        /// </summary>
        /// <param name="list">New list of the BookRecord.</param>
        /// <param name="localRoot">Tree root.</param>
        /// <returns>All nodes values.</returns>
        public List<BookRecord> GetRecords(List<BookRecord> list, Node localRoot)
        {
            if (localRoot == null)
            {
                return list;
            }
            else
            {
                list.Add(localRoot.Value);
                GetRecords(list, localRoot.LeftChild);

                GetRecords(list, localRoot.RightChild);
            }
            return list;
        }

    }
}
