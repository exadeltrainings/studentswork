﻿namespace Exadel.Trainings.TreeAndHashTable.Common.BinaryTree
{
    /// <summary>
    /// Represents a binary tree node.
    /// </summary>
    public class Node
    {
        /// <summary>
        /// Node's value.
        /// </summary>
        public BookRecord Value { get; private set; }

        /// <summary>
        /// Node's left child.
        /// </summary>
        public Node LeftChild { get; set; }

        /// <summary>
        /// Node's right child.
        /// </summary>
        public Node RightChild { get; set; }

        /// <summary>
        /// Binary node constructor.
        /// </summary>
        /// <param name="newValue">Node value.</param>
        public Node(BookRecord newValue)
        {
            Value = newValue;
        }
    }
}
