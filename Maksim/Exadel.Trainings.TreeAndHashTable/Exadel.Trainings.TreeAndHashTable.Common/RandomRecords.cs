﻿using System;

namespace Exadel.Trainings.TreeAndHashTable.Common
{
    public static class RandomRecords
    {
        private static Random _rand = new Random();

        /// <summary>
        /// Randomize all fields of the record.
        /// </summary>
        public static void RandomFields(BookRecord record)
        {
            record.PhoneNumber = PhoneNumberRandom();
            record.FirstName = NamesRandom(_rand.Next(4, 15));
            record.MiddleName = NamesRandom(_rand.Next(6, 20));
            record.LastName = NamesRandom(_rand.Next(6, 20));

            record.Address = NamesRandom(_rand.Next(8, 20)) + " " + _rand.Next(10);
        }

        /// <summary>
        /// Randomize person's phone number.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string PhoneNumberRandom(int length = 12)
        {
            string result = null;
            for (int i = 0; i < 12; i++)
            {
                result += _rand.Next(10);
            }

            return result;
        }

        /// <summary>
        /// Randomize person's name.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string NamesRandom(int length)
        {
            var result = new char[length];
            for (int i = 0; i < result.Length; i++)
            {
                do
                    result[i] = (char)_rand.Next(97, 123);
                while (result[i] < '!');
            }
            result[0] = Char.ToUpper(result[0]);
            return new string(result);
        }
    }
}
