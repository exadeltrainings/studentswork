﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Exadel.Trainings.TreeAndHashTable.Common.Helpers
{
    /// <summary>
    /// Represents methods performance time.
    /// </summary>
    public static class TimeHelper
    {
        /// <summary>
        /// Calculate method's performance time.
        /// </summary>
        /// <param name="function">Method.</param>
        /// <param name="value">Method's params.</param>
        /// <returns>Performance time.</returns>
        public static long CalculateTime(Action<BookRecord> function, BookRecord value)
        {
            var timer = new Stopwatch();
            timer.Start();

            function(value);

            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        /// <summary>
        /// Calculate method's performance time.
        /// </summary>
        /// <param name="function">Method.</param>
        /// <param name="value">Method's params.</param>
        /// <returns>Performance time.</returns>
        public static long CalculateTime(Action<List<BookRecord>> function, List<BookRecord> value)
        {
            var timer = new Stopwatch();
            timer.Start();

            function(value);

            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        /// <summary>
        /// Calculate method's performance time.
        /// </summary>
        /// <param name="function">Method.</param>
        /// <returns>Performance time.</returns>
        public static long CalculateTime(Action function)
        {
            var timer = new Stopwatch();
            timer.Start();

            function();

            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        /// <summary>
        /// Calculate method's performance time.
        /// </summary>
        /// <param name="function">Method.</param>
        /// <param name="value">Method's params.</param>
        /// <param name="result">Result.</param>
        /// <returns>Performance time.</returns>
        public static long CalculateTime(Func<string, BookRecord> function, string value, out BookRecord result)
        {
            var timer = new Stopwatch();
            timer.Start();

            result = function(value);

            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        /// <summary>
        /// Calculate method's performance time.
        /// </summary>
        /// <param name="function">Method.</param>
        /// <param name="value">Method's params</param>
        /// <param name="result">Result.</param>
        /// <returns>Performance time.</returns>
        public static long CalculateTime(Func<string, long> function, string value, out long result)
        {
            var timer = new Stopwatch();
            timer.Start();

            result = function(value);

            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        /// <summary>
        /// Calculate method's performance time.
        /// </summary>
        /// <param name="function">Method.</param>
        /// <param name="value">Method's params</param>
        /// <param name="result">Result.</param>
        /// <returns>Performance time.</returns>
        public static long CalculateTime(Func<string, bool> function, string value, out bool result)
        {
            var timer = new Stopwatch();
            timer.Start();

            result = function(value);

            timer.Stop();
            return timer.ElapsedMilliseconds;
        }
    }
}
