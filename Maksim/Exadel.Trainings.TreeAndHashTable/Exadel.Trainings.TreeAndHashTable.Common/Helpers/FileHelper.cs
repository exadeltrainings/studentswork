﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Xml.Serialization;

namespace Exadel.Trainings.TreeAndHashTable.Common.Helpers
{
    /// <summary>
    /// Contains methods that helps work with files.
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// Reads data from specify file.
        /// </summary>
        /// <param name="path">File path.</param>
        /// <returns>List of a BookRecord elements.</returns>
        public static List<BookRecord> ReadData(string path)
        {
            using (var reader = new StreamReader(path))
            {
                var records = new List<BookRecord>();
                while (!reader.EndOfStream)
                {
                    var fields = reader.ReadLine()?.Split(',');
                    if (fields == null) continue;
                    fields[4] = fields[4].Replace("\"", "");
                    records.Add(new BookRecord(fields[0], fields[1], fields[2], fields[3], fields[4]));
                }

                return records;
            }
        }

        /// <summary>
        /// Reads data from specify json file.
        /// </summary>
        /// <param name="path">File path.</param>
        /// <returns>List of a BookRecord elements.</returns>
        public static List<BookRecord> ReadDataFromJsonFile(string path)
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(List<BookRecord>));
            List<BookRecord> list;

            using (var reader = new FileStream(path, FileMode.Open))
            {
                list = (List<BookRecord>)jsonSerializer.ReadObject(reader);
            }

            return list;
        }

        /// <summary>
        /// Reads data from specify xml file.
        /// </summary>
        /// <param name="path">File path.</param>
        /// <returns>List of a BookRecord elements.</returns>
        public static List<BookRecord> ReadDataFromXmlFile(string path)
        {
            var serializer = new XmlSerializer(typeof(List<BookRecord>));
            List<BookRecord> list;

            using (var reader = new StreamReader(path))
            {
                list = (List<BookRecord>)serializer.Deserialize(reader);
            }

            return list;
        }

        /// <summary>
        /// Writes data into the xml file.
        /// </summary>
        /// <param name="path">File path.</param>
        /// <param name="records">List of a BookRecord elements.</param>
        public static void WriteDataIntoJsonFile(string path, List<BookRecord> records)
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(List<BookRecord>));

            using (var writer = new FileStream(path, FileMode.Create))
            {
                jsonSerializer.WriteObject(writer, records);
            }
        }

        /// <summary>
        /// Writes data into the xml file.
        /// </summary>
        /// <param name="path">File path.</param>
        /// <param name="records">List of a BookRecord elements.</param>
        public static void WriteDataIntoXmlFile(string path, List<BookRecord> records)
        {
            var serializer = new XmlSerializer(typeof(List<BookRecord>));

            using (var sw = new StreamWriter(path))
            {
                serializer.Serialize(sw, records);
            }
        }

        /// <summary>
        /// Writes data into the text file.
        /// </summary>
        /// <param name="path">File path.</param>
        /// <param name="source">Collection of a BookRecord elements.</param>
        public static void WriteDataIntoFile(string path, IEnumerable<BookRecord> source)
        {
            using (var fw = new StreamWriter(path))
            {
                foreach (var i in source)
                {
                    fw.WriteLine("{0},{1},{2},{3},\"{4}\"", i.PhoneNumber, i.FirstName, i.MiddleName, i.LastName, i.Address);
                }
            }
        }
    }
}
