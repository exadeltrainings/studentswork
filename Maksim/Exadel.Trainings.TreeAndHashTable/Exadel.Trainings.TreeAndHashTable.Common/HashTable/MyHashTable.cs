﻿using System.Collections.Generic;
using System.Linq;

namespace Exadel.Trainings.TreeAndHashTable.Common.HashTable
{
    /// <summary>
    /// Represents a hash table collection
    /// </summary>
    public class MyHashTable
    {
        /// <summary>
        /// Returns hash table array.
        /// </summary>
        public BookRecord[] Array { get; private set; }

        /// <summary>
        /// Returns hash table size.
        /// </summary>
        public int Size { get; private set; }

        /// <summary>
        /// Returns collisions count.
        /// </summary>
        public int CollisionsCount { get; private set; }

        // Max load of the hash table
        private double _loadFactor = .72;
        /// <summary>
        /// Returns the load factor.
        /// </summary>
        public double CurrentLoadFactor { get; private set; }

        /// <summary>
        /// Return the number of the elements in hash table.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Hash table constructor.
        /// </summary>
        /// <param name="count">Elements count.</param>
        public MyHashTable(int count)
        {
            Size = (int)(count / _loadFactor);
            Array = new BookRecord[Size];
        }

        /// <summary>
        /// Hash table constructor.
        /// </summary>
        /// <param name="elements">Collection of the elements to add into hash table.</param>
        public MyHashTable(IEnumerable<BookRecord> elements)
        {
            Size = (int)(elements.Count() / _loadFactor);
            Array = new BookRecord[Size];

            foreach (var e in elements)
            {
                Add(e);
            }
        }

        public void Clear()
        {
            Array = null;
            Size = 0;
            CollisionsCount = 0;
            CurrentLoadFactor = 0;
            Count = 0;
        }

        /// <summary>
        /// Creates valid hash code.
        /// </summary>
        /// <param name="key">Large hash code.</param>
        /// <returns>The valid hash code.</returns>
        public int CreateHashCode(string value)
        {
            const ulong b = 378551;
            ulong hash = 0;
            ulong a = 63689;

            foreach (var c in value)
            {
                hash = hash * a + c;
                a = a * b;
            }

            return (int)(hash % (ulong)Size);
        }

        /// <summary>
        /// Add the element into the hash table.
        /// </summary>
        /// <param name="record">The value of the element to add.</param>
        public void Add(BookRecord record)
        {
            if (CollisionsCount > 0.1 * Count || CurrentLoadFactor > _loadFactor)
            {
                Rebuild();
            }

            var key = CreateHashCode(record.PhoneNumber); 
            // used to check collision
            var tempKey = key;

            while (Array[key] != null && key < Array.Length)
            {
                key++;
                //if end of table return to the begin
                key %= Size;
            }
            if (key != tempKey)
            {
                CollisionsCount++;
            }


            Array[key] = record;
            Count++;

            CurrentLoadFactor = (double)Count / Size;
        }

        /// <summary>
        /// Rebuilds the current hash table.
        /// </summary>
        private void Rebuild()
        {
            var elements = GetRecords();

            Size = (int)(Size * 1.1);
            Array = new BookRecord[Size];
            CollisionsCount = 0;
            Count = 0;

            foreach (var e in elements)
            {
                Add(e);
            }
        }

        /// <summary>
        /// Removes the element with the specified value from hash table.
        /// </summary>
        /// <param name="number">The phone number of the book record.</param>
        public bool Remove(string number)
        {
            var index = FindArrayIndex(number);

            if (index == -1)
            {
                return false;
            }

            var collisionFlag = false;
            //if end of table return to the begin
            var nextIndex = (index + 1) % Size;
            var next = Array[nextIndex];

            var nextHashCode = 0;
            if (next != null)
            {
               nextHashCode = CreateHashCode(next.PhoneNumber);
            }
            while (next != null && nextIndex != nextHashCode)
            {
                Array[index] = next;
                //if end of table return to the begin
                index = (index + 1) % Size;
                nextIndex = (nextIndex + 1) % Size;
                next = Array[nextIndex];
                nextHashCode = CreateHashCode(next.PhoneNumber);
                collisionFlag = true;
            }

            if (collisionFlag)
            {
                CollisionsCount--;
            }

            Array[index] = null;
            Count--;
            return true;
        }

        /// <summary>
        /// Returns array index of book record with specify phone number.
        /// </summary>
        /// <param name="number">Books record phone number.</param>
        /// <returns>Array index.</returns>
        public long FindArrayIndex(string number)
        {
            var key = CreateHashCode(number);
            if (Array[key] == null)
            {
                return -1;
            }
            if (Array[key].PhoneNumber == number)
            {
                return key;
            }
            else
            {
                while (Array[key] != null && Array[key].PhoneNumber != number)
                {
                    key++;
                    //if end of table return to the begin
                    key %= Size;
                }
                if (Array[key] == null)
                {
                    return - 1;
                }
                else
                {
                    return key;
                }
            }
        }

        /// <summary>
        /// Determines whether an element is in the hash table.
        /// </summary>
        /// <param name="number">Records phone number. </param>
        /// <returns>
        /// True if hash table contains element with specified value,
        /// else false.
        /// </returns>
        public bool ContainsValue(string number)
        {
            return GetRecords().Any(i => i.PhoneNumber.CompareTo(number) == 0);
        }

        /// <summary>
        /// Finds element in the hash table.
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Element value. If value wasn't found returns null.</returns>
        public BookRecord Find(string number)
        {
            var key = CreateHashCode(number);

            while (Array[key] != null)
            {
                if (Array[key].PhoneNumber == number)
                {
                    return Array[key];
                }
                else
                {
                    key++;
                    //if end of table return to the begin
                    key %= Size;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns all records in phonebook.
        /// </summary>
        /// <returns>The list of DataItem elements.</returns>
        public List<BookRecord> GetRecords()
        {
            return (from i in Array where i != null select i).ToList();
        }
    }
}
