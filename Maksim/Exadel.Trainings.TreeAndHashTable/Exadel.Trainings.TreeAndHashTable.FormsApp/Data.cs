﻿using System.Collections.Generic;
using Exadel.Trainings.TreeAndHashTable.Common;
using Exadel.Trainings.TreeAndHashTable.Common.BinaryTree;
using Exadel.Trainings.TreeAndHashTable.Common.HashTable;

namespace Exadel.Trainings.TreeAndHashTable.FormsApp
{
    /// <summary>
    /// This class is used to send data from one form to another. 
    /// </summary>
    public static class Data
    {
        /// <summary>
        /// Stores the list of BookRecord that was read from the file.
        /// </summary>
        public static List<BookRecord> List { get; set; }

        /// <summary>
        /// Stores the hash table that was created from the list.
        /// </summary>
        public static MyHashTable HashTable { get; set; }

        /// <summary>
        /// Stores the binary tree that was created from the list.
        /// </summary>
        public static BinaryTree BinaryTree { get; set; }

        /// <summary>
        /// Stores the BookRecord to add to a hash table or binary tree.
        /// </summary>
        public static BookRecord Record { get; set; }

        /// <summary>
        /// Stores the phone number to find or to remove record to a hash table or binary tree.
        /// </summary>
        public static string PhoneNumber { get; set; }
    }
}

