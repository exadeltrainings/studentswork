﻿using System;
using System.Windows.Forms;
using Exadel.Trainings.TreeAndHashTable.Common.Helpers;

namespace Exadel.Trainings.TreeAndHashTable.FormsApp.Forms
{
    /// <summary>
    /// Main form of the application.
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Main form constructor.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            DisplayDateAndTime();
        }

        private ToolStripLabel _timeLabel;

        /// <summary>
        /// Displays date and time in the statusStrip.
        /// </summary>
        private void DisplayDateAndTime()
        {
            var infoLabel = new ToolStripLabel();
            _timeLabel = new ToolStripLabel();
            infoLabel.Text = @"Today is " + DateTime.Today.ToShortDateString() + @". Time: ";

            statusStrip.Items.Add(infoLabel);
            statusStrip.Items.Add(_timeLabel);

            var timer = new Timer() {Interval = 1000};
            timer.Tick += TimerTick;
            timer.Start();
        }

        /// <summary>
        /// Sets _timeLabel's text property into the current time.
        /// </summary>
        private void TimerTick(object sender, EventArgs e)
        {
            _timeLabel.Text = DateTime.Now.ToLongTimeString();
        }

        /// <summary>
        /// Shows dialog to choose file.
        /// </summary>
        private void ChooseFileButton_Click(object sender, EventArgs e)
        {
            var openDlg = new OpenFileDialog
            {
                Multiselect = false,
                Filter = @"(*.txt)|*.txt|(*.xml)|*.xml|(*.json)|*.json",
                Title = @"Choose file"
            };
            var dlgResult = openDlg.ShowDialog();
            if (dlgResult == DialogResult.OK)
            {
                try
                {
                    switch (openDlg.FilterIndex)
                    {
                        case 1:
                            Data.List = FileHelper.ReadData(openDlg.FileName);
                            break;
                        case 2:
                            Data.List = FileHelper.ReadDataFromXmlFile(openDlg.FileName);
                            break;
                        default:
                            Data.List = FileHelper.ReadDataFromJsonFile(openDlg.FileName);
                            break;
                    }
                    
                    BookRecordsDataGridView.DataSource = Data.List; 

                    CountLabel.Text = @"File contains " + BookRecordsDataGridView.RowCount + @" records";

                    CountLabel.Visible = true;
                    HashTableButton.Enabled = true;
                    TreeButton.Enabled = true;
                    hashTableToolStripMenuItem.Enabled = true;
                    binaryTreeToolStripMenuItem.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Closes form.
        /// </summary>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Opens new form and builds hash table.
        /// </summary>
        private void HashTableButton_Click(object sender, EventArgs e)
        {
            var hashTableForm = new HashTableForm();
            hashTableForm.Show();
        }

        /// <summary>
        /// Opens new form and builds binary tree.
        /// </summary>
        private void TreeButton_Click(object sender, EventArgs e)
        {
            var treeForm = new TreeForm();
            treeForm.Show();
        }

        /// <summary>
        /// Shows message box for "About" menu item.
        /// </summary>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var text =
                "This application shows hash table and binary tree works. Choose file, then choose \"Hash table\" or \"Binary tree\". You can interact with selected data structure at the new form. Created by Maksim Berezovskiy";
            MessageBox.Show(text, @"About application", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
