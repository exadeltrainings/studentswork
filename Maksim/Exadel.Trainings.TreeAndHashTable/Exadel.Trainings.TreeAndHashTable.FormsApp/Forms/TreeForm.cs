﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Exadel.Trainings.TreeAndHashTable.Common;
using Exadel.Trainings.TreeAndHashTable.Common.BinaryTree;
using Exadel.Trainings.TreeAndHashTable.Common.Helpers;

namespace Exadel.Trainings.TreeAndHashTable.FormsApp.Forms
{
    /// <summary>
    /// Represents a form for work with binary tree.
    /// </summary>
    public partial class TreeForm : Form
    {
        /// <summary>
        /// Tree form constructor.
        /// </summary>
        public TreeForm()
        {
            InitializeComponent();

            StatisticalDataGridView.Rows.Add("Build time :", TimeHelper.CalculateTime(BuildBinaryTree) + @" ms");
            StatisticalDataGridView.Rows.Add("Tree depth :", Data.BinaryTree.Depth());
            StatisticalDataGridView.Rows.Add("Elements :", Data.BinaryTree.Count(Data.BinaryTree.Root));
            StatisticalDataGridView.Rows.Add("Tree height :", Data.BinaryTree.Height());
            StatisticalDataGridView.Rows.Add("Tree width :", Data.BinaryTree.GetMaxWidth());
            StatisticalDataGridView.Rows.Add("Performance time:", 0);
            StatisticalDataGridView.Rows[5].Visible = false;

            RecordDataGridView.Rows.Add("Phone number :", "");
            RecordDataGridView.Rows.Add("First name :", "");
            RecordDataGridView.Rows.Add("Middle name :", "");
            RecordDataGridView.Rows.Add("Last name :", "");
            RecordDataGridView.Rows.Add("Address :", "");

            WrapDrawTree(Data.BinaryTree.Root);

            BinaryTreeView.ExpandAll();
        }

        /// <summary>
        /// Builds binary tree.
        /// </summary>
        private void BuildBinaryTree()
        {
            Data.BinaryTree = new BinaryTree(new Node(Data.List[0]));
            for (int i = 1; i < Data.List.Count; i++)
            {
                Data.BinaryTree.Insert(Data.List[i]);
            }
        }

        /// <summary>
        /// Refreshes statistic's labels.
        /// </summary>
        private void RefreshLabels()
        {
            StatisticalDataGridView.Rows[1].SetValues("Tree depth :", Data.BinaryTree.Depth());
            StatisticalDataGridView.Rows[2].SetValues("Elements :", Data.BinaryTree.Count(Data.BinaryTree.Root));
            StatisticalDataGridView.Rows[3].SetValues("Tree height :", Data.BinaryTree.Height());
            StatisticalDataGridView.Rows[4].SetValues("Tree width :", Data.BinaryTree.GetMaxWidth());
        }

        /// <summary>
        /// Creates binary tree in the TreeView.
        /// </summary>
        /// <param name="root">The root of the binary tree.</param>
        private void WrapDrawTree(Node root)
        {
            var node = DrawTree(root);
            BinaryTreeView.Nodes.Add(node);
        }

        private TreeNode DrawTree(Node localRoot)
        {
            TreeNode vrt = null;
            if (localRoot != null)
            {
                vrt = new TreeNode(localRoot.Value.PhoneNumber) {Name = localRoot.Value.PhoneNumber};
                var branch = DrawTree(localRoot.LeftChild);
                if (branch != null)
                {
                    //branch.ForeColor = Color.Aqua;
                    branch.Name = localRoot.LeftChild.Value.PhoneNumber;
                    vrt.Nodes.Add(branch);
                }
                else
                {
                    vrt.Nodes.Add("***");
                }

                branch = DrawTree(localRoot.RightChild);
                if (branch != null)
                {
                    //branch.ForeColor = Color.Crimson;
                    branch.Name = localRoot.RightChild.Value.PhoneNumber;
                    vrt.Nodes.Add(branch);
                }
                else
                {
                    vrt.Nodes.Add("***");
                }
            }

            return vrt;
        }

        private void FindButton_Click(object sender, EventArgs e)
        {
            var findForm = new FindAndRemove();
            findForm.Done += FindRecordInTheBinaryTree;
            findForm.ShowDialog();
        }

        private void FindRecordInTheBinaryTree(object sender, EventArgs e)
        {
            BookRecord record = null;
            StatisticalDataGridView.Rows[5].SetValues("Search time :",
                TimeHelper.CalculateTime(Data.BinaryTree.Find, Data.PhoneNumber, out record) + " ms");
            if (record != null)
            {
                StatisticalDataGridView.Rows[5].Visible = true;
                BinaryTreeView.SelectedNode = BinaryTreeView.Nodes.Find(record.PhoneNumber, true).First();
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var addForm = new AddForm();
            addForm.Done += AddRecordToTheBinaryTree;
            addForm.ShowDialog();
        }

        private void AddRecordToTheBinaryTree(object sender, EventArgs e)
        {
            StatisticalDataGridView.Rows[5].SetValues("Addition time :",
                TimeHelper.CalculateTime(Data.BinaryTree.Insert, Data.Record) + " ms");
            BinaryTreeView.Nodes.Clear();
            WrapDrawTree(Data.BinaryTree.Root);

            BinaryTreeView.ExpandAll();
            StatisticalDataGridView.Rows[5].Visible = true;
            BinaryTreeView.SelectedNode = BinaryTreeView.Nodes.Find(Data.Record.PhoneNumber, true).First();

            RefreshLabels();
            MessageBox.Show(@"Record has been added into the binary tree.", @"Message", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            var removeForm = new FindAndRemove();
            removeForm.Done += RemoveRecordFromTheBinaryTree;
            removeForm.ShowDialog();
        }

        private void RemoveRecordFromTheBinaryTree(object sender, EventArgs e)
        {
            var removeFlag = false;
            StatisticalDataGridView.Rows[5].SetValues("Removal time :",
                TimeHelper.CalculateTime(Data.BinaryTree.Remove, Data.PhoneNumber, out removeFlag) + " ms");
            if (removeFlag)
            {
                MessageBox.Show(@"Record has been removed from the binary tree.", @"Message", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                BinaryTreeView.Nodes.Clear();
                WrapDrawTree(Data.BinaryTree.Root);
                BinaryTreeView.ExpandAll();

                RefreshLabels();
            }
            else
            {
                MessageBox.Show(@"The binary tree doesn't contains record with such phone number.", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            statusStrip1.Items.Clear();
        }

        private void ShowHelpInTheStatusStrip(string text)
        {
            var infoStatusStrip = new ToolStripLabel(text);
            statusStrip1.Items.Add(infoStatusStrip);
        }

        private void FindButton_MouseEnter(object sender, EventArgs e)
        {
            ShowHelpInTheStatusStrip("Click to find record!");
        }

        private void AddButton_MouseEnter(object sender, EventArgs e)
        {
            ShowHelpInTheStatusStrip("Click to add record!");
        }

        private void RemoveButton_MouseEnter(object sender, EventArgs e)
        {
            ShowHelpInTheStatusStrip("Click to remove record!");
        }

        private void ShowStatisticsButton_Click(object sender, EventArgs e)
        {
            if (StatisticalLabel.Visible == false)
            {
                StatisticalLabel.Visible = true;
                StatisticalDataGridView.Visible = true;
                ShowStatisticsButton.Text = @"Hide Statistics";
            }
            else
            {
                StatisticalLabel.Visible = false;
                StatisticalDataGridView.Visible = false;
                ShowStatisticsButton.Text = @"Show Statistics";
            }
        }

        private void BinaryTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var number = BinaryTreeView.SelectedNode.Text;
            if (number != "***")
            {
                BookRecord record = null;
                StatisticalDataGridView.Rows[5].SetValues("Search time: ",
                    TimeHelper.CalculateTime(Data.BinaryTree.Find, number, out record) + " ms");
                StatisticalDataGridView.Rows[5].Visible = true;
                RecordDataGridView.Rows[0].SetValues("Phone number :", record.PhoneNumber);
                RecordDataGridView.Rows[1].SetValues("First name :", record.FirstName);
                RecordDataGridView.Rows[2].SetValues("Middle name :", record.MiddleName);
                RecordDataGridView.Rows[3].SetValues("Last name :", record.LastName);
                RecordDataGridView.Rows[4].SetValues("Address :", record.Address);
            }
            else
            {
                StatisticalDataGridView.Rows[5].Visible = false;
                RecordDataGridView.Rows[0].SetValues("Phone number :", "");
                RecordDataGridView.Rows[1].SetValues("First name :", "");
                RecordDataGridView.Rows[2].SetValues("Middle name :", "");
                RecordDataGridView.Rows[3].SetValues("Last name :", "");
                RecordDataGridView.Rows[4].SetValues("Address :", "");
            }
        }

        private void SaveAsButton_Click(object sender, EventArgs e)
        {
            var saveDlg = new SaveFileDialog
            {
                Filter = @"(*.txt)|*.txt|(*.xml)|*.xml|(*.json)|*.json",
                Title = @"Choose file"
            };

            if (saveDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    switch (saveDlg.FilterIndex)
                    {
                        case 1:
                            FileHelper.WriteDataIntoFile(saveDlg.FileName, Data.BinaryTree.GetRecords(new List<BookRecord>(), Data.BinaryTree.Root));
                            break;
                        case 2:
                            FileHelper.WriteDataIntoXmlFile(saveDlg.FileName, Data.BinaryTree.GetRecords(new List<BookRecord>(), Data.BinaryTree.Root));
                            break;
                        default:
                            FileHelper.WriteDataIntoJsonFile(saveDlg.FileName, Data.BinaryTree.GetRecords(new List<BookRecord>(), Data.BinaryTree.Root));
                            break;
                    }

                    MessageBox.Show(@"Data has been saved.", @"Information", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
