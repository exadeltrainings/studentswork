﻿namespace Exadel.Trainings.TreeAndHashTable.FormsApp
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HelpPhoneLabel = new System.Windows.Forms.Label();
            this.PhoneTextBox = new System.Windows.Forms.TextBox();
            this.HelpFirstNameLabel = new System.Windows.Forms.Label();
            this.FirstNameTextBox = new System.Windows.Forms.TextBox();
            this.HelpMiddleNameLabel = new System.Windows.Forms.Label();
            this.MiddleNameTextBox = new System.Windows.Forms.TextBox();
            this.HelpLastNameLabel = new System.Windows.Forms.Label();
            this.LastNameTextBox = new System.Windows.Forms.TextBox();
            this.HelpAddressLabel = new System.Windows.Forms.Label();
            this.AddressTextBox = new System.Windows.Forms.TextBox();
            this.DoneButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HelpPhoneLabel
            // 
            this.HelpPhoneLabel.AutoSize = true;
            this.HelpPhoneLabel.Location = new System.Drawing.Point(17, 16);
            this.HelpPhoneLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HelpPhoneLabel.Name = "HelpPhoneLabel";
            this.HelpPhoneLabel.Size = new System.Drawing.Size(95, 16);
            this.HelpPhoneLabel.TabIndex = 0;
            this.HelpPhoneLabel.Text = "Phone number";
            // 
            // PhoneTextBox
            // 
            this.PhoneTextBox.Location = new System.Drawing.Point(17, 37);
            this.PhoneTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.PhoneTextBox.Name = "PhoneTextBox";
            this.PhoneTextBox.Size = new System.Drawing.Size(194, 22);
            this.PhoneTextBox.TabIndex = 1;
            // 
            // HelpFirstNameLabel
            // 
            this.HelpFirstNameLabel.AutoSize = true;
            this.HelpFirstNameLabel.Location = new System.Drawing.Point(17, 65);
            this.HelpFirstNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HelpFirstNameLabel.Name = "HelpFirstNameLabel";
            this.HelpFirstNameLabel.Size = new System.Drawing.Size(70, 16);
            this.HelpFirstNameLabel.TabIndex = 2;
            this.HelpFirstNameLabel.Text = "First name";
            // 
            // FirstNameTextBox
            // 
            this.FirstNameTextBox.Location = new System.Drawing.Point(17, 86);
            this.FirstNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.FirstNameTextBox.Name = "FirstNameTextBox";
            this.FirstNameTextBox.Size = new System.Drawing.Size(194, 22);
            this.FirstNameTextBox.TabIndex = 3;
            // 
            // HelpMiddleNameLabel
            // 
            this.HelpMiddleNameLabel.AutoSize = true;
            this.HelpMiddleNameLabel.Location = new System.Drawing.Point(21, 119);
            this.HelpMiddleNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HelpMiddleNameLabel.Name = "HelpMiddleNameLabel";
            this.HelpMiddleNameLabel.Size = new System.Drawing.Size(86, 16);
            this.HelpMiddleNameLabel.TabIndex = 4;
            this.HelpMiddleNameLabel.Text = "Middle name";
            // 
            // MiddleNameTextBox
            // 
            this.MiddleNameTextBox.Location = new System.Drawing.Point(17, 140);
            this.MiddleNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.MiddleNameTextBox.Name = "MiddleNameTextBox";
            this.MiddleNameTextBox.Size = new System.Drawing.Size(194, 22);
            this.MiddleNameTextBox.TabIndex = 5;
            // 
            // HelpLastNameLabel
            // 
            this.HelpLastNameLabel.AutoSize = true;
            this.HelpLastNameLabel.Location = new System.Drawing.Point(21, 174);
            this.HelpLastNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HelpLastNameLabel.Name = "HelpLastNameLabel";
            this.HelpLastNameLabel.Size = new System.Drawing.Size(70, 16);
            this.HelpLastNameLabel.TabIndex = 6;
            this.HelpLastNameLabel.Text = "Last name";
            // 
            // LastNameTextBox
            // 
            this.LastNameTextBox.Location = new System.Drawing.Point(17, 194);
            this.LastNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.LastNameTextBox.Name = "LastNameTextBox";
            this.LastNameTextBox.Size = new System.Drawing.Size(194, 22);
            this.LastNameTextBox.TabIndex = 7;
            // 
            // HelpAddressLabel
            // 
            this.HelpAddressLabel.AutoSize = true;
            this.HelpAddressLabel.Location = new System.Drawing.Point(21, 223);
            this.HelpAddressLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HelpAddressLabel.Name = "HelpAddressLabel";
            this.HelpAddressLabel.Size = new System.Drawing.Size(59, 16);
            this.HelpAddressLabel.TabIndex = 8;
            this.HelpAddressLabel.Text = "Address";
            // 
            // AddressTextBox
            // 
            this.AddressTextBox.Location = new System.Drawing.Point(17, 244);
            this.AddressTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.AddressTextBox.Name = "AddressTextBox";
            this.AddressTextBox.Size = new System.Drawing.Size(194, 22);
            this.AddressTextBox.TabIndex = 9;
            // 
            // DoneButton
            // 
            this.DoneButton.Location = new System.Drawing.Point(17, 273);
            this.DoneButton.Name = "DoneButton";
            this.DoneButton.Size = new System.Drawing.Size(75, 23);
            this.DoneButton.TabIndex = 10;
            this.DoneButton.Text = "Done";
            this.DoneButton.UseVisualStyleBackColor = true;
            this.DoneButton.Click += new System.EventHandler(this.DoneButton_Click);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(229, 304);
            this.Controls.Add(this.DoneButton);
            this.Controls.Add(this.AddressTextBox);
            this.Controls.Add(this.HelpAddressLabel);
            this.Controls.Add(this.LastNameTextBox);
            this.Controls.Add(this.HelpLastNameLabel);
            this.Controls.Add(this.MiddleNameTextBox);
            this.Controls.Add(this.HelpMiddleNameLabel);
            this.Controls.Add(this.FirstNameTextBox);
            this.Controls.Add(this.HelpFirstNameLabel);
            this.Controls.Add(this.PhoneTextBox);
            this.Controls.Add(this.HelpPhoneLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add the record";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HelpPhoneLabel;
        private System.Windows.Forms.TextBox PhoneTextBox;
        private System.Windows.Forms.Label HelpFirstNameLabel;
        private System.Windows.Forms.TextBox FirstNameTextBox;
        private System.Windows.Forms.Label HelpMiddleNameLabel;
        private System.Windows.Forms.TextBox MiddleNameTextBox;
        private System.Windows.Forms.Label HelpLastNameLabel;
        private System.Windows.Forms.TextBox LastNameTextBox;
        private System.Windows.Forms.Label HelpAddressLabel;
        private System.Windows.Forms.TextBox AddressTextBox;
        private System.Windows.Forms.Button DoneButton;
    }
}