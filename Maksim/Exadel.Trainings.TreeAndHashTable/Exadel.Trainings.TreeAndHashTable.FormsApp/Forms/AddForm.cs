﻿using System;
using System.Windows.Forms;
using Exadel.Trainings.TreeAndHashTable.Common;

namespace Exadel.Trainings.TreeAndHashTable.FormsApp
{
    /// <summary>
    /// Represents form whiсh is used to adds book record.
    /// </summary>
    public partial class AddForm : Form
    {
        /// <summary>
        /// An event which occurs if all is OK.
        /// </summary>
        public event EventHandler Done;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AddForm()
        {
            InitializeComponent();
        }

        private void DoneButton_Click(object sender, EventArgs e)
        {
            if (PhoneTextBox.Text != "" && FirstNameTextBox.Text != "" && MiddleNameTextBox.Text != "" && LastNameTextBox.Text != "" && AddressTextBox.Text != "")
            {
                long number;
                if (long.TryParse(PhoneTextBox.Text, out number))
                {
                    Data.Record = new BookRecord(PhoneTextBox.Text, FirstNameTextBox.Text, MiddleNameTextBox.Text, LastNameTextBox.Text, AddressTextBox.Text);
                    OnDone(EventArgs.Empty);
                    Close();
                }
                else
                {
                    MessageBox.Show(@"Phone number is incorrect!", @"Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(@"Fill all text boxes!!", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        protected virtual void OnDone(EventArgs e)
        {
            Done?.Invoke(this, EventArgs.Empty);
        }
    }
}
