﻿namespace Exadel.Trainings.TreeAndHashTable.FormsApp
{
    partial class FindAndRemove
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HelpPhoneLabel = new System.Windows.Forms.Label();
            this.PhoneTextBox = new System.Windows.Forms.TextBox();
            this.DoneButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HelpPhoneLabel
            // 
            this.HelpPhoneLabel.AutoSize = true;
            this.HelpPhoneLabel.Location = new System.Drawing.Point(14, 9);
            this.HelpPhoneLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.HelpPhoneLabel.Name = "HelpPhoneLabel";
            this.HelpPhoneLabel.Size = new System.Drawing.Size(95, 16);
            this.HelpPhoneLabel.TabIndex = 1;
            this.HelpPhoneLabel.Text = "Phone number";
            // 
            // PhoneTextBox
            // 
            this.PhoneTextBox.Location = new System.Drawing.Point(14, 30);
            this.PhoneTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.PhoneTextBox.Name = "PhoneTextBox";
            this.PhoneTextBox.Size = new System.Drawing.Size(257, 22);
            this.PhoneTextBox.TabIndex = 2;
            // 
            // DoneButton
            // 
            this.DoneButton.Location = new System.Drawing.Point(14, 60);
            this.DoneButton.Name = "DoneButton";
            this.DoneButton.Size = new System.Drawing.Size(75, 23);
            this.DoneButton.TabIndex = 11;
            this.DoneButton.Text = "Done";
            this.DoneButton.UseVisualStyleBackColor = true;
            this.DoneButton.Click += new System.EventHandler(this.DoneButton_Click);
            // 
            // FindAndRemove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 95);
            this.Controls.Add(this.DoneButton);
            this.Controls.Add(this.PhoneTextBox);
            this.Controls.Add(this.HelpPhoneLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FindAndRemove";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Find or Remove";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HelpPhoneLabel;
        private System.Windows.Forms.TextBox PhoneTextBox;
        private System.Windows.Forms.Button DoneButton;
    }
}