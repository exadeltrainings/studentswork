﻿namespace Exadel.Trainings.TreeAndHashTable.FormsApp.Forms
{
    partial class TreeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.FindButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.RemoveButton = new System.Windows.Forms.Button();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workWithMeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findARecordToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addARecordToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removeARecordToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showHideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SaveAsButton = new System.Windows.Forms.Button();
            this.BinaryTreeView = new System.Windows.Forms.TreeView();
            this.StatisticalLabel = new System.Windows.Forms.Label();
            this.StatisticalDataGridView = new System.Windows.Forms.DataGridView();
            this.Property = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Values = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShowStatisticsButton = new System.Windows.Forms.Button();
            this.RecordLabel = new System.Windows.Forms.Label();
            this.RecordDataGridView = new System.Windows.Forms.DataGridView();
            this.Record = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatisticalDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 600);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1126, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // FindButton
            // 
            this.FindButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FindButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindButton.Location = new System.Drawing.Point(16, 557);
            this.FindButton.Margin = new System.Windows.Forms.Padding(4);
            this.FindButton.Name = "FindButton";
            this.FindButton.Size = new System.Drawing.Size(135, 34);
            this.FindButton.TabIndex = 3;
            this.FindButton.Text = "Find a record";
            this.FindButton.UseVisualStyleBackColor = true;
            this.FindButton.Click += new System.EventHandler(this.FindButton_Click);
            this.FindButton.Enter += new System.EventHandler(this.FindButton_MouseEnter);
            this.FindButton.Leave += new System.EventHandler(this.Button_MouseLeave);
            this.FindButton.MouseEnter += new System.EventHandler(this.FindButton_MouseEnter);
            this.FindButton.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // AddButton
            // 
            this.AddButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddButton.Location = new System.Drawing.Point(159, 558);
            this.AddButton.Margin = new System.Windows.Forms.Padding(4);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(125, 33);
            this.AddButton.TabIndex = 7;
            this.AddButton.Text = "Add a record";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            this.AddButton.Enter += new System.EventHandler(this.AddButton_MouseEnter);
            this.AddButton.Leave += new System.EventHandler(this.Button_MouseLeave);
            this.AddButton.MouseEnter += new System.EventHandler(this.AddButton_MouseEnter);
            this.AddButton.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // RemoveButton
            // 
            this.RemoveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RemoveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemoveButton.Location = new System.Drawing.Point(292, 558);
            this.RemoveButton.Margin = new System.Windows.Forms.Padding(4);
            this.RemoveButton.Name = "RemoveButton";
            this.RemoveButton.Size = new System.Drawing.Size(165, 33);
            this.RemoveButton.TabIndex = 8;
            this.RemoveButton.Text = "Remove a record";
            this.RemoveButton.UseVisualStyleBackColor = true;
            this.RemoveButton.Click += new System.EventHandler(this.RemoveButton_Click);
            this.RemoveButton.Enter += new System.EventHandler(this.RemoveButton_MouseEnter);
            this.RemoveButton.Leave += new System.EventHandler(this.Button_MouseLeave);
            this.RemoveButton.MouseEnter += new System.EventHandler(this.RemoveButton_MouseEnter);
            this.RemoveButton.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.workWithMeToolStripMenuItem,
            this.statisticsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip2.Size = new System.Drawing.Size(1126, 24);
            this.menuStrip2.TabIndex = 12;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.saveAsToolStripMenuItem.Text = "Save as";
            // 
            // workWithMeToolStripMenuItem
            // 
            this.workWithMeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findARecordToolStripMenuItem1,
            this.addARecordToolStripMenuItem1,
            this.removeARecordToolStripMenuItem1});
            this.workWithMeToolStripMenuItem.Name = "workWithMeToolStripMenuItem";
            this.workWithMeToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.workWithMeToolStripMenuItem.Text = "Work with records";
            // 
            // findARecordToolStripMenuItem1
            // 
            this.findARecordToolStripMenuItem1.Name = "findARecordToolStripMenuItem1";
            this.findARecordToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.findARecordToolStripMenuItem1.Text = "Find a record";
            this.findARecordToolStripMenuItem1.Click += new System.EventHandler(this.FindButton_Click);
            // 
            // addARecordToolStripMenuItem1
            // 
            this.addARecordToolStripMenuItem1.Name = "addARecordToolStripMenuItem1";
            this.addARecordToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.addARecordToolStripMenuItem1.Text = "Add a record";
            this.addARecordToolStripMenuItem1.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // removeARecordToolStripMenuItem1
            // 
            this.removeARecordToolStripMenuItem1.Name = "removeARecordToolStripMenuItem1";
            this.removeARecordToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.removeARecordToolStripMenuItem1.Text = "Remove a record";
            this.removeARecordToolStripMenuItem1.Click += new System.EventHandler(this.RemoveButton_Click);
            // 
            // statisticsToolStripMenuItem
            // 
            this.statisticsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showHideToolStripMenuItem});
            this.statisticsToolStripMenuItem.Name = "statisticsToolStripMenuItem";
            this.statisticsToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.statisticsToolStripMenuItem.Text = "Statistics";
            // 
            // showHideToolStripMenuItem
            // 
            this.showHideToolStripMenuItem.Name = "showHideToolStripMenuItem";
            this.showHideToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.showHideToolStripMenuItem.Text = "Show/Hide";
            this.showHideToolStripMenuItem.Click += new System.EventHandler(this.ShowStatisticsButton_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.exitToolStripMenuItem.Text = "Close";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseButton.Location = new System.Drawing.Point(1002, 557);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(4);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(117, 33);
            this.CloseButton.TabIndex = 20;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // SaveAsButton
            // 
            this.SaveAsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveAsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveAsButton.Location = new System.Drawing.Point(877, 558);
            this.SaveAsButton.Margin = new System.Windows.Forms.Padding(4);
            this.SaveAsButton.Name = "SaveAsButton";
            this.SaveAsButton.Size = new System.Drawing.Size(117, 34);
            this.SaveAsButton.TabIndex = 24;
            this.SaveAsButton.Text = "Save As";
            this.SaveAsButton.UseVisualStyleBackColor = true;
            this.SaveAsButton.Click += new System.EventHandler(this.SaveAsButton_Click);
            // 
            // BinaryTreeView
            // 
            this.BinaryTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BinaryTreeView.Location = new System.Drawing.Point(13, 55);
            this.BinaryTreeView.Margin = new System.Windows.Forms.Padding(4);
            this.BinaryTreeView.Name = "BinaryTreeView";
            this.BinaryTreeView.Size = new System.Drawing.Size(792, 494);
            this.BinaryTreeView.TabIndex = 0;
            this.BinaryTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.BinaryTreeView_AfterSelect);
            // 
            // StatisticalLabel
            // 
            this.StatisticalLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StatisticalLabel.AutoSize = true;
            this.StatisticalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatisticalLabel.Location = new System.Drawing.Point(896, 66);
            this.StatisticalLabel.Name = "StatisticalLabel";
            this.StatisticalLabel.Size = new System.Drawing.Size(143, 24);
            this.StatisticalLabel.TabIndex = 29;
            this.StatisticalLabel.Text = "Statistical Data";
            this.StatisticalLabel.Visible = false;
            // 
            // StatisticalDataGridView
            // 
            this.StatisticalDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StatisticalDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StatisticalDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Property,
            this.Values});
            this.StatisticalDataGridView.Location = new System.Drawing.Point(816, 94);
            this.StatisticalDataGridView.Margin = new System.Windows.Forms.Padding(4);
            this.StatisticalDataGridView.Name = "StatisticalDataGridView";
            this.StatisticalDataGridView.ReadOnly = true;
            this.StatisticalDataGridView.Size = new System.Drawing.Size(297, 210);
            this.StatisticalDataGridView.TabIndex = 28;
            this.StatisticalDataGridView.Visible = false;
            // 
            // Property
            // 
            this.Property.HeaderText = "Property";
            this.Property.Name = "Property";
            this.Property.ReadOnly = true;
            this.Property.Width = 127;
            // 
            // Values
            // 
            this.Values.HeaderText = "Values";
            this.Values.Name = "Values";
            this.Values.ReadOnly = true;
            this.Values.Width = 127;
            // 
            // ShowStatisticsButton
            // 
            this.ShowStatisticsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowStatisticsButton.Location = new System.Drawing.Point(464, 558);
            this.ShowStatisticsButton.Name = "ShowStatisticsButton";
            this.ShowStatisticsButton.Size = new System.Drawing.Size(127, 33);
            this.ShowStatisticsButton.TabIndex = 30;
            this.ShowStatisticsButton.Text = "Show Statistics";
            this.ShowStatisticsButton.UseVisualStyleBackColor = true;
            this.ShowStatisticsButton.Click += new System.EventHandler(this.ShowStatisticsButton_Click);
            // 
            // RecordLabel
            // 
            this.RecordLabel.AutoSize = true;
            this.RecordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RecordLabel.Location = new System.Drawing.Point(896, 337);
            this.RecordLabel.Name = "RecordLabel";
            this.RecordLabel.Size = new System.Drawing.Size(78, 24);
            this.RecordLabel.TabIndex = 31;
            this.RecordLabel.Text = "Record";
            // 
            // RecordDataGridView
            // 
            this.RecordDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RecordDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RecordDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RecordDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Record,
            this.Value});
            this.RecordDataGridView.Location = new System.Drawing.Point(816, 364);
            this.RecordDataGridView.Name = "RecordDataGridView";
            this.RecordDataGridView.Size = new System.Drawing.Size(297, 159);
            this.RecordDataGridView.TabIndex = 32;
            // 
            // Record
            // 
            this.Record.HeaderText = "Field";
            this.Record.Name = "Record";
            // 
            // Value
            // 
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            // 
            // TreeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 622);
            this.Controls.Add(this.RecordDataGridView);
            this.Controls.Add(this.RecordLabel);
            this.Controls.Add(this.ShowStatisticsButton);
            this.Controls.Add(this.StatisticalLabel);
            this.Controls.Add(this.StatisticalDataGridView);
            this.Controls.Add(this.BinaryTreeView);
            this.Controls.Add(this.SaveAsButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.RemoveButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.FindButton);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TreeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TreeForm";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatisticalDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button FindButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button RemoveButton;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem workWithMeToolStripMenuItem;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button SaveAsButton;
        private System.Windows.Forms.TreeView BinaryTreeView;
        private System.Windows.Forms.Label StatisticalLabel;
        private System.Windows.Forms.DataGridView StatisticalDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Property;
        private System.Windows.Forms.DataGridViewTextBoxColumn Values;
        private System.Windows.Forms.Button ShowStatisticsButton;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findARecordToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addARecordToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem removeARecordToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem statisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showHideToolStripMenuItem;
        private System.Windows.Forms.Label RecordLabel;
        private System.Windows.Forms.DataGridView RecordDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Record;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
    }
}