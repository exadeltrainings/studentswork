﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Exadel.Trainings.TreeAndHashTable.Common;
using Exadel.Trainings.TreeAndHashTable.Common.HashTable;
using Exadel.Trainings.TreeAndHashTable.Common.Helpers;

namespace Exadel.Trainings.TreeAndHashTable.FormsApp.Forms
{
    /// <summary>
    /// Represents a form for work with hash table.
    /// </summary>
    public partial class HashTableForm : Form
    {
        /// <summary>
        /// HashTableForm constructor.
        /// </summary>
        public HashTableForm()
        {
            InitializeComponent();

            StatisticalDataGridView.Rows.Add("Build time :",
                TimeHelper.CalculateTime(CreateHashTable, Data.List) + @" ms");
            StatisticalDataGridView.Rows.Add("Hash table size :", Data.HashTable.Size);
            StatisticalDataGridView.Rows.Add("Elements :", Data.HashTable.Count);
            StatisticalDataGridView.Rows.Add("Collisions :", Data.HashTable.CollisionsCount);
            StatisticalDataGridView.Rows.Add("Table load :",
                String.Format("{0:0.00}", Data.HashTable.CurrentLoadFactor*100) + @" %");
            StatisticalDataGridView.Rows.Add("Performance time:", 0);
            StatisticalDataGridView.Rows[5].Visible = false;

            HashTableDataGridView.DataSource = Data.HashTable.Array;
        }

        /// <summary>
        /// Creates new hash table.
        /// </summary>
        /// <param name="list"></param>
        private void CreateHashTable(IEnumerable<BookRecord> list)
        {
            Data.HashTable = new MyHashTable(list);
        }

        private void FindButton_Click(object sender, EventArgs e)
        {
            var findForm = new FindAndRemove();
            findForm.Done += FindRecordInTheHashTable;
            findForm.ShowDialog();
        }

        private void FindRecordInTheHashTable(object sender, EventArgs e)
        {
            long result;
            StatisticalDataGridView.Rows[5].SetValues("Search time :",
                TimeHelper.CalculateTime(Data.HashTable.FindArrayIndex, Data.PhoneNumber, out result) + @" ms");

            if (result != -1)
            {
                HashTableDataGridView.ClearSelection();
                HashTableDataGridView.Rows[(int) result].Selected = true;
                HashTableDataGridView.CurrentCell = HashTableDataGridView.Rows[(int) result].Cells[0];
                StatisticalDataGridView.Rows[5].Visible = true;
            }
            else
            {
                MessageBox.Show(@"Hash table doesn't contains record with such phone number", @"Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                StatisticalDataGridView.Rows[5].Visible = false;
            }
        }

        /// <summary>
        /// Refresh HashTable.Size and CollisionCount at the form.
        /// </summary>
        private void RefreshLabels()
        {
            StatisticalDataGridView.Rows[1].SetValues("Hash table size :", Data.HashTable.Size);
            StatisticalDataGridView.Rows[2].SetValues("Elements :", Data.HashTable.Count);
            StatisticalDataGridView.Rows[3].SetValues("Collisions :", Data.HashTable.CollisionsCount);
            StatisticalDataGridView.Rows[4].SetValues("Table load :",
                String.Format("{0:0.00}", Data.HashTable.CurrentLoadFactor*100) + @" %");
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var addForm = new AddForm();
            addForm.Done += AddRecordToTheHashTable;
            addForm.ShowDialog();
        }

        private void AddRecordToTheHashTable(object sender, EventArgs e)
        {
            StatisticalDataGridView.Rows[5].Visible = true;
            StatisticalDataGridView.Rows[5].SetValues("Addition time :",
                TimeHelper.CalculateTime(Data.HashTable.Add, Data.Record) + " ms");

            var result = Data.HashTable.FindArrayIndex(Data.Record.PhoneNumber);
            HashTableDataGridView.ClearSelection();
            HashTableDataGridView.Rows[(int) result].Selected = true;
            HashTableDataGridView.CurrentCell = HashTableDataGridView.Rows[(int) result].Cells[0];
            StatisticalDataGridView.Rows[5].Visible = true;

            HashTableDataGridView.Refresh();

            RefreshLabels();
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            var removeForm = new FindAndRemove();
            removeForm.Done += RemoveRecordFromTheHashTable;
            removeForm.ShowDialog();
        }

        private void RemoveRecordFromTheHashTable(object sender, EventArgs e)
        {
            bool isRemoved;
            StatisticalDataGridView.Rows[5].SetValues("Removal time :",
                TimeHelper.CalculateTime(Data.HashTable.Remove, Data.PhoneNumber, out isRemoved) + @" ms");

            if (isRemoved)
            {
                HashTableDataGridView.Refresh();

                MessageBox.Show(@"Record has been removed from the hash table.", @"Message", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                StatisticalDataGridView.Rows[5].Visible = true;
            }
            else
            {
                MessageBox.Show(@"Hash table doesn't contains record with such phone number.", @"Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                StatisticalDataGridView.Rows[5].Visible = false;
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FindButton_MouseEnter(object sender, EventArgs e)
        {
            ShowHelpInTheStatusStrip("Click to find record!");
        }

        private void ShowHelpInTheStatusStrip(string text)
        {
            var infoStatusStrip = new ToolStripLabel(text);
            statusStrip1.Items.Add(infoStatusStrip);
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            statusStrip1.Items.Clear();
        }

        private void AddButton_MouseEnter(object sender, EventArgs e)
        {
            ShowHelpInTheStatusStrip("Click to add record!");
        }


        private void RemoveButton_MouseEnter(object sender, EventArgs e)
        {
            ShowHelpInTheStatusStrip("Click to remove record!");
        }

        private void SaveAsButton_Click(object sender, EventArgs e)
        {
            var saveDlg = new SaveFileDialog
            {
                Filter = @"(*.txt)|*.txt|(*.xml)|*.xml|(*.json)|*.json",
                Title = @"Choose file"
            };

            if (saveDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    switch (saveDlg.FilterIndex)
                    {
                        case 1:
                            FileHelper.WriteDataIntoFile(saveDlg.FileName, Data.HashTable.GetRecords());
                            break;
                        case 2:
                            FileHelper.WriteDataIntoXmlFile(saveDlg.FileName, Data.HashTable.GetRecords());
                            break;
                        default:
                            FileHelper.WriteDataIntoJsonFile(saveDlg.FileName, Data.HashTable.GetRecords());
                            break;
                    }

                    MessageBox.Show(@"Data has been saved.", @"Information", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ShowStatisticsButton_Click(object sender, EventArgs e)
        {
            if (StatisticalLabel.Visible == false)
            {
                StatisticalLabel.Visible = true;
                StatisticalDataGridView.Visible = true;
                ShowStatisticsButton.Text = @"Hide Statistics";
            }
            else
            {
                StatisticalLabel.Visible = false;
                StatisticalDataGridView.Visible = false;
                ShowStatisticsButton.Text = @"Show Statistics";
            }
        }
    }
}
