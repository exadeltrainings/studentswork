﻿using System;
using System.Windows.Forms;

namespace Exadel.Trainings.TreeAndHashTable.FormsApp
{
    /// <summary>
    /// Represents form whiсh is used to find or remove book record.
    /// </summary>
    public partial class FindAndRemove : Form
    {
        /// <summary>
        /// An event which occurs if all is OK.
        /// </summary>
        public event EventHandler Done;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FindAndRemove()
        {
            InitializeComponent();
        }

        private void DoneButton_Click(object sender, EventArgs e)
        {
            if (PhoneTextBox.Text != "")
            {
                long number;
                if (long.TryParse(PhoneTextBox.Text, out number))
                {
                    Data.PhoneNumber = PhoneTextBox.Text;
                    OnDone(EventArgs.Empty);
                    Close();
                }
                else
                {
                    MessageBox.Show(@"Phone number is incorrect!", @"Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(@"Enter the record's phone number!!", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        protected virtual void OnDone(EventArgs e)
        {
            Done?.Invoke(this, EventArgs.Empty);
        }
    }
}
