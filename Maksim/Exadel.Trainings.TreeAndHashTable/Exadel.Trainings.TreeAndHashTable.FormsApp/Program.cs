﻿using System;
using System.Windows.Forms;
using Exadel.Trainings.TreeAndHashTable.FormsApp.Forms;

namespace Exadel.Trainings.TreeAndHashTable.FormsApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
