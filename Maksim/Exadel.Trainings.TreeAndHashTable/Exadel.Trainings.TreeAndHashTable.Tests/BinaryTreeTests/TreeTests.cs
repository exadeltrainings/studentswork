﻿using Exadel.Trainings.TreeAndHashTable.Common;
using Exadel.Trainings.TreeAndHashTable.Common.BinaryTree;
using Exadel.Trainings.TreeAndHashTable.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exadel.Trainings.TreeAndHashTable.Tests.BinaryTreeTests
{
    [TestClass]
    public class TreeTests
    {
        private BinaryTree _tree;
        
        [TestInitialize]
        public void TestInit()
        {
            var list = FileHelper.ReadData(@"..\..\App_Data\DataForTests.txt");
            _tree = new BinaryTree(new Node(list[0]));
            for (int i = 1; i < list.Count; i++)
            {
                _tree.Insert(list[i]);
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            _tree.Clear();
        }

        [TestMethod]
        public void Cout()
        {
            // arrange
            var actualCount = 0;
            var expectedCount = 10;

            // act
            actualCount = _tree.Count(_tree.Root);

            // assert
            Assert.AreEqual(expectedCount, actualCount, "Count is not equals!!");
        }

        [TestMethod]
        public void Clear()
        {
            // arrange
            var actualCount = _tree.Count(_tree.Root);
            var expectedCount = 0;

            // act 
            _tree.Clear();
            actualCount = _tree.Count(_tree.Root);

            // assert
            Assert.AreEqual(expectedCount, actualCount, "Count is not equals!!");
        }

        [TestMethod]
        public void Depth()
        {
            // arrange
            var actualDepth = 0;
            var expectedDepth = 6;

            // act
            actualDepth = _tree.Depth();

            // assert
            Assert.AreEqual(expectedDepth, actualDepth, "Depth is not equals!!");
        }

        [TestMethod]
        public void Height()
        {
            // arrange
            var actualHeight = 0;
            var expectedHeight = 7;

            // act
            actualHeight = _tree.Height();

            // assert
            Assert.AreEqual(expectedHeight, actualHeight, "Height is not equals!!");
        }

        [TestMethod]
        public void MaxWidth()
        {
            // arrange
            var actualWidth = 0;
            var expectedWidth = 2;

            // act
            actualWidth = _tree.GetMaxWidth();

            // assert
            Assert.AreEqual(expectedWidth, actualWidth, "Width is not equals!!");
        }

        [TestMethod]
        public void Insert_RootPosition()
        {
            // arrange
            var record = new BookRecord("5", "v", "v", "v", "v");
            var expectedRoot = record;

            // act
            _tree.Clear();
            _tree.Insert(record);

            // assert 
            Assert.AreEqual(expectedRoot, _tree.Root.Value, "Expected root node is not equals actual!!");
        }

        [TestMethod]
        public void Insert_LeftChild_RightChid()
        {
            // arrange
            var recordLeft = new BookRecord("285555500000", "b", "b", "b", "b");
            var recordRight = new BookRecord("466666666666", "c", "c", "c", "c");
            var expectedLeftRightRightChild = recordLeft;
            var expectedRightLeftChild = recordRight;
            var expectedCount = 12;
            
            // act
            _tree.Insert(recordLeft);
            _tree.Insert(recordRight);
            var actualLeftRightRightChild = _tree.Root.LeftChild.RightChild.RightChild.Value;
            var actualRightLeftChild = _tree.Root.RightChild.LeftChild.Value;
            
            // assert
            Assert.AreEqual(expectedLeftRightRightChild , actualLeftRightRightChild, "Expected left child is not equals actual!!");
            Assert.AreEqual(expectedRightLeftChild, actualRightLeftChild, "Expected right child is not equals actual!!");
            
            Assert.AreEqual(expectedCount, _tree.Count(_tree.Root), "Count is not equals!!");
        }

        [TestMethod]
        public void Find_NonExistingValue()
        {
            // arrange
            var toFind = "123";
            var actualResult = new BookRecord("1", "1", "1", "1", "1");
            
            // act
            actualResult = _tree.Find(toFind);

            // assert
            Assert.AreEqual(null, actualResult, "Value was returned!!");
        }

        [TestMethod]
        public void Find_ExistingValue()
        {
            // arrange
            var toFind = "667218596540";
            var expectedResult = new BookRecord("667218596540", "Czyldyxf", "Sdjshc", "Lhydbqvkfccqx", "Roguzgeshkch 5");
            BookRecord actualResult = null;

            // act
            actualResult = _tree.Find(toFind);

            // assert
            Assert.AreEqual(expectedResult.Address, actualResult.Address, "Values is not equals!!");
            Assert.AreEqual(expectedResult.FirstName, actualResult.FirstName, "Values is not equals!!");
            Assert.AreEqual(expectedResult.MiddleName, actualResult.MiddleName, "Values is not equals!!");
            Assert.AreEqual(expectedResult.LastName, actualResult.LastName, "Values is not equals!!");
        }

        [TestMethod]
        public void RemoveFrom_EmptyTree()
        {
            // arrange
            var actualResult = true;

            // act
            _tree.Clear();
            actualResult = _tree.Remove("221798000846");

            // assert
            Assert.IsFalse(actualResult, "Actual result is true!!");
        }

        [TestMethod]
        public void Remove_WrongNumber()
        {
            // arrange
            var actualResult = true;

            // act
            _tree.Clear();
            actualResult = _tree.Remove("1");

            // assert
            Assert.IsFalse(actualResult, "Actual result is true!!");
        }

        [TestMethod]
        public void RemoveNode_WithoutChild()
        {
            // arrange 
            var toRemove = "221798000846";
            var actualResult = false;
            var expectedCount = _tree.Count(_tree.Root) - 1;

            // act
            actualResult = _tree.Remove(toRemove);

            // assert
            Assert.IsTrue(actualResult, "Actual result is false!!");
            Assert.AreEqual(expectedCount, _tree.Count(_tree.Root), "Count is not equals!!");
            Assert.IsNull(_tree.Root.LeftChild.RightChild, "Node still in the tree!!");
        }

        [TestMethod]
        public void Remove_WithLeftChild()
        {
            // arrange 
            var toRemove = "970391387168";
            var actualResult = false;
            var expectedCount = _tree.Count(_tree.Root) - 1;
            var expectedNodeValue = _tree.Root.RightChild.RightChild.LeftChild.Value;

            // act
            actualResult = _tree.Remove(toRemove);

            // assert
            Assert.IsTrue(actualResult, "Actual result is false!!");
            Assert.AreEqual(expectedCount, _tree.Count(_tree.Root), "Count is not equals!!");
            Assert.AreEqual(expectedNodeValue, _tree.Root.RightChild.RightChild.Value, "Expected value is not equals actual!!");
        }

        [TestMethod]
        public void Remove_WithRightChild()
        {
            // arrange 
            var toRemove = "141968556032";
            var actualResult = false;
            var expectedCount = _tree.Count(_tree.Root) - 1;
            var expectedNodeValue = _tree.Root.LeftChild.RightChild.Value;

            // act
            actualResult = _tree.Remove(toRemove);

            // assert
            Assert.IsTrue(actualResult, "Actual result is false!!");
            Assert.AreEqual(expectedCount, _tree.Count(_tree.Root), "Count is not equals!!");
            Assert.AreEqual(expectedNodeValue, _tree.Root.LeftChild.Value, "Expected value is not equals actual!!");
        }

        [TestMethod]
        public void Remove_WithTwoChild()
        {
            // arrange 
            var toRemove = "891851272934";
            var actualResult = false;
            var expectedCount = _tree.Count(_tree.Root) - 1;
            var expectedNodeValue = new BookRecord("968454412583", "Dofavhhsa", "Cxhvitanauawt", "Sodmemajmnc", "Mgpvaxyctnhrcle 0");

            // act
            actualResult = _tree.Remove(toRemove);

            // assert
            Assert.IsTrue(actualResult, "Actual result is false!!");
            Assert.AreEqual(expectedCount, _tree.Count(_tree.Root), "Count is not equals!!");
            Assert.AreEqual(expectedNodeValue.PhoneNumber, _tree.Root.RightChild.RightChild.LeftChild.Value.PhoneNumber, "Expected value is not equals actual!!");
        }
    }
}
