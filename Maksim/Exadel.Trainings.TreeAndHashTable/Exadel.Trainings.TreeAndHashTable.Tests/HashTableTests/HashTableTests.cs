﻿using Exadel.Trainings.TreeAndHashTable.Common;
using Exadel.Trainings.TreeAndHashTable.Common.HashTable;
using Exadel.Trainings.TreeAndHashTable.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exadel.Trainings.TreeAndHashTable.Tests.HashTableTests
{
    [TestClass]
    public class HashTableTests
    {
        private MyHashTable _table;

        [TestInitialize]
        public void TestInit()
        {
            var list = FileHelper.ReadData(@"..\..\App_Data\DataForTests.txt");
            _table = new MyHashTable(list);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _table.Clear();
        }

        [TestMethod]
        public void Count()
        {
            // arrange
            var expectedCount = 10;
            var actualCount = 0;

            // act
            actualCount = _table.Count;

            // assert
            Assert.AreEqual(expectedCount, actualCount, "Count is not equals!!");
        }

        [TestMethod]
        public void Clear()
        {
            // arrange
            var expectedCount = 0;
            var actualCount = 10;

            // act
            _table.Clear();
            actualCount = _table.Count;

            // assert
            Assert.AreEqual(expectedCount, actualCount, "Count is not equals!!");
        }

        [TestMethod]
        public void Size()
        {
            // arrange
            var expectedSize = 33;
            var actualSize = 0;

            // act
            actualSize = _table.Size;

            // assert
            Assert.AreEqual(expectedSize, actualSize, "Size is not equals!!");
        }

        [TestMethod]
        public void LoadFactor()
        {
            // arrange
            double expectedLoadFactor = (double)_table.Count / _table.Size;
            double actualLoadFactor = 0;

            // act
            actualLoadFactor = _table.CurrentLoadFactor;

            // assert
            Assert.AreEqual(expectedLoadFactor, actualLoadFactor, 0.1, "LoadFactor is not equals!!");
        }

        [TestMethod]
        public void CollisionCount()
        {
            // arrange
            double expectedCollision = 0;
            double actualCollision = 1000;

            // act
            actualCollision = _table.CollisionsCount;

            // assert
            Assert.AreEqual(expectedCollision, actualCollision, "CollisionsCount is not equals!!");
        }

        [TestMethod]
        public void Contains()
        {
            // arrage
            var actualResult = false;
            var number = "221798000846";

            // act
            actualResult = _table.ContainsValue(number);

            // assert
            Assert.IsTrue(actualResult, "Actual result is not true");
        }

        [TestMethod]
        public void ConstructorWithCountParam()
        {
            // arrange
            var hashTable = new MyHashTable(10);
            var expectedSize = 13;
            var actualSize = 0;

            // act
            actualSize = hashTable.Size;

            // assert
            Assert.AreEqual(expectedSize, actualSize, "Size is not equal!!");
        }

        [TestMethod]
        public void Find_WrongNumber()
        {
            // arrange
            var toFind = "1";
            
            // act 
            var actualResult = _table.Find(toFind);

            // assert
            Assert.IsNull(actualResult, "Something was found!!");
        }

        [TestMethod]
        public void Find()
        {
            // arrange
            var toFind = "221798000846";
            var expectedResult = new BookRecord("221798000846", "Hmcimfeozdo", "Jyeavzw", "Lngeybchfdbmc", "Undmrfztgcpro 9");
            BookRecord actualResult = null;

            // act 
            actualResult = _table.Find(toFind);

            // assert
            Assert.AreEqual(expectedResult.FirstName, actualResult.FirstName, "Expected result is not equals actual!!");
            Assert.AreEqual(expectedResult.MiddleName, actualResult.MiddleName, "Expected result is not equals actual!!");
            Assert.AreEqual(expectedResult.Address, actualResult.Address, "Expected result is not equals actual!!");
        }

        [TestMethod]
        public void FindArrayIndex_WrongNumber()
        {
            // arrange
            var toFind = "1";
            var expectedResult = -1;

            // act 
            var actualResult = _table.FindArrayIndex(toFind);

            // assert
            Assert.AreEqual(expectedResult, actualResult, "Expected result is not equals actual!!");
        }

        [TestMethod]
        public void FindArrayIndex()
        {
            // arrange
            //var toFind = "141968556032";
            var toFind = "891851272934";
            long expectedResult = 9;
            long actualResult = 0;

            // act 
            actualResult = _table.FindArrayIndex(toFind);

            // assert
            Assert.AreEqual(expectedResult, actualResult, "Expected result is not equals actual!!");
        }

        [TestMethod]
        public void Remove_WrongNumber()
        {
            // arrange
            var toRemove = "1";
            var actualResult = true;

            // act
            actualResult = _table.Remove(toRemove);

            // assert
            Assert.IsFalse(actualResult, "Actual result is not false!!");
        }

        [TestMethod]
        public void Remove()
        {
            // arrange
            var toRemove = "968454412583";
            var actualResult = false;
            
            // act
            actualResult = _table.Remove(toRemove);

            // assert
            Assert.IsTrue(actualResult, "Actual result is not true!!");
            Assert.IsFalse(_table.ContainsValue(toRemove), "Hash table consist the removed value!!");
        }
    }
}
