﻿using System;

namespace Exadel.Trainings.CustomLists.Common
{
    /// <summary>
    /// Element of CustomDoublyLinkedList
    /// </summary>
    /// <typeparam name="T">Type of the element</typeparam>
    public class CustomDoublyLinkedListElement<T> : IComparable<T> where T : IComparable
    {
        /// <summary>
        /// Contains value of the element
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// Contains link to the next element in the list
        /// </summary>
        public CustomDoublyLinkedListElement<T> Next { get; set; }

        /// <summary>
        /// Contains link to the previous element in the list
        /// </summary>
        public CustomDoublyLinkedListElement<T> Previous { get; set; }

        /// <summary>
        /// CustomDoublyLinkedListElement constructor
        /// </summary>
        /// <param name="newValue">Value of the new element</param>
        public CustomDoublyLinkedListElement(T newValue)
        {
            Value = newValue;
        }

        /// <summary>
        /// Compares the current instance with another of the type T.
        /// </summary>
        /// <param name="other">An object to compare with this instance.</param>
        /// <returns>
        /// Zero if elements are equal. If value greater than zero - current element greater than other.
        /// If value less than zero - current element less than other.
        ///</returns>
        public int CompareTo(T other)
        {
            if (((dynamic)Value).CompareTo((dynamic)other) > 0)
            {
                return 1;
            }
            else if (((dynamic)Value).CompareTo((dynamic)other) < 0)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
