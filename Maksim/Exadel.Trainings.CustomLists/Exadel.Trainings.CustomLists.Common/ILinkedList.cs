﻿using System.Collections;

namespace Exadel.Trainings.CustomLists.Common
{
    /// <summary>
    /// Intefrace for linked lists.
    /// </summary>
    /// <typeparam name="T">Type of elements.</typeparam>
    public interface ILinkedList <T>
    {
        /// <summary>
        /// Adds an element to the end of the linked list.
        /// </summary>
        /// <param name="newValue">The element to be added to the end of the CustomLinkedList</param>
        void Add(T newValue);

        /// <summary>
        /// Removes the first occurrence of an element with specify data.
        /// </summary>
        /// <param name="data">Data of the element</param>
        /// <returns>
        /// True if element succsesfully removed, false if element not found
        /// </returns>
        bool Remove(T data);

        /// <summary>
        /// Removes the first occurrence of an element with specify index.
        /// </summary>
        /// <param name="index">Index of the element</param>
        void RemoveAt(int index);

        /// <summary>
        /// Adds an element to the specify position of the linked list.
        /// </summary>
        /// <param name="index">Position of the new element</param>
        /// <param name="data">Elements data</param>
        void Append(int index, T data);

        /// <summary>
        /// Adds an element at the _head of the linked list.
        /// </summary>
        /// <param name="data">Elements data</param>
        void AppendFirst(T data);

        /// <summary>
        /// Exchange elements in linked list.
        /// </summary>
        /// <param name="index1">Index of the first element</param>
        /// <param name="index2">Index of the second element</param>
        void ExchangeElements(int index1, int index2);

        /// <summary>
        /// Sorts the elements in linked list by using bubble sort.
        /// </summary>
        void BubbleSortByElements();

        /// <summary>
        /// Reverses the order of the elements in the CustomLinkedList.
        /// </summary>
        void ReverseElements();

        /// <summary>
        /// Returns count elements in the CustomLinkedList.
        /// </summary>
        int Count { get; set; }

        /// <summary>
        /// Check CustomLinkedList on emptiness.
        /// </summary>
        bool IsEmpty();

        /// <summary>
        /// Remove all elements of the linked list.
        /// </summary>
        void Clear();

        /// <summary>
        /// Determines whether an element is in the CustomLinkedList.
        /// </summary>
        /// <param name="data">Elements data</param>
        /// <returns>
        /// true if element is found,
        /// false if element is not found
        /// </returns>
        bool Contains(T data);

        /// <summary>
        /// Search element with specify data in the CustomLinkedList.
        /// </summary>
        /// <param name="data">Elements data</param>
        /// <returns>
        /// Elements position if element was found, else -1
        /// </returns>
        int FindByData(T data);

        /// <summary>
        /// Set or Return elements data.
        /// </summary>
        /// <param name="key">Elements position</param>
        /// <returns>Elements data</returns>
        T this[int key] { get; set; }

        /// <summary>
        /// Used by foreach loop.
        /// </summary>
        /// <returns></returns>
        IEnumerator GetEnumerator();

        /// <summary>
        /// Exchange values of the elements in CustomLinkedList.
        /// </summary>
        /// <param name="index1">Index of the first element</param>
        /// <param name="index2">Index of the second element</param>
        void Exchange(int index1, int index2);

        /// <summary>
        /// Sorts the values of the elements in CustomLinkedList by using insertion sort.
        /// </summary>
        void InsertionSort();

        /// <summary>
        /// Reverses the values of the elements in the CustomLinkedList.
        /// </summary>
        void Reverse();
    }
}
