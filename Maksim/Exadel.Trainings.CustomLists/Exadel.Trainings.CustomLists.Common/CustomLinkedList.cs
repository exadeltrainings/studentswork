﻿using System;
using System.Collections;

namespace Exadel.Trainings.CustomLists.Common
{   
    /// <summary>
    /// Linked List developed by Maksim Berezovskiy. Represents a strongly typed
    /// list of elements. Provides method to search, add and remove.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomLinkedList<T> : ILinkedList<T> where T : IComparable
    {
        /// <summary>
        /// Reference on the begin of the list
        /// </summary>
        private CustomLinkedListElement<T> _head;

        /// <summary>
        /// Returns count elements in the CustomLinkedList.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Check CustomLinkedList on emptiness.
        /// </summary>
        public bool IsEmpty()
        {
            return Count == 0;
        }

        /// <summary>
        /// Remove all elements of the CustomLinkedList.
        /// </summary>
        public void Clear()
        {
            _head = null;
            Count = 0;
        }

        /// <summary>
        /// Adds an element to the end of the CustomLinkedList.
        /// </summary>
        /// <param name="newValue">The element to be added to the end of the CustomLinkedList</param>
        public void Add(T newValue)
        {
            var elem = new CustomLinkedListElement<T>(newValue);

            if (_head == null)
            {
                _head = elem;
            }
            else
            {
                var current = _head;
                while (current.Next != null)
                {
                    current = current.Next;
                }
                current.Next = elem;
            }
            Count++;
        }

        /// <summary>
        /// Removes the first occurrence of an element with specify data.
        /// </summary>
        /// <param name="data">Data of the element</param>
        /// <returns>
        /// True if element succsesfully removed,
        /// false if element not found
        /// </returns>
        public bool Remove(T data)
        {
            var current = _head;
            CustomLinkedListElement<T> previous = null;

            while (current != null)
            {
                if (current.Value.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                    }
                    else
                    {
                        _head = current.Next;
                    }

                    Count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            }

            return false;
        }

        /// <summary>
        /// Removes the first occurrence of an element with specify index.
        /// </summary>
        /// <param name="index">Index of the element</param>
        public void RemoveAt(int index)
        {
            if ((index >= Count || index < 0) || Count == 0) throw new IndexOutOfRangeException();
            if (index == 0)
            {
                _head = _head.Next ?? null;
            }
            else
            {
                var current = _head;
                for (int i = 0; i < index - 1; i++)
                {
                    current = current.Next;
                }

                current.Next = current.Next.Next;
            }
            Count--;
        }

        /// <summary>
        /// Adds an element to the specify position of the CustomLinkedList.
        /// </summary>
        /// <param name="index">Position of the new element</param>
        /// <param name="data">Elements data</param>
        public void Append(int index, T data)
        {
            if (index < 0)
            {
                throw new IndexOutOfRangeException();
            }
            if (index == 0)
            {
                AppendFirst(data);
            }
            else if (index >= Count)
            {
                Add(data);
            }
            else
            {
                var elem = new CustomLinkedListElement<T>(data);

                var current = _head;
                for (int i = 0; i < index - 1; i++)
                {
                    current = current.Next;
                }

                elem.Next = current.Next;
                current.Next = elem;

                Count++;
            }
        }

        /// <summary>
        /// Adds an element at the _head of the CustomLinkedList.
        /// </summary>
        /// <param name="data">Elements data</param>
        public void AppendFirst(T data)
        {
            var elem = new CustomLinkedListElement<T>(data) {Next = _head};

            _head = elem;

            Count++;
        }

        /// <summary>
        /// Determines whether an element is in the CustomLinkedList.
        /// </summary>
        /// <param name="data">Elements data</param>
        /// <returns>
        /// true if element is found,
        /// false if element is not found
        /// </returns>
        public bool Contains(T data)
        {
            var current = _head;
            while (current != null)
            {
                if (current.Value.Equals(data))
                {
                    return true;
                }
                current = current.Next;
            }
            return false;
        }

        /// <summary>
        /// Search element with specify data in the CustomLinkedList.
        /// </summary>
        /// <param name="data">Elements data</param>
        /// <returns>
        /// Elements position if element was found, else -1
        /// </returns>
        public int FindByData(T data)
        {
            var current = _head;
            int index = 0;
            while (current != null)
            {
                if (current.Value.Equals(data))
                {
                    return index;
                }
                current = current.Next;
                index++;
            }

            return -1;
        }

        /// <summary>
        /// Set or Return elements data.
        /// </summary>
        /// <param name="key">Elements position</param>
        /// <returns>Elements data</returns>
        public T this[int key]
        {
            get
            {
                var current = _head;
                for (int i = 0; i < key; i++)
                {
                    current = current.Next;
                }
                return current.Value;
            }
            set
            {
                var current = _head;
                for (int i = 0; i < key; i++)
                {
                    current = current.Next;
                }
                current.Value = value;
            }
        }

        /// <summary>
        /// Used by foreach loop.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            var current = _head;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        /// <summary>
        /// Exchange elements in CustomLinkedList.
        /// </summary>
        /// <param name="index1">Index of the first element</param>
        /// <param name="index2">Index of the second element</param>
        public void ExchangeElements(int index1, int index2)
        {
            if ((index1 > Count - 1 || index1 < 0) || (index2 > Count - 1 || index2 < 0))
            {
                throw new IndexOutOfRangeException();
            }
            if (index1 == index2)
            {
                return;
            }
            if (index1 == 0 || index2 == 0)
            {
                ExchangeElementsIfOneOfIndexesIsZero(index1, index2);
            }
            else
            {
                var preFirst = GetElement(index1 - 1);
                var first = preFirst.Next;

                var preSecond = GetElement(index2 - 1);
                var second = preSecond.Next;

                preFirst.Next = second;
                preSecond.Next = first;
                var tmp = first.Next;
                first.Next = second.Next;
                second.Next = tmp;
            }
        }

        private void ExchangeElementsIfOneOfIndexesIsZero(int index1, int index2)
        {
            var headElement = _head;
            var preElementWithPrevious = index1 == 0 ? GetElement(index2 - 1) : GetElement(index1 - 1);
            var elementWithPrevious = preElementWithPrevious.Next;

            preElementWithPrevious.Next = headElement;
            var tmp = headElement.Next;
            headElement.Next = elementWithPrevious.Next;
            elementWithPrevious.Next = tmp;
            _head = elementWithPrevious;
        }

        /// <summary>
        /// Return the element of the list
        /// </summary>
        /// <param name="index">Element index</param>
        /// <returns>CustomLinkedListElement</returns>
        public CustomLinkedListElement<T> GetElement(int index)
        {
            if (index > Count - 1 || index < 0)
            {
                throw new IndexOutOfRangeException();
            }
            var current = _head;
            for (int i = 0; i < index; i++)
            {
                current = current.Next;
            }

            return current;
        }

        /// <summary>
        /// Sorts the elements in CustomLinkedList by using bubble sort.
        /// </summary>
        public void BubbleSortByElements()
        {
            if (Count == 0) return;
            for (int i = 0; i < Count; i++)
            {
                for (int j = 0; j < Count - i - 1; j++)
                {
                    var a = GetElement(j);
                    var b = GetElement(j + 1);
                    if (a.CompareTo(b.Value) < 0)
                    {
                        ExchangeElements(j, j + 1);
                    }
                }
            }
        }

        /// <summary>
        /// Reverses the order of the elements in the CustomLinkedList.
        /// </summary>
        public void ReverseElements()
        {
            if (Count == 1) return;
            for (int i = 0; i < Count / 2; i++)
            {
                ExchangeElements(i, Count - 1 - i);
            }
        }

        /// <summary>
        /// Exchange values of the elements in CustomLinkedList.
        /// </summary>
        /// <param name="index1">Index of the first element</param>
        /// <param name="index2">Index of the second element</param>
        public void Exchange(int index1, int index2)
        {
            if ((index1 > Count - 1 || index1 < 0) || (index2 > Count - 1 || index2 < 0))
            {
                throw new IndexOutOfRangeException();
            }
            if (index1 == index2)
            {
                return;
            }
            var a = this[index1];
            this[index1] = this[index2];
            this[index2] = a;
        }

        /// <summary>
        /// Sorts the values of the elements in CustomLinkedList by using insertion sort.
        /// </summary>
        public void InsertionSort()
        {
            if (Count == 0) return;
            for (int i = 1; i < Count; i++)
            {
                var current = this[i];
                var j = i;
                while (j > 0 && current.CompareTo(this[j - 1]) < 0)
                {
                    this[j] = this[j - 1];
                    j--;
                }
                this[j] = current;
            }
        }

        /// <summary>
        /// Reverses the values of the elements in the CustomLinkedList.
        /// </summary>
        public void Reverse()
        {
            for (int i = 0; i < Count / 2; i++)
            {
                Exchange(i, Count - 1 - i);
            }
        }
    }
}
