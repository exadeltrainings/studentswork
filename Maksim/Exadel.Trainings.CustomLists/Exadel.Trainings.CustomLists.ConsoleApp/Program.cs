﻿using System;

namespace Exadel.Trainings.CustomLists.ConsoleApp
{
    static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MainInternal();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }

            Console.ReadKey();
        }

        static void MainInternal()
        {
            var flag = true;
            ConsoleHelper cs;
            while (flag)
            {
                Console.Clear();
                Console.WriteLine("1. Linked list");
                Console.WriteLine("2. Doubly linked list");
                Console.WriteLine("3. Exit");
                Console.WriteLine("Choose menu item...");
                switch (Console.ReadLine())
                {
                    case "1":
                        cs = new ConsoleHelper("Linked");
                        Console.Clear();
                        cs.WorkWithLinkedList();
                        break;
                    case "2":
                        cs = new ConsoleHelper();
                        Console.Clear();
                        cs.WorkWithLinkedList();
                        break;
                    case "3":
                        flag = false;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
