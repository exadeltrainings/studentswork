﻿using Exadel.Trainings.CustomLists.Common;
using System;

namespace Exadel.Trainings.CustomLists.ConsoleApp
{
    /// <summary>
    /// Class that works with Console
    /// </summary>
    public class ConsoleHelper
    {
        private ILinkedList<int> _list;

        public ConsoleHelper(string listName = "DoublyLinked")
        {
            if (listName.Equals("DoubliLinked"))
            {
                _list = new CustomDoublyLinkedList<int>();
            }
            else
            {
                _list = new CustomLinkedList<int>();
            }
        }

        /// <summary>
        /// This method represents work with CustomLinkedList
        /// </summary>
        public void WorkWithLinkedList()
        {
            var flag = true;
            while (flag)
            {
                PrintMenu();

                int answer;
                int.TryParse(Console.ReadLine(), out answer);

                switch (answer)
                {
                    case 1:
                        Console.Clear();
                        AddElement();
                        Print();
                        break;
                    case 2:
                        Console.Clear();
                        AppendElement();
                        Print();
                        break;
                    case 3:
                        Console.Clear();
                        AppendElementAtTheFirstPosition();
                        Print();
                        break;
                    case 4:
                        Console.Clear();
                        Print();
                        RemoveByPosition();
                        Print();
                        break;
                    case 5:
                        Console.Clear();
                        Print();
                        RemoveByData();
                        Print();
                        break;
                    case 6:
                        Console.Clear();
                        Count();
                        break;
                    case 7:
                        Console.Clear();
                        FindByValue();
                        break;
                    case 8:
                        Console.Clear();
                        FindByPosition();
                        break;
                    case 9:
                        Console.Clear();
                        Print();
                        ExchangeByValue();
                        Print();
                        break;
                    case 10:
                        Console.Clear();
                        Print();
                        ExchangeByElement();
                        Print();
                        break;
                    case 11:
                        Console.Clear();
                        Contains();
                        break;
                    case 12:
                        Console.Clear();
                        IsEmpty();
                        break;
                    case 13:
                        Console.Clear();
                        Print();
                        IntensionSort();
                        Print();
                        break;
                    case 14:
                        Console.Clear();
                        Print();
                        ExchangeSort();
                        Print();
                        break;
                    case 15:
                        Console.Clear();
                        Print();
                        ReverseByValue();
                        Print();
                        break;
                    case 16:
                        Console.Clear();
                        Print();
                        ReverseByElement();
                        Print();
                        break;
                    case 17:
                        Console.Clear();
                        Clear();
                        break;
                    case 18:
                        Console.Clear();
                        Print();
                        break;
                    case 19:
                        flag = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private void AddElement()
        {
            int element;
            Console.WriteLine("Enter element : ");
            if (int.TryParse(Console.ReadLine(), out element))
            {
                _list.Add(element);
                Console.WriteLine("Element was successfully added");
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
        }

        private void AppendElement()
        {
            int element;
            int position;
            Console.WriteLine("Enter element and position : ");
            if (int.TryParse(Console.ReadLine(), out element) && int.TryParse(Console.ReadLine(), out position))
            {
                try
                {
                    _list.Append(position - 1, element);
                    Console.WriteLine("Element was successfully added");
                }
                catch (Exception)
                {
                    Console.WriteLine("Wrong position!");
                }
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
        }

        private void AppendElementAtTheFirstPosition()
        {
            int element;
            Console.WriteLine("Enter value of the element :");
            if (int.TryParse(Console.ReadLine(), out element))
            {
                _list.AppendFirst(element);
                Console.WriteLine("Element was successfully added");
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
        }

        private void RemoveByPosition()
        {
            int position;
            Console.WriteLine("Enter position :");
            if (int.TryParse(Console.ReadLine(), out position))
            {
                try
                {
                    _list.RemoveAt(position - 1);
                    Console.WriteLine("Element was succesfully removed");
                }
                catch (IndexOutOfRangeException e1)
                {

                }
                catch
                {
                    Console.WriteLine("Incorrect position!");
                    throw;
                }
                finally
                {
                    
                }
            }
            else
            {
                Console.WriteLine("Something went wrong!");
            }
        }

        private void RemoveByData()
        {
            int element;
            Console.WriteLine("Enter value :");
            if (int.TryParse(Console.ReadLine(), out element))
            {
                if (_list.Remove(element))
                {
                    Console.WriteLine("Element was succesfully removed");
                }
                else
                {
                    Console.WriteLine("Element wasn't removed");
                }
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
        }

        private void Count()
        {
            Console.WriteLine("List contains " + _list.Count + " elements");
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        private void FindByValue()
        {
            int value;
            Console.WriteLine("Enter value of the element :");
            if (int.TryParse(Console.ReadLine(), out value))
            {
                var position = _list.FindByData(value);
                if (position != -1)
                {
                    Console.WriteLine("Element with value {0} on position {1}", value, position);
                }
                else
                {
                    Console.WriteLine("List doesn't contain element with such value!");
                }
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        private void FindByPosition()
        {
            int position;
            Console.WriteLine("Enter element position :");
            if (int.TryParse(Console.ReadLine(), out position))
            {
                Console.WriteLine("Element at position {0} has value {1}", position, _list[position]);
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        private void ExchangeByValue()
        {
            int firstPosition;
            int secondPosition;
            Console.WriteLine("Enter first and second elements position :");
            Console.WriteLine("Enter the position of the first element and the second element :");
            if (int.TryParse(Console.ReadLine(), out firstPosition) &&
                int.TryParse(Console.ReadLine(), out secondPosition))
            {
                try
                {
                    _list.Exchange(firstPosition, secondPosition);
                    Console.WriteLine("Elements were successfully exchanged");
                }
                catch (Exception)
                {
                    Console.WriteLine("Incorrect input!");    
                } 
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
        }

        private void ExchangeByElement()
        {
            int firstPosition;
            int secondPosition;
            Console.WriteLine("Enter the position of the first element and the second element :");
            if (int.TryParse(Console.ReadLine(), out firstPosition) && int.TryParse(Console.ReadLine(), out secondPosition))
            {
                try
                {
                    _list.ExchangeElements(firstPosition, secondPosition);
                    Console.WriteLine("Elements were successfully exchanged");
                }
                catch (Exception)
                {
                    Console.WriteLine("Incorrect input!");    
                }
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
        }

        private void Contains()
        {
            int value;
            Console.WriteLine("Enter value of the element :");
            if (int.TryParse(Console.ReadLine(), out value))
            {
                if (_list.Contains(value))
                {
                    Console.WriteLine("List contain value " + value);    
                }
                else
                {
                    Console.WriteLine("List doesn't contain value " + value);
                }
            }
            else
            {
                Console.WriteLine("Incorrect input!");
            }
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        private void IsEmpty()
        {
            if (_list.IsEmpty())
            {
                Console.WriteLine("List is empty");
            }
            else
            {
                Console.WriteLine("List is not empty");
            }
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        private void IntensionSort()
        {
            _list.InsertionSort();
            Console.WriteLine("List was sorted");
        }

        private void ExchangeSort()
        {
            _list.BubbleSortByElements();
            Console.WriteLine("List was sorted");
        }

        private void ReverseByValue()
        {
            _list.Reverse();
            Console.WriteLine("List was reversed");
        }

        private void ReverseByElement()
        {
            _list.ReverseElements();
            Console.WriteLine("List was reversed");
        }

        private void Clear()
        {
            _list.Clear();
            Console.WriteLine("List was cleared. Now list is empty");
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        private void Print()
        {
            Console.WriteLine("List :");
            foreach (var i in _list)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        private static void PrintMenu()
        {
            Console.Clear();
            Console.WriteLine("1. Add the element to the list");
            Console.WriteLine("2. Append the element to the list");
            Console.WriteLine("3. Append the element at the first position in the list");
            Console.WriteLine("4. Remove an element from the list by position");
            Console.WriteLine("5. Remove an element from the list by value");
            Console.WriteLine("6. Count of elements in the list");
            Console.WriteLine("7. Find an element position in the list, using value");
            Console.WriteLine("8. Find an element value, using element position");
            Console.WriteLine("9. Exchange elements in the list, using value");
            Console.WriteLine("10. Exchange elements in the list, using element");
            Console.WriteLine("11. Check the list for containing value");
            Console.WriteLine("12. Check the list for emptiness");
            Console.WriteLine("13. Sort the list, using insertion sort");
            Console.WriteLine("14. Sort the list, using exchange sort");
            Console.WriteLine("15. Reverse the list, using value");
            Console.WriteLine("16. Reverse the list, using element");
            Console.WriteLine("17. Clear the list");
            
            Console.WriteLine("18. Print the list");
            Console.WriteLine("19. Exit");
            Console.WriteLine();
            Console.WriteLine("Choose menu item...");
        }
    }
}
