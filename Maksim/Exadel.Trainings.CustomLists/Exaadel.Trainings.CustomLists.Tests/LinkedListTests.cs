﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exadel.Trainings.CustomLists.Common;

namespace Exaadel.Trainings.CustomLists.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        CustomLinkedList<int> list = new CustomLinkedList<int>();
        Random random = new Random();

        [TestInitialize]
        public void TestInit()
        {
            var rand = new Random(10);
            for (int i = 0; i < 5; i++)
            {
                list.Add(rand.Next());
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            list.Clear();
        }

        [TestMethod]
        public void List_Count()
        {
            // arrange
            var actualListCount = 0;
            var expectedListCount = 5;

            // act
            actualListCount = list.Count;

            // assert
            Assert.AreEqual(actualListCount, expectedListCount, "List.Count works wrong!");
        }

        [TestMethod]
        public void List_IsEmpty()
        {
            // assert
            Assert.IsFalse(list.IsEmpty(), "IsEmpty works wrong!");
        }

        [TestMethod]
        public void List_Clear()
        {
            // arrange
            var expectedCount = 0;

            // act
            list.Clear();

            // assert
            Assert.AreEqual(list.Count, expectedCount, "Count is not equals!");
        }

        [TestMethod]
        public void Add_ToList()
        {
            // arrange
            var actualValue = random.Next(100);
            var expectedValue = actualValue;
            var expectedListCount = list.Count + 1;

            // act
            list.Add(actualValue);

            // assert
            Assert.IsTrue(list.Contains(expectedValue), "List doesn't contain expected value!");
            Assert.AreEqual(expectedListCount, list.Count, "Count is not equals!");
            Assert.AreEqual(expectedValue, list[list.Count - 1], "Expected value has wrong index!");
        }

        [TestMethod]
        public void Add_ToEmptyList()
        {
            // arrange
            var actualValue = random.Next(100);
            var expectedValue = actualValue;
            var expectedIndex = 0;
            var expectedCount = 1;

            // act
            list.Clear();
            list.Add(actualValue);

            // assert
            Assert.IsTrue(list.Contains(expectedValue), "List doesn't contain expected value!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
            Assert.AreEqual(expectedValue, list[expectedIndex], "Wrong element in the expected index!");
        }

        [TestMethod]
        public void Remove_MiddleElement_FromList()
        {
            // arrange
            var actualValueIndex = 2;
            var actualValue = list[actualValueIndex];
            var expectedValue = list[actualValueIndex + 1];
            var expectedValueIndex = actualValueIndex;
            var expectedCount = list.Count - 1;
            var expectedResult = false;

            // act
            expectedResult = list.Remove(actualValue);

            // assert
            Assert.IsTrue(expectedResult, "Expected result is false!");
            Assert.AreEqual(expectedValue, list[expectedValueIndex], "Wrong element at the removed element position!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        public void Remove_FirstElementFrom_List()
        {
            // arrange
            var actualValueIndex = 0;
            var actualValue = list[actualValueIndex];
            var expectedValue = list[actualValueIndex + 1];
            var expectedValueIndex = actualValueIndex;
            var expectedCount = list.Count - 1;
            var expectedResult = false;

            // act
            expectedResult = list.Remove(actualValue);

            // assert
            Assert.IsTrue(expectedResult, "Expected result is false!");
            Assert.AreEqual(expectedValue, list[expectedValueIndex], "Wrong element at the removed element position!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        public void Remove_LastElementFrom_List()
        {
            // arrange
            var actualValueIndex = 4;
            var actualValue = list[actualValueIndex];
            var expectedCount = list.Count - 1;
            var expectedResult = false;

            // act
            expectedResult = list.Remove(actualValue);

            // assert
            Assert.IsTrue(expectedResult, "Expected result is false!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        public void RemoveFrom_EmptyList()
        {
            // arrange
            var actualIndex = 1;
            var expectedResult = true;

            // act
            list.Clear();
            expectedResult = list.Remove(actualIndex);

            // assert
            Assert.IsFalse(expectedResult, "Expected result is true!");
        }

        [TestMethod]
        public void RemoveAt_FirstIndex()
        {
            // arrange
            var actualElementPosition = 0;
            var expectedValue = list[actualElementPosition + 1];
            var expectedCount = list.Count - 1;

            // act
            list.RemoveAt(actualElementPosition);

            // assert
            Assert.AreEqual(expectedValue, list[actualElementPosition], "Expected value not equals actual value!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        public void RemoveAt_IndexFrom_List()
        {
            // arrange
            var actualElementPosition = 2;
            var expectedValue = list[actualElementPosition + 1];
            var expectedCount = list.Count - 1;

            // act
            list.RemoveAt(actualElementPosition);

            // assert
            Assert.AreEqual(expectedValue, list[actualElementPosition], "Expected value not equals actual value!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        public void RemoveAt_LastIndexFrom_List()
        {
            // arrange
            var actualElementPosition = 4;
            var expectedCount = list.Count - 1;

            // act
            list.RemoveAt(actualElementPosition);

            // assert
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException), "Attempt to remove element from empty list!")]
        public void RemoveAtIndexFrom_EmptyList_Or_WrongIndex()
        {
            // arrange
            var actualPosition = random.Next(100);

            // act
            list.Clear();
            list.RemoveAt(actualPosition);
        }

        [TestMethod]
        public void AppendElementInto_List()
        {
            // arrange
            var actualValue = 0;
            var actualIndex = 2;
            var expectedIndex = 2;
            var expectedValue = 0;
            var expectedCount = list.Count + 1;

            // act
            list.Append(actualIndex, actualValue);

            // assert
            Assert.IsTrue(list.Contains(expectedValue), "List doesn't contain expected value!");
            Assert.AreEqual(expectedValue, list[expectedIndex], "Expected value has wrong index!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        public void AppendElementInto_EmptyList()
        {
            // arrange
            var actualValue = -1;
            var actualIndex = 0;
            var expectedIndex = 0;
            var expectedValue = -1;
            var expectedCount = 1;

            // act
            list.Clear();
            list.Append(actualIndex, actualValue);

            // assert
            Assert.IsTrue(list.Contains(expectedValue), "List doesn't contain expected value!");
            Assert.AreEqual(expectedValue, list[expectedIndex], "Expected value has wrong index!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException), "Element was inappropriately added!")]
        public void AppendElementAt_WrongIndex_LessThenZero()
        {
            // arrange
            var actualValue = 0;
            var actualIndex = -1;
            var expectedCount = list.Count;

            // act
            list.Append(actualIndex, actualValue);
        }

        [TestMethod]
        public void AppendElementAt_WrongIndex_GreaterThen_ListCount()
        {
            // arrange
            var actualValue = 0;
            var actualIndex = 10;
            var expectedIndex = list.Count;
            var expectedValue = 0;
            var expectedCount = list.Count + 1;

            // act
            list.Append(actualIndex, actualValue);

            // assert
            Assert.IsTrue(list.Contains(expectedValue), "List doesn't contain expected value!");
            Assert.AreEqual(expectedValue, list[expectedIndex], "Expected value was not found on the expected position!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
        }

        [TestMethod]
        public void AppendFirstElement()
        {
            // arrange
            var actualValue = -1;
            var expectedValue = -1;
            var expectedIndex = 0;
            var expectedAfterFirstElement = list[0];
            var expectedIndexAfterFirstElementValue = 1;
            var expectedCount = list.Count + 1;

            // act
            list.AppendFirst(actualValue);

            // assert
            Assert.IsTrue(list.Contains(expectedValue), "List doesn't contain expected value!");
            Assert.AreEqual(expectedValue, list[expectedIndex], "Expected value has wrong index!");
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
            Assert.AreEqual(expectedAfterFirstElement, list[expectedIndexAfterFirstElementValue], "Wrong value after first element!");
        }

        [TestMethod]
        public void ContainsElementInTheList()
        {
            // arrange
            var existingValueInList = list[random.Next(4)];
            var expectedResult = false;

            // act
            expectedResult = list.Contains(existingValueInList);

            // assert
            Assert.IsTrue(expectedResult, "Expected result is not true!");
        }

        [TestMethod]
        public void NotContainsElementInTheList()
        {
            // arrange
            var existingValueInList = -9999;
            var expectedResult = true;

            // act
            expectedResult = list.Contains(existingValueInList);

            // assert
            Assert.IsFalse(expectedResult, "Expected result is not true!");
        }

        [TestMethod]
        public void FindElementBy_Data()
        {
            // arrange
            var actualIndex = random.Next(4); 
            var actualData = list[actualIndex];
            var expectedResult = 0;

            // act
            expectedResult = list.FindByData(actualData);

            // assert
            Assert.AreEqual(expectedResult, actualIndex, "Expected result is not equals actual!");
        }

        [TestMethod]
        public void FindElementBy_NotExistingData()
        {
            // arrange
            var actualResult = 0;
            var actualData = -9999;
            var expectedResult = -1;

            // act
            actualResult = list.FindByData(actualData);

            // assert
            Assert.AreEqual(expectedResult, actualResult, "Expected -1, but actual admissible value!");
        }

        [TestMethod]
        public void ExchangeValueOfTheElementsInTheList()
        {
            // arrange
            var firstValueIndex = random.Next(4);
            var secondValueIndex = random.Next(4);
            var firstValue = list[firstValueIndex];
            var secondValue = list[secondValueIndex];

            // act
            list.Exchange(firstValueIndex, secondValueIndex);

            // assert
            Assert.AreEqual(secondValue, list[firstValueIndex], "Expected element is not equals actual!");
            Assert.AreEqual(firstValue, list[secondValueIndex], "Expected element is not equals actual!");
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException), "Exchange elements value with wrong indexes was inappropriately allowed!")]
        public void ExchangeElements_WithWrongIndexes_InTheList()
        {
            // arrange
            var firstValueIndex = random.Next(1000);
            var secondValueIndex = random.Next(1000);
            
            // act
            list.Exchange(firstValueIndex, secondValueIndex);
        }

        [TestMethod]
        public void GetElement()
        {
            // arrange
            CustomLinkedListElement<int> actualElement;
            var actualIndex = random.Next(4);
            var expectedValue = list[actualIndex];
            
            // act
            actualElement = list.GetElement(actualIndex);

            // assert
            Assert.AreEqual(expectedValue, actualElement.Value, "Values are not equal");
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException), "Obtaining element with wrong index was inappropriately allowed!")]
        public void GetElementByWrongIndex()
        {
            // arrange
            var actualIndex = 100;

            // act
            list.GetElement(actualIndex);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException), "Exchange elements with wrong indexes was inappropriately allowed!")]
        public void ExchangeElements_OneHasWrongIndex()
        {
            // arrange
            var firstElementIndex = 0;
            var secondElementIndex = 100;

            // act
            list.ExchangeElements(firstElementIndex, secondElementIndex);
        }

        [TestMethod]
        public void ExchangeElements_OneElementFirst()
        {
            // arrange
            var firstElementIndex = 0;
            var secondElementIndex = 2;
            var expectedFirstElement = list.GetElement(secondElementIndex);
            var expectedSecondElement = list.GetElement(firstElementIndex);

            // act
            list.ExchangeElements(firstElementIndex, secondElementIndex);

            // assert
            Assert.AreSame(expectedFirstElement, list.GetElement(firstElementIndex), "First element are not equals expected element");
            Assert.AreSame(expectedSecondElement, list.GetElement(secondElementIndex), "Second element are not equals expected element");
        }

        [TestMethod]
        public void ExchangeElements_OneElementLast()
        {
            // arrange
            var firstElementIndex = 1;
            var secondElementIndex = list.Count - 1;
            var expectedFirstElement = list.GetElement(secondElementIndex);
            var expectedSecondElement = list.GetElement(firstElementIndex);

            // act
            list.ExchangeElements(firstElementIndex, secondElementIndex);

            // assert
            Assert.AreSame(expectedFirstElement, list.GetElement(firstElementIndex), "First element are not equals expected element");
            Assert.AreSame(expectedSecondElement, list.GetElement(secondElementIndex), "Second element are not equals expected element");
        }

        [TestMethod]
        public void ExchangeRandomElements()
        {
            // arrange
            var firstElementIndex = random.Next(4);
            var secondElementIndex = random.Next(4);
            var expectedFirstElement = list.GetElement(secondElementIndex);
            var expectedSecondElement = list.GetElement(firstElementIndex);

            // act
            list.ExchangeElements(firstElementIndex, secondElementIndex);

            // assert
            Assert.AreSame(expectedFirstElement, list.GetElement(firstElementIndex), "First element are not equals expected element");
            Assert.AreSame(expectedSecondElement, list.GetElement(secondElementIndex), "Second element are not equals expected element");
        }

        [TestMethod]
        public void BubbleSortByElement()
        {
            // arrange
            var firstIndex = random.Next(3);
            var secondIndex = random.Next(3);
            var expectedCount = list.Count;

            // act
            list.BubbleSortByElements();

            //assert
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
            Assert.IsTrue(list.GetElement(firstIndex).CompareTo(list.GetElement(firstIndex + 1).Value) >= 0, "Current element greater than next!");
            Assert.IsTrue(list.GetElement(secondIndex).CompareTo(list.GetElement(secondIndex + 1).Value) >= 0, "Current element greater than next!");
        }

        [TestMethod]
        public void ReverseElementsInTheList()
        {
            // arrange
            var expectedLastElement = list.GetElement(0);
            var expectedFirstElement = list.GetElement(list.Count - 1);
            var expectedCount = list.Count;

            // act
            list.ReverseElements();

            // assert
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
            Assert.AreEqual(expectedLastElement.Value, list.GetElement(list.Count - 1).Value, "Expected last element is not equals actual!");
            Assert.AreEqual(expectedFirstElement.Value, list.GetElement(0).Value, "Expected first element is not equals actual!");
        }

        [TestMethod]
        public void IntensionSort()
        {
            // arrange
            var firstIndex = random.Next(3);
            var secondIndex = random.Next(3);
            var expectedCount = list.Count;

            // act
            list.InsertionSort();

            //assert
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
            Assert.IsTrue(list[firstIndex] <= list[firstIndex + 1], "Current element value greater than next!");
            Assert.IsTrue(list[secondIndex] <= list[secondIndex + 1], "Current element value greater than next!");
        }

        [TestMethod]
        public void ReverseList()
        {
            // arrange
            var expectedLastElement = list[0];
            var expectedFirstElement = list[list.Count - 1];
            var expectedCount = list.Count;

            // act
            list.Reverse();

            // assert
            Assert.AreEqual(expectedCount, list.Count, "Count is not equals!");
            Assert.AreEqual(expectedLastElement, list[list.Count - 1], "Expected last element value is not equals actual!");
            Assert.AreEqual(expectedFirstElement ,list[0], "Expected first element value is not equals actual!");
        }
    }
}
