﻿using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using System.Collections.Generic;

namespace Exadel.Trainings.MetroWinApp.BLL.Interfaces
{
    /// <summary>
    /// Provides basic methods to auth service.
    /// </summary>
    public interface IAuthProvider
    {
        /// <summary>
        /// Returns all users in db.
        /// </summary>
        /// <returns>Users collection.</returns>
        IEnumerable<User> GetAllUsers();

        /// <summary>
        /// User verification on the existence in the database.
        /// </summary>
        /// <param name="user">User data, that need to check.</param>
        /// <returns>If user exists - return all information about him, otherwise - null.</returns>
        User IsUser(User user);

        /// <summary>
        /// Register new user.
        /// </summary>
        /// <param name="newUser">User data, that need to register.</param>
        /// <returns>True if registration is success, otherwise - false.</returns>
        bool Register(User newUser);

        /// <summary>
        /// Username verification on the existence in the database.
        /// </summary>
        /// <param name="username">Username, that need to check.</param>
        /// <returns>True if username in the db, otherwise - false.</returns>
        bool HaveUserWithSuchName(string username);
    }
}
