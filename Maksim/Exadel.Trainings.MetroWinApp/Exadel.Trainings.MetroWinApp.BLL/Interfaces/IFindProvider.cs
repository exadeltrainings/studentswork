﻿using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using System.Collections.Generic;

namespace Exadel.Trainings.MetroWinApp.BLL.Interfaces
{
    /// <summary>
    /// IFindService provide basic methods to find servises.
    /// </summary>
    public interface IFindService
    {
        /// <summary>
        /// Returns collection of cars with specify parameters. If parameter equals null, it will not be used in selection.
        /// </summary>
        /// <param name="mark">Car's mark.</param>
        /// <param name="model">Car's model.</param>
        /// <param name="country">Car's producer country.</param>
        /// <param name="fromPrice">Car's minimum price.</param>
        /// <param name="toPrice">Car's maximum price.</param>
        /// <param name="fromYear">Car's minimum year.</param>
        /// <param name="toYear">Car's maximum year.</param>
        /// <param name="transmission">Car's transmission.</param>
        /// <param name="bodyType">Car's body type.</param>
        /// <returns>Collection of cars.</returns>
        IEnumerable<Car> FindCars(object mark, object model, object country, string fromPrice, string toPrice, object fromYear
            , object toYear, object transmission, object bodyType);
    }
}
