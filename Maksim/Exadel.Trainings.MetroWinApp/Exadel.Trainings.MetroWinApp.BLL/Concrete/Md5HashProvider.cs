﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Exadel.Trainings.MetroWinApp.BLL.Concrete
{
    /// <summary>
    /// Class, used to hashing user's data.
    /// </summary>
    public static class Md5HashProvider
    {
        /// <summary>
        /// Generate MD5 hash code as a 32 character hexadecimal string.
        /// </summary>
        /// <param name="input">The string, which need to hash.</param>
        /// <returns>Hashed 32 character hexadecimal string.</returns>
        public static string GenerateMd5Hash(string input)
        {
            var md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            var hashedString = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                hashedString.Append(data[i].ToString("x2"));
            }

            return hashedString.ToString();
        }

        /// <summary>
        /// Verifies string with a hash code.
        /// </summary>
        /// <param name="input">The string, which need to verify.</param>
        /// <param name="hash">Hash, with which need to verify.</param>
        /// <returns>True if input and hash equals, otherwise - false.</returns>
        public static bool verifyMd5Hash(string input, string hash)
        {
            string hashOfInput = GenerateMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            var comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
