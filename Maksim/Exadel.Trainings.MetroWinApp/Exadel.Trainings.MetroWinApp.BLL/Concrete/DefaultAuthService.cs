﻿using Exadel.Trainings.MetroWinApp.BLL.Interfaces;
using System.Collections.Generic;
using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using Exadel.Trainings.MetroWinApp.DAL.Interfaces;
using Exadel.Trainings.MetroWinApp.DAL;

namespace Exadel.Trainings.MetroWinApp.BLL.Concrete
{
    public class DefaultAuthService : IAuthProvider
    {
        private IUnitOfWork _uofw;

        public DefaultAuthService()
        {
            _uofw = new UnitOfWork();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _uofw.Users.GetAll();
        }

        public bool HaveUserWithSuchName(string username)
        {
            var users = GetAllUsers();
            foreach (var u in users)
            {
                if (u.Username == username)
                {
                    return true;
                }
            }

            return false;
        }

        public User IsUser(User user)
        {
            var users = GetAllUsers();
            foreach (var u in users)
            {
                if (u.Username == user.Username && Md5HashProvider.verifyMd5Hash(user.Password, u.Password))
                {
                    return u;
                }
            }

            return null;
        }

        public bool Register(User newUser)
        {
            if (!HaveUserWithSuchName(newUser.Username))
            {
                _uofw.Users.Add(newUser);
                _uofw.Save();
                return true;
            }
            else return false;
        }
    }
}
