﻿using Exadel.Trainings.MetroWinApp.BLL.Interfaces;
using Exadel.Trainings.MetroWinApp.DAL;
using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using Exadel.Trainings.MetroWinApp.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exadel.Trainings.MetroWinApp.BLL.Concrete
{
    public class DefaultFindService : IFindService
    {
        private IUnitOfWork _unitOfWork;

        public DefaultFindService()
        {
            _unitOfWork = new UnitOfWork();
        }

        public IEnumerable<Car> FindCars(object mark, object model, object country, string fromPrice, string toPrice, object fromYear, 
            object toYear, object transmission, object bodyType)
        {
            var cars = _unitOfWork.Cars.GetAll().ToList();
            if (mark != null)
            {
                cars = cars.FindAll(c => c.Model.Mark.Mark1 == ((Mark)mark).Mark1);
            }
            if (model != null)
            {
                cars = cars.FindAll(c => c.Model.Model1 == model.ToString());
            }
            if (country != null)
            {
                cars = cars.FindAll(c => c.Producer.Country.Country1 == ((Country)country).Country1);
            }
            if (fromPrice != "")
            {
                cars = cars.FindAll(c => c.Price >= Convert.ToDouble(fromPrice));
            }
            if (toPrice != "")
            {
                cars = cars.FindAll(c => c.Price <= Convert.ToDouble(toPrice));
            }
            if (fromYear != null)
            {
                cars = cars.FindAll(c => c.Year >= Convert.ToInt32(fromYear));
            }
            if (toYear != null)
            {
                cars = cars.FindAll(c => c.Year <= Convert.ToInt32(toYear));
            }
            if (transmission != null)
            {
                cars = cars.FindAll(c => c.Transmission.Transmission1 == ((Transmission)transmission).Transmission1);
            }
            if (bodyType != null)
            {
                cars = cars.FindAll(c => c.Body.BodyType == ((Body)bodyType).BodyType);
            }

            return cars;
        }
    }
}
