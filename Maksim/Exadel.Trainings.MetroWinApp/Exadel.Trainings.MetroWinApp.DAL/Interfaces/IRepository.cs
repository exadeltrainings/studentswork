﻿using System;
using System.Collections.Generic;

namespace Exadel.Trainings.MetroWinApp.DAL.Interfaces
{
    /// <summary>
    /// Generic IRepository provides basic methods to the repositories.
    /// </summary>
    /// <typeparam name="T">Class of the repository.</typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Returns element with specify id.
        /// </summary>
        /// <param name="id">Id ot the element, that we need.</param>
        /// <returns>Element of a T type, with specify id.</returns>
        T Get(int id);
    
        /// <summary>
        /// Returns all elements from a table.
        /// </summary>
        /// <returns>List of elements.</returns>
        IEnumerable<T> GetAll();
    
        /// <summary>
        /// Finds element using specify predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Element of a T type.</returns>
        IEnumerable<T> Find(Func<T, Boolean> predicate);
    
        /// <summary>
        /// Adds new row into the database table.
        /// </summary>
        /// <param name="item">Element that's data need to add.</param>
        void Add(T item);
    
        /// <summary>
        /// Adds rows into the database table.
        /// </summary>
        /// <param name="items">Collection of the elements, that's data need to add.</param>
        void AddRange(IEnumerable<T> items);
    
        /// <summary>
        /// Removes row from a database table.
        /// </summary>
        /// <param name="item">Element that need to remove.</param>
        void Remove(T item);
    
        /// <summary>
        /// Removes rows from a database table.
        /// </summary>
        /// <param name="items">Collection of the elements, that's data need to remove.</param>
        void RemoveRange(IEnumerable<T> items);
    }
}
