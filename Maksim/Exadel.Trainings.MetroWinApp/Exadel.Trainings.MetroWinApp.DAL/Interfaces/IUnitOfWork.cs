﻿using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using System;

namespace Exadel.Trainings.MetroWinApp.DAL.Interfaces
{
    /// <summary>
    /// Must be implemented to work with pattern unit of work.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Contains all body types.
        /// </summary>
        IRepository<Body> Bodies { get; }

        /// <summary>
        /// Contains all cars.
        /// </summary>
        IRepository<Car> Cars { get; }

        /// <summary>
        /// Contains all countries.
        /// </summary>
        IRepository<Country> Countries { get; }

        /// <summary>
        /// Contains all marks.
        /// </summary>
        IRepository<Mark> Marks { get; }

        /// <summary>
        /// Contains all models.
        /// </summary>
        IRepository<Model> Models { get; }

        /// <summary>
        /// Contains all producers.
        /// </summary>
        IRepository<Producer> Producers { get; }

        /// <summary>
        /// Contains all transmissions.
        /// </summary>
        IRepository<Transmission> Transmissions { get; }

        /// <summary>
        /// Contains all users.
        /// </summary>
        IRepository<User> Users { get; }

        /// <summary>
        /// Method that will be used to save changes.
        /// </summary>
        void Save();
    }
}
