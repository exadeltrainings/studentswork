﻿using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Concrete;
using Exadel.Trainings.MetroWinApp.DAL.Interfaces;

namespace Exadel.Trainings.MetroWinApp.DAL
{
    /// <summary>
    /// Implementation IUnitOfWork interface.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private DbEntities _db;

        /// <summary>
        /// UnitOfWork constructor.
        /// </summary>
        /// <param name="context">Ef AutoShop context.</param>
        public UnitOfWork(/*DbEntities db = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString*/)
        {
            _db = new DbEntities(); /*db;*/

            Countries = new Repository<Country>(_db);
            Bodies = new Repository<Body>(_db);
            Cars = new Repository<Car>(_db);
            Marks = new Repository<Mark>(_db);
            Models = new Repository<Model>(_db);
            Producers = new Repository<Producer>(_db);
            Transmissions = new Repository<Transmission>(_db);
            Users = new Repository<User>(_db);
        }

        /// <summary>
        /// Body types repository.
        /// </summary>
        public IRepository<Body> Bodies { get; private set; }

        /// <summary>
        /// Car Repository.
        /// </summary>
        public IRepository<Car> Cars { get; private set; }

        /// <summary>
        /// Countries repository.
        /// </summary>
        public IRepository<Country> Countries { get; private set; }

        /// <summary>
        /// Mark Repository.
        /// </summary>
        public IRepository<Mark> Marks { get; private set; }

        /// <summary>
        /// Model Repository.
        /// </summary>
        public IRepository<Model> Models { get; private set; }

        /// <summary>
        /// Producer Repository.
        /// </summary>
        public IRepository<Producer> Producers { get; private set; }

        /// <summary>
        /// Transmissions repository.
        /// </summary>
        public IRepository<Transmission> Transmissions { get; private set; }

        /// <summary>
        /// Represents app users.
        /// </summary>
        public IRepository<User> Users { get; set; }

        /// <summary>
        /// Saves all changes.
        /// </summary>
        public void Save()
        {
            _db.SaveChanges();
        }

        /// <summary>
        /// Dispose UnitOfWork.
        /// </summary>
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
