﻿using Exadel.Trainings.MetroWinApp.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Concrete
{
    /// <summary>
    /// Represents generic version of the repository. Implements IRepository.
    /// </summary>
    /// <typeparam name="TEntity">Custom model.</typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _context;

        /// <summary>
        /// Repository constructor.
        /// </summary>
        /// <param name="context">DbContext.</param>
        public Repository(DbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds new row into the database table.
        /// </summary>
        /// <param name="item">Element whose data need to add into table.</param>
        public void Add(TEntity item)
        {
            _context.Set<TEntity>().Add(item);
        }

        /// <summary>
        /// Adds elements from the specify collection into the database table.
        /// </summary>
        /// <param name="items">Collection whose elements need to add to database table.</param>
        public void AddRange(IEnumerable<TEntity> items)
        {
            _context.Set<TEntity>().AddRange(items);
        }

        /// <summary>
        /// Searches for an element that matches the conditions defined by the specified predicate.
        /// </summary>
        /// <param name="predicate">delegate that defines the conditions of the element to search for.</param>
        /// <returns>The first element that matches the conditions.</returns>
        public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
        {
            return _context.Set<TEntity>().Where(predicate).ToList();
        }

        /// <summary>
        /// Returns an element with specify id.
        /// </summary>
        /// <param name="id">Id of the element.</param>
        /// <returns>Element.</returns>
        public TEntity Get(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        /// <summary>
        /// Returns all elements in the table.
        /// </summary>
        /// <returns>List of elements.</returns>
        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        /// <summary>
        /// Removes an specify element from the table.
        /// </summary>
        /// <param name="item">The element, that need to remove.</param>
        public void Remove(TEntity item)
        {
            _context.Set<TEntity>().Remove(item);
        }

        /// <summary>
        /// Removes a range of elements from the table.
        /// </summary>
        /// <param name="items">Collection of elements, that need to remove.</param>
        public void RemoveRange(IEnumerable<TEntity> items)
        {
            _context.Set<TEntity>().RemoveRange(items);
        }
    }
}
