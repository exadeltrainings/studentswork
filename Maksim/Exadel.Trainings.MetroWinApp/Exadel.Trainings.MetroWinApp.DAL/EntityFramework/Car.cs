//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Exadel.Trainings.MetroWinApp.DAL.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class Car
    {
        public int Id { get; set; }
        public int ModelId { get; set; }
        public int ProducerId { get; set; }
        public int TransmissionId { get; set; }
        public int BodyId { get; set; }
        public int SalerId { get; set; }
        public double Price { get; set; }
        public int Year { get; set; }
        public string Engine { get; set; }
        public Nullable<int> Mileage { get; set; }
        public string Color { get; set; }
        public string Condition { get; set; }
        public string VIN { get; set; }
        public string Description { get; set; }
    
        public virtual Body Body { get; set; }
        public virtual Model Model { get; set; }
        public virtual Producer Producer { get; set; }
        public virtual Transmission Transmission { get; set; }
        public virtual User User { get; set; }
    }
}
