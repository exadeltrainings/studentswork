﻿namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    partial class ucMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.mtBuy = new MetroFramework.Controls.MetroTile();
            this.mtSell = new MetroFramework.Controls.MetroTile();
            this.mtAdminPanel = new MetroFramework.Controls.MetroTile();
            this.mtService = new MetroFramework.Controls.MetroTile();
            this.mtProfile = new MetroFramework.Controls.MetroTile();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.AutoSize = true;
            this.tableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel.Controls.Add(this.mtBuy, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.mtSell, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.mtAdminPanel, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.mtService, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.mtProfile, 2, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(476, 350);
            this.tableLayoutPanel.TabIndex = 6;
            // 
            // mtBuy
            // 
            this.mtBuy.ActiveControl = null;
            this.mtBuy.AutoSize = true;
            this.mtBuy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mtBuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtBuy.Location = new System.Drawing.Point(3, 3);
            this.mtBuy.Name = "mtBuy";
            this.mtBuy.Size = new System.Drawing.Size(151, 169);
            this.mtBuy.TabIndex = 2;
            this.mtBuy.Text = "Buy";
            this.mtBuy.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_magnify_browse;
            this.mtBuy.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtBuy.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtBuy.UseSelectable = true;
            this.mtBuy.UseTileImage = true;
            this.mtBuy.Click += new System.EventHandler(this.mtBuy_Click);
            // 
            // mtSell
            // 
            this.mtSell.ActiveControl = null;
            this.mtSell.AutoSize = true;
            this.mtSell.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mtSell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtSell.Location = new System.Drawing.Point(160, 3);
            this.mtSell.Name = "mtSell";
            this.mtSell.Size = new System.Drawing.Size(151, 169);
            this.mtSell.TabIndex = 3;
            this.mtSell.Text = "Sell";
            this.mtSell.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_forsale;
            this.mtSell.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtSell.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtSell.UseSelectable = true;
            this.mtSell.UseTileImage = true;
            // 
            // mtAdminPanel
            // 
            this.mtAdminPanel.ActiveControl = null;
            this.mtAdminPanel.AutoSize = true;
            this.mtAdminPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mtAdminPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtAdminPanel.Location = new System.Drawing.Point(160, 178);
            this.mtAdminPanel.Name = "mtAdminPanel";
            this.mtAdminPanel.Size = new System.Drawing.Size(151, 169);
            this.mtAdminPanel.TabIndex = 5;
            this.mtAdminPanel.Text = "Admin panel";
            this.mtAdminPanel.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_people_profile;
            this.mtAdminPanel.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtAdminPanel.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtAdminPanel.UseSelectable = true;
            this.mtAdminPanel.UseTileImage = true;
            this.mtAdminPanel.Visible = false;
            this.mtAdminPanel.Click += new System.EventHandler(this.mtAdminPanel_Click);
            // 
            // mtService
            // 
            this.mtService.ActiveControl = null;
            this.mtService.AutoSize = true;
            this.mtService.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mtService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtService.Location = new System.Drawing.Point(3, 178);
            this.mtService.Name = "mtService";
            this.mtService.Size = new System.Drawing.Size(151, 169);
            this.mtService.TabIndex = 4;
            this.mtService.Text = "Service";
            this.mtService.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_tools;
            this.mtService.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtService.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtService.UseSelectable = true;
            this.mtService.UseTileImage = true;
            // 
            // mtProfile
            // 
            this.mtProfile.ActiveControl = null;
            this.mtProfile.AutoSize = true;
            this.mtProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mtProfile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtProfile.Enabled = false;
            this.mtProfile.Location = new System.Drawing.Point(324, 3);
            this.mtProfile.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.mtProfile.Name = "mtProfile";
            this.mtProfile.Size = new System.Drawing.Size(149, 169);
            this.mtProfile.TabIndex = 6;
            this.mtProfile.Text = "Profile";
            this.mtProfile.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_people;
            this.mtProfile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtProfile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtProfile.UseSelectable = true;
            this.mtProfile.UseTileImage = true;
            this.mtProfile.Click += new System.EventHandler(this.mtProfile_Click);
            // 
            // ucMenu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "ucMenu";
            this.Size = new System.Drawing.Size(476, 350);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTile mtService;
        private MetroFramework.Controls.MetroTile mtAdminPanel;
        private MetroFramework.Controls.MetroTile mtSell;
        private MetroFramework.Controls.MetroTile mtBuy;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private MetroFramework.Controls.MetroTile mtProfile;
    }
}
