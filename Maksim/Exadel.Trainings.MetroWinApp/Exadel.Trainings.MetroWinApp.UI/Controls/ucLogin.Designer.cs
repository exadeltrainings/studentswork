﻿namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    partial class ucLogin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.mtbPassword = new MetroFramework.Controls.MetroTextBox();
            this.mtbUsername = new MetroFramework.Controls.MetroTextBox();
            this.mbtnBack = new MetroFramework.Controls.MetroButton();
            this.mbtnLogin = new MetroFramework.Controls.MetroButton();
            this.mlPassword = new MetroFramework.Controls.MetroLabel();
            this.mlUsername = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(847, 638);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.mtbPassword);
            this.metroPanel1.Controls.Add(this.mtbUsername);
            this.metroPanel1.Controls.Add(this.mbtnBack);
            this.metroPanel1.Controls.Add(this.mbtnLogin);
            this.metroPanel1.Controls.Add(this.mlPassword);
            this.metroPanel1.Controls.Add(this.mlUsername);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(231, 244);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(385, 150);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // mtbPassword
            // 
            // 
            // 
            // 
            this.mtbPassword.CustomButton.Image = null;
            this.mtbPassword.CustomButton.Location = new System.Drawing.Point(214, 1);
            this.mtbPassword.CustomButton.Name = "";
            this.mtbPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbPassword.CustomButton.TabIndex = 1;
            this.mtbPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbPassword.CustomButton.UseSelectable = true;
            this.mtbPassword.CustomButton.Visible = false;
            this.mtbPassword.Lines = new string[0];
            this.mtbPassword.Location = new System.Drawing.Point(107, 55);
            this.mtbPassword.MaxLength = 32767;
            this.mtbPassword.Name = "mtbPassword";
            this.mtbPassword.PasswordChar = '●';
            this.mtbPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbPassword.SelectedText = "";
            this.mtbPassword.SelectionLength = 0;
            this.mtbPassword.SelectionStart = 0;
            this.mtbPassword.ShortcutsEnabled = true;
            this.mtbPassword.Size = new System.Drawing.Size(236, 23);
            this.mtbPassword.TabIndex = 1;
            this.mtbPassword.UseSelectable = true;
            this.mtbPassword.UseSystemPasswordChar = true;
            this.mtbPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbUsername
            // 
            // 
            // 
            // 
            this.mtbUsername.CustomButton.Image = null;
            this.mtbUsername.CustomButton.Location = new System.Drawing.Point(214, 1);
            this.mtbUsername.CustomButton.Name = "";
            this.mtbUsername.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbUsername.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbUsername.CustomButton.TabIndex = 1;
            this.mtbUsername.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbUsername.CustomButton.UseSelectable = true;
            this.mtbUsername.CustomButton.Visible = false;
            this.mtbUsername.Lines = new string[0];
            this.mtbUsername.Location = new System.Drawing.Point(107, 26);
            this.mtbUsername.MaxLength = 32767;
            this.mtbUsername.Name = "mtbUsername";
            this.mtbUsername.PasswordChar = '\0';
            this.mtbUsername.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbUsername.SelectedText = "";
            this.mtbUsername.SelectionLength = 0;
            this.mtbUsername.SelectionStart = 0;
            this.mtbUsername.ShortcutsEnabled = true;
            this.mtbUsername.Size = new System.Drawing.Size(236, 23);
            this.mtbUsername.TabIndex = 0;
            this.mtbUsername.UseSelectable = true;
            this.mtbUsername.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbUsername.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mbtnBack
            // 
            this.mbtnBack.Location = new System.Drawing.Point(187, 98);
            this.mbtnBack.Name = "mbtnBack";
            this.mbtnBack.Size = new System.Drawing.Size(75, 23);
            this.mbtnBack.TabIndex = 2;
            this.mbtnBack.Text = "Back";
            this.mbtnBack.UseSelectable = true;
            this.mbtnBack.Click += new System.EventHandler(this.mbtnBack_Click);
            // 
            // mbtnLogin
            // 
            this.mbtnLogin.Location = new System.Drawing.Point(268, 98);
            this.mbtnLogin.Name = "mbtnLogin";
            this.mbtnLogin.Size = new System.Drawing.Size(75, 23);
            this.mbtnLogin.TabIndex = 3;
            this.mbtnLogin.Text = "&Login";
            this.mbtnLogin.UseSelectable = true;
            this.mbtnLogin.Click += new System.EventHandler(this.mbtnLogin_Click);
            // 
            // mlPassword
            // 
            this.mlPassword.AutoSize = true;
            this.mlPassword.Location = new System.Drawing.Point(26, 55);
            this.mlPassword.Name = "mlPassword";
            this.mlPassword.Size = new System.Drawing.Size(66, 19);
            this.mlPassword.TabIndex = 2;
            this.mlPassword.Text = "Password:";
            // 
            // mlUsername
            // 
            this.mlUsername.AutoSize = true;
            this.mlUsername.Location = new System.Drawing.Point(26, 26);
            this.mlUsername.Name = "mlUsername";
            this.mlUsername.Size = new System.Drawing.Size(75, 19);
            this.mlUsername.TabIndex = 2;
            this.mlUsername.Text = "User name:";
            // 
            // ucLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ucLogin";
            this.Size = new System.Drawing.Size(847, 638);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTextBox mtbUsername;
        private MetroFramework.Controls.MetroButton mbtnLogin;
        private MetroFramework.Controls.MetroLabel mlUsername;
        private MetroFramework.Controls.MetroTextBox mtbPassword;
        private MetroFramework.Controls.MetroLabel mlPassword;
        private MetroFramework.Controls.MetroButton mbtnBack;
    }
}
