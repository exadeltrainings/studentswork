﻿namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    partial class ucRegistration
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.mtbComfirmPass = new MetroFramework.Controls.MetroTextBox();
            this.mtbPassword = new MetroFramework.Controls.MetroTextBox();
            this.mtbPhone = new MetroFramework.Controls.MetroTextBox();
            this.mtbBirthdate = new MetroFramework.Controls.MetroTextBox();
            this.mtbUsername = new MetroFramework.Controls.MetroTextBox();
            this.mbtnBack = new MetroFramework.Controls.MetroButton();
            this.mlConfirmPass = new MetroFramework.Controls.MetroLabel();
            this.mlPhone = new MetroFramework.Controls.MetroLabel();
            this.mbtnSignUp = new MetroFramework.Controls.MetroButton();
            this.mlBirthdate = new MetroFramework.Controls.MetroLabel();
            this.mlPassword = new MetroFramework.Controls.MetroLabel();
            this.mlUsername = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(989, 654);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.mtbComfirmPass);
            this.metroPanel1.Controls.Add(this.mtbPassword);
            this.metroPanel1.Controls.Add(this.mtbPhone);
            this.metroPanel1.Controls.Add(this.mtbBirthdate);
            this.metroPanel1.Controls.Add(this.mtbUsername);
            this.metroPanel1.Controls.Add(this.mbtnBack);
            this.metroPanel1.Controls.Add(this.mlConfirmPass);
            this.metroPanel1.Controls.Add(this.mlPhone);
            this.metroPanel1.Controls.Add(this.mbtnSignUp);
            this.metroPanel1.Controls.Add(this.mlBirthdate);
            this.metroPanel1.Controls.Add(this.mlPassword);
            this.metroPanel1.Controls.Add(this.mlUsername);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(281, 212);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(426, 230);
            this.metroPanel1.TabIndex = 1;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // mtbComfirmPass
            // 
            // 
            // 
            // 
            this.mtbComfirmPass.CustomButton.Image = null;
            this.mtbComfirmPass.CustomButton.Location = new System.Drawing.Point(214, 1);
            this.mtbComfirmPass.CustomButton.Name = "";
            this.mtbComfirmPass.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbComfirmPass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbComfirmPass.CustomButton.TabIndex = 1;
            this.mtbComfirmPass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbComfirmPass.CustomButton.UseSelectable = true;
            this.mtbComfirmPass.CustomButton.Visible = false;
            this.mtbComfirmPass.Lines = new string[0];
            this.mtbComfirmPass.Location = new System.Drawing.Point(162, 84);
            this.mtbComfirmPass.MaxLength = 32767;
            this.mtbComfirmPass.Name = "mtbComfirmPass";
            this.mtbComfirmPass.PasswordChar = '●';
            this.mtbComfirmPass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbComfirmPass.SelectedText = "";
            this.mtbComfirmPass.SelectionLength = 0;
            this.mtbComfirmPass.SelectionStart = 0;
            this.mtbComfirmPass.ShortcutsEnabled = true;
            this.mtbComfirmPass.Size = new System.Drawing.Size(236, 23);
            this.mtbComfirmPass.TabIndex = 2;
            this.mtbComfirmPass.UseSelectable = true;
            this.mtbComfirmPass.UseSystemPasswordChar = true;
            this.mtbComfirmPass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbComfirmPass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbPassword
            // 
            // 
            // 
            // 
            this.mtbPassword.CustomButton.Image = null;
            this.mtbPassword.CustomButton.Location = new System.Drawing.Point(214, 1);
            this.mtbPassword.CustomButton.Name = "";
            this.mtbPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbPassword.CustomButton.TabIndex = 1;
            this.mtbPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbPassword.CustomButton.UseSelectable = true;
            this.mtbPassword.CustomButton.Visible = false;
            this.mtbPassword.Lines = new string[0];
            this.mtbPassword.Location = new System.Drawing.Point(162, 55);
            this.mtbPassword.MaxLength = 32767;
            this.mtbPassword.Name = "mtbPassword";
            this.mtbPassword.PasswordChar = '●';
            this.mtbPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbPassword.SelectedText = "";
            this.mtbPassword.SelectionLength = 0;
            this.mtbPassword.SelectionStart = 0;
            this.mtbPassword.ShortcutsEnabled = true;
            this.mtbPassword.Size = new System.Drawing.Size(236, 23);
            this.mtbPassword.TabIndex = 1;
            this.mtbPassword.UseSelectable = true;
            this.mtbPassword.UseSystemPasswordChar = true;
            this.mtbPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbPhone
            // 
            // 
            // 
            // 
            this.mtbPhone.CustomButton.Image = null;
            this.mtbPhone.CustomButton.Location = new System.Drawing.Point(214, 1);
            this.mtbPhone.CustomButton.Name = "";
            this.mtbPhone.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbPhone.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbPhone.CustomButton.TabIndex = 1;
            this.mtbPhone.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbPhone.CustomButton.UseSelectable = true;
            this.mtbPhone.CustomButton.Visible = false;
            this.mtbPhone.Lines = new string[0];
            this.mtbPhone.Location = new System.Drawing.Point(162, 142);
            this.mtbPhone.MaxLength = 32767;
            this.mtbPhone.Name = "mtbPhone";
            this.mtbPhone.PasswordChar = '\0';
            this.mtbPhone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbPhone.SelectedText = "";
            this.mtbPhone.SelectionLength = 0;
            this.mtbPhone.SelectionStart = 0;
            this.mtbPhone.ShortcutsEnabled = true;
            this.mtbPhone.Size = new System.Drawing.Size(236, 23);
            this.mtbPhone.TabIndex = 4;
            this.mtbPhone.UseSelectable = true;
            this.mtbPhone.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbPhone.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbBirthdate
            // 
            // 
            // 
            // 
            this.mtbBirthdate.CustomButton.Image = null;
            this.mtbBirthdate.CustomButton.Location = new System.Drawing.Point(214, 1);
            this.mtbBirthdate.CustomButton.Name = "";
            this.mtbBirthdate.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbBirthdate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbBirthdate.CustomButton.TabIndex = 1;
            this.mtbBirthdate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbBirthdate.CustomButton.UseSelectable = true;
            this.mtbBirthdate.CustomButton.Visible = false;
            this.mtbBirthdate.Lines = new string[0];
            this.mtbBirthdate.Location = new System.Drawing.Point(162, 113);
            this.mtbBirthdate.MaxLength = 32767;
            this.mtbBirthdate.Name = "mtbBirthdate";
            this.mtbBirthdate.PasswordChar = '\0';
            this.mtbBirthdate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbBirthdate.SelectedText = "";
            this.mtbBirthdate.SelectionLength = 0;
            this.mtbBirthdate.SelectionStart = 0;
            this.mtbBirthdate.ShortcutsEnabled = true;
            this.mtbBirthdate.Size = new System.Drawing.Size(236, 23);
            this.mtbBirthdate.TabIndex = 3;
            this.mtbBirthdate.UseSelectable = true;
            this.mtbBirthdate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbBirthdate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbUsername
            // 
            // 
            // 
            // 
            this.mtbUsername.CustomButton.Image = null;
            this.mtbUsername.CustomButton.Location = new System.Drawing.Point(214, 1);
            this.mtbUsername.CustomButton.Name = "";
            this.mtbUsername.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbUsername.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbUsername.CustomButton.TabIndex = 1;
            this.mtbUsername.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbUsername.CustomButton.UseSelectable = true;
            this.mtbUsername.CustomButton.Visible = false;
            this.mtbUsername.Lines = new string[0];
            this.mtbUsername.Location = new System.Drawing.Point(162, 26);
            this.mtbUsername.MaxLength = 32767;
            this.mtbUsername.Name = "mtbUsername";
            this.mtbUsername.PasswordChar = '\0';
            this.mtbUsername.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbUsername.SelectedText = "";
            this.mtbUsername.SelectionLength = 0;
            this.mtbUsername.SelectionStart = 0;
            this.mtbUsername.ShortcutsEnabled = true;
            this.mtbUsername.Size = new System.Drawing.Size(236, 23);
            this.mtbUsername.TabIndex = 0;
            this.mtbUsername.UseSelectable = true;
            this.mtbUsername.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbUsername.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mbtnBack
            // 
            this.mbtnBack.Location = new System.Drawing.Point(242, 185);
            this.mbtnBack.Name = "mbtnBack";
            this.mbtnBack.Size = new System.Drawing.Size(75, 23);
            this.mbtnBack.TabIndex = 5;
            this.mbtnBack.Text = "Back";
            this.mbtnBack.UseSelectable = true;
            this.mbtnBack.Click += new System.EventHandler(this.mbtnBack_Click);
            // 
            // mlConfirmPass
            // 
            this.mlConfirmPass.AutoSize = true;
            this.mlConfirmPass.Location = new System.Drawing.Point(26, 84);
            this.mlConfirmPass.Name = "mlConfirmPass";
            this.mlConfirmPass.Size = new System.Drawing.Size(124, 19);
            this.mlConfirmPass.TabIndex = 2;
            this.mlConfirmPass.Text = "Confirm Password*:";
            // 
            // mlPhone
            // 
            this.mlPhone.AutoSize = true;
            this.mlPhone.Location = new System.Drawing.Point(26, 142);
            this.mlPhone.Name = "mlPhone";
            this.mlPhone.Size = new System.Drawing.Size(98, 19);
            this.mlPhone.TabIndex = 2;
            this.mlPhone.Text = "Contact Phone:";
            // 
            // mbtnSignUp
            // 
            this.mbtnSignUp.Location = new System.Drawing.Point(323, 185);
            this.mbtnSignUp.Name = "mbtnSignUp";
            this.mbtnSignUp.Size = new System.Drawing.Size(75, 23);
            this.mbtnSignUp.TabIndex = 6;
            this.mbtnSignUp.Text = "&Sign Up";
            this.mbtnSignUp.UseSelectable = true;
            this.mbtnSignUp.Click += new System.EventHandler(this.mbtnSignUp_Click);
            // 
            // mlBirthdate
            // 
            this.mlBirthdate.AutoSize = true;
            this.mlBirthdate.Location = new System.Drawing.Point(26, 113);
            this.mlBirthdate.Name = "mlBirthdate";
            this.mlBirthdate.Size = new System.Drawing.Size(81, 19);
            this.mlBirthdate.TabIndex = 2;
            this.mlBirthdate.Text = "Date of bith:";
            // 
            // mlPassword
            // 
            this.mlPassword.AutoSize = true;
            this.mlPassword.Location = new System.Drawing.Point(26, 55);
            this.mlPassword.Name = "mlPassword";
            this.mlPassword.Size = new System.Drawing.Size(72, 19);
            this.mlPassword.TabIndex = 2;
            this.mlPassword.Text = "Password*:";
            // 
            // mlUsername
            // 
            this.mlUsername.AutoSize = true;
            this.mlUsername.Location = new System.Drawing.Point(26, 26);
            this.mlUsername.Name = "mlUsername";
            this.mlUsername.Size = new System.Drawing.Size(81, 19);
            this.mlUsername.TabIndex = 2;
            this.mlUsername.Text = "User name*:";
            // 
            // ucRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ucRegistration";
            this.Size = new System.Drawing.Size(989, 654);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTextBox mtbPassword;
        private MetroFramework.Controls.MetroTextBox mtbUsername;
        private MetroFramework.Controls.MetroButton mbtnBack;
        private MetroFramework.Controls.MetroButton mbtnSignUp;
        private MetroFramework.Controls.MetroLabel mlPassword;
        private MetroFramework.Controls.MetroLabel mlUsername;
        private MetroFramework.Controls.MetroTextBox mtbComfirmPass;
        private MetroFramework.Controls.MetroTextBox mtbBirthdate;
        private MetroFramework.Controls.MetroLabel mlConfirmPass;
        private MetroFramework.Controls.MetroLabel mlBirthdate;
        private MetroFramework.Controls.MetroTextBox mtbPhone;
        private MetroFramework.Controls.MetroLabel mlPhone;
    }
}
