﻿using Exadel.Trainings.MetroWinApp.UI.Forms;
using MetroFramework.Controls;
using System.Windows.Forms;

namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    public partial class ucMenu : MetroUserControl
    {
        public ucMenu()
        {
            InitializeComponent();
        }

        private void mtBuy_Click(object sender, System.EventArgs e)
        {
            //Creates user control with search menu
            var uc = new ucBuy();
            uc.Dock = DockStyle.Fill;
            //adds to the panel with different menu
            MainForm.Instance.Controls["mPanel"].Controls.Add(uc);
            //represents to the user.
            MainForm.Instance.MetroContainer.Controls["ucBuy"].BringToFront();
        }

        private void mtAdminPanel_Click(object sender, System.EventArgs e)
        {
            var admin = new AdminForm();
            admin.Show();
        }

        private void mtProfile_Click(object sender, System.EventArgs e)
        {
            //Creates user control with profile menu
            var uc = new ucProfile();
            uc.Dock = DockStyle.Fill;
            //adds to the panel with different menu
            MainForm.Instance.Controls["mPanel"].Controls.Add(uc);
            //represents to the user.
            MainForm.Instance.MetroContainer.Controls["ucProfile"].BringToFront();
        }
    }
}
