﻿using System;
using System.Data;
using System.Linq;
using MetroFramework.Controls;
using Exadel.Trainings.MetroWinApp.UI.Helpers;
using Exadel.Trainings.MetroWinApp.DAL.Interfaces;
using Exadel.Trainings.MetroWinApp.DAL;
using Exadel.Trainings.MetroWinApp.UI.Forms;

namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    public partial class ucProfile : MetroUserControl
    {
        private IUnitOfWork _uofw; 

        public ucProfile()
        {
            InitializeComponent();
        }

        private void ucProfile_Load(object sender, EventArgs e)
        {
            _uofw = new UnitOfWork();
            mlUsername.Text = CurrentUser.Username;
            ControlsHelper.GenerateDataGridHeaders(dgvUserCars);

            //Shows current user adds.
            var cars = _uofw.Cars.GetAll().ToList();
            var usercars= cars.Where(c => c.User.Username == CurrentUser.Username);
            foreach (var c in usercars)
            {
                dgvUserCars.Rows.Add(c.Model.Mark.Mark1, c.Model.Model1, c.Producer.Country.Country1, c.Transmission.Transmission1,
                    c.Body.BodyType, c.User.Username, c.Price + " $", c.Year, c.Engine, c.Mileage, c.Color, c.Condition, c.VIN, c.Description);
            }
        }

        private void mtAddCar_Click(object sender, EventArgs e)
        {
            var frm = new AddEditFrom();
            frm.ShowDialog();
        }

        /// <summary>
        /// Returns user to the main menu.
        /// </summary>
        private void mtHome_Click(object sender, EventArgs e)
        {
            MainForm.Instance.MetroContainer.Controls["ucMenu"].BringToFront();
        }
    }
}
