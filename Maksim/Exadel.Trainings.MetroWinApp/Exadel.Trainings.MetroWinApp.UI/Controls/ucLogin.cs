﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MetroFramework.Controls;
using Exadel.Trainings.MetroWinApp.UI.Forms;
using Exadel.Trainings.MetroWinApp.BLL.Concrete;
using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using MetroFramework;

namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    public partial class ucLogin : MetroUserControl
    {
        public ucLogin()
        {
            InitializeComponent();
        }

        private void mbtnLogin_Click(object sender, EventArgs e)
        {
            //Check login & password
            var auth = new DefaultAuthService();
            var obj = auth.IsUser(new User { Username = mtbUsername.Text, Password = mtbPassword.Text });
            if (obj != null)
            {
                CurrentUser.Login(obj);

                MainForm.Instance.MetroContainer.Controls["ucMenu"].BringToFront();
                var label = (MetroLink)MainForm.Instance.Controls["mlLogin"];
                label.Text = "logout";
                label.Image = Image.FromFile(@"..\..\Resources\appbar.cabinet.out.png");
                MainForm.Instance.Controls["mlSignUp"].Enabled = false;
                MainForm.Instance.MetroContainer.Controls["ucMenu"].Controls["tableLayoutPanel"].Controls["mtProfile"].Enabled = true;

                if (CurrentUser.Role == "admin")
                {
                    MainForm.Instance.MetroContainer.Controls["ucMenu"].Controls["tableLayoutPanel"].Controls["mtAdminPanel"].Visible = true;
                }
            }
            else
                MetroMessageBox.Show(this, "Invalid username or password", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void mbtnBack_Click(object sender, EventArgs e)
        {
            MainForm.Instance.MetroContainer.Controls["ucMenu"].BringToFront();
        }
    }
}
