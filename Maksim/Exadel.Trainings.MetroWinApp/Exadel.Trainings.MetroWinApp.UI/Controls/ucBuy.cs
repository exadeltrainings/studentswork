﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Exadel.Trainings.MetroWinApp.DAL.Interfaces;
using Exadel.Trainings.MetroWinApp.DAL;
using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using Exadel.Trainings.MetroWinApp.BLL.Concrete;
using Exadel.Trainings.MetroWinApp.UI.Forms;
using Exadel.Trainings.MetroWinApp.UI.Helpers;

namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    public partial class ucBuy : MetroUserControl
    {
        private IUnitOfWork _uofw;

        public ucBuy()
        {
            InitializeComponent();
            _uofw = new UnitOfWork();
        }

        private void ucBuy_Load(object sender, EventArgs e)
        {
            BindBindingSources();

            ControlsHelper.GenerateDataGridHeaders(dgvCars);
            FillDataGrid();

            ControlsHelper.GenerateYears(mcbYearFrom);
            ControlsHelper.GenerateYears(mcbYearTo);
            MarkComboBox_SelectedIndexChanged(this, new EventArgs());
        }

        /// <summary>
        /// Binds all binding sources.
        /// </summary>
        private void BindBindingSources()
        {
            markBindingSource.DataSource = _uofw.Marks.GetAll();
            countryBindingSource.DataSource = _uofw.Countries.GetAll();
            transmissionBindingSource.DataSource = _uofw.Transmissions.GetAll();
            bodyBindingSource.DataSource = _uofw.Bodies.GetAll();
        }

        /// <summary>
        /// Inserts all cars from db into DataGrid.
        /// </summary>
        private void FillDataGrid()
        {
            Cursor.Current = Cursors.WaitCursor;
            var cars = _uofw.Cars.GetAll();
            foreach (var c in cars)
            {
                dgvCars.Rows.Add(c.Model.Mark.Mark1, c.Model.Model1, c.Producer.Country.Country1, c.Transmission.Transmission1,
                    c.Body.BodyType, c.User.Username, c.Price + " $", c.Year, c.Engine, c.Mileage, c.Color, c.Condition, c.VIN, c.Description);
            }
            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Fills ModelComboBox.
        /// </summary>
        private void MarkComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            mcbModel.Items.Clear();
            if (mcbMark.SelectedItem != null)
            {
                var models = _uofw.Models.Find(m => m.Mark.Mark1 == ((Mark)mcbMark.SelectedItem).Mark1);
                foreach (var m in models)
                {
                    mcbModel.Items.Add(m.Model1);
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void mtFind_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            dgvCars.Rows.Clear();

            var serv = new DefaultFindService();
            var cars = serv.FindCars(mcbMark.SelectedItem, mcbModel.SelectedItem, mcbCountry.SelectedItem, mtbPriceFrom.Text, mtbPriceTo.Text,
                mcbYearFrom.SelectedItem, mcbYearTo.SelectedItem, mcbTransmission.SelectedItem, mcbBodyType.SelectedItem);

            foreach (var c in cars)
            {
                dgvCars.Rows.Add(c.Model.Mark.Mark1, c.Model.Model1, c.Producer.Country.Country1, c.Transmission.Transmission1,
                    c.Body.BodyType, c.User.Username, c.Price + " $", c.Year, c.Engine, c.Mileage, c.Color, c.Condition, c.VIN, c.Description);
            }

            Cursor.Current = Cursors.Default;
        }

        private void mtClear_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            dgvCars.Rows.Clear();
            FillDataGrid();

            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Returns user to the main menu.
        /// </summary>
        private void mtHome_Click(object sender, EventArgs e)
        {
            MainForm.Instance.MetroContainer.Controls["ucMenu"].BringToFront();
        }
    }
}
