﻿namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    partial class ucProfile
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucProfile));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mlUsername = new MetroFramework.Controls.MetroLabel();
            this.mlAboutGrid = new MetroFramework.Controls.MetroLabel();
            this.dgvUserCars = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mtAddCar = new MetroFramework.Controls.MetroTile();
            this.mtHome = new MetroFramework.Controls.MetroTile();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserCars)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mlUsername, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.mlAboutGrid, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dgvUserCars, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(895, 538);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // mlUsername
            // 
            this.mlUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mlUsername.AutoSize = true;
            this.mlUsername.Location = new System.Drawing.Point(3, 141);
            this.mlUsername.Name = "mlUsername";
            this.mlUsername.Size = new System.Drawing.Size(134, 19);
            this.mlUsername.TabIndex = 1;
            this.mlUsername.Text = "Here will be username";
            // 
            // mlAboutGrid
            // 
            this.mlAboutGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mlAboutGrid.AutoSize = true;
            this.mlAboutGrid.Location = new System.Drawing.Point(143, 141);
            this.mlAboutGrid.Name = "mlAboutGrid";
            this.mlAboutGrid.Size = new System.Drawing.Size(66, 19);
            this.mlAboutGrid.TabIndex = 5;
            this.mlAboutGrid.Text = "Your cars:";
            // 
            // dgvUserCars
            // 
            this.dgvUserCars.BackgroundColor = System.Drawing.Color.White;
            this.dgvUserCars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserCars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUserCars.Location = new System.Drawing.Point(143, 163);
            this.dgvUserCars.Name = "dgvUserCars";
            this.dgvUserCars.Size = new System.Drawing.Size(749, 372);
            this.dgvUserCars.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.mtAddCar);
            this.panel1.Controls.Add(this.mtHome);
            this.panel1.Location = new System.Drawing.Point(3, 163);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(134, 372);
            this.panel1.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_camera_switch_invert;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 126);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // mtAddCar
            // 
            this.mtAddCar.ActiveControl = null;
            this.mtAddCar.Location = new System.Drawing.Point(3, 3);
            this.mtAddCar.Name = "mtAddCar";
            this.mtAddCar.Size = new System.Drawing.Size(128, 107);
            this.mtAddCar.TabIndex = 0;
            this.mtAddCar.Text = "Add advert";
            this.mtAddCar.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_man_suitcase_fast;
            this.mtAddCar.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtAddCar.UseSelectable = true;
            this.mtAddCar.UseTileImage = true;
            this.mtAddCar.Click += new System.EventHandler(this.mtAddCar_Click);
            // 
            // mtHome
            // 
            this.mtHome.ActiveControl = null;
            this.mtHome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mtHome.Location = new System.Drawing.Point(3, 262);
            this.mtHome.Name = "mtHome";
            this.mtHome.Size = new System.Drawing.Size(128, 107);
            this.mtHome.TabIndex = 0;
            this.mtHome.Text = "Home";
            this.mtHome.TileImage = ((System.Drawing.Image)(resources.GetObject("mtHome.TileImage")));
            this.mtHome.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtHome.UseSelectable = true;
            this.mtHome.UseTileImage = true;
            this.mtHome.Click += new System.EventHandler(this.mtHome_Click);
            // 
            // ucProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ucProfile";
            this.Size = new System.Drawing.Size(895, 538);
            this.Load += new System.EventHandler(this.ucProfile_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserCars)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroLabel mlUsername;
        private MetroFramework.Controls.MetroLabel mlAboutGrid;
        private System.Windows.Forms.DataGridView dgvUserCars;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroTile mtAddCar;
        private MetroFramework.Controls.MetroTile mtHome;
    }
}
