﻿namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    partial class ucBuy
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucBuy));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pSearchParam = new System.Windows.Forms.Panel();
            this.mtbPriceTo = new MetroFramework.Controls.MetroTextBox();
            this.mtbPriceFrom = new MetroFramework.Controls.MetroTextBox();
            this.mcbYearTo = new MetroFramework.Controls.MetroComboBox();
            this.mcbYearFrom = new MetroFramework.Controls.MetroComboBox();
            this.mcbCountry = new MetroFramework.Controls.MetroComboBox();
            this.countryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mcbModel = new MetroFramework.Controls.MetroComboBox();
            this.mlYearTo = new MetroFramework.Controls.MetroLabel();
            this.mcbMark = new MetroFramework.Controls.MetroComboBox();
            this.markBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mlYearFrom = new MetroFramework.Controls.MetroLabel();
            this.mlPriceTo = new MetroFramework.Controls.MetroLabel();
            this.mlPriceFrom = new MetroFramework.Controls.MetroLabel();
            this.mlYear = new MetroFramework.Controls.MetroLabel();
            this.mlPrice = new MetroFramework.Controls.MetroLabel();
            this.mlCountry = new MetroFramework.Controls.MetroLabel();
            this.mlModel = new MetroFramework.Controls.MetroLabel();
            this.mlMark = new MetroFramework.Controls.MetroLabel();
            this.dgvCars = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mtFind = new MetroFramework.Controls.MetroTile();
            this.mtHome = new MetroFramework.Controls.MetroTile();
            this.mtClear = new MetroFramework.Controls.MetroTile();
            this.mlTransmission = new MetroFramework.Controls.MetroLabel();
            this.mlBodyType = new MetroFramework.Controls.MetroLabel();
            this.mcbTransmission = new MetroFramework.Controls.MetroComboBox();
            this.mcbBodyType = new MetroFramework.Controls.MetroComboBox();
            this.transmissionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bodyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.pSearchParam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.countryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.markBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCars)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transmissionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bodyBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pSearchParam, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvCars, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(952, 793);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pSearchParam
            // 
            this.pSearchParam.AutoScroll = true;
            this.pSearchParam.Controls.Add(this.mtbPriceTo);
            this.pSearchParam.Controls.Add(this.mtbPriceFrom);
            this.pSearchParam.Controls.Add(this.mcbYearTo);
            this.pSearchParam.Controls.Add(this.mcbYearFrom);
            this.pSearchParam.Controls.Add(this.mcbBodyType);
            this.pSearchParam.Controls.Add(this.mcbCountry);
            this.pSearchParam.Controls.Add(this.mcbTransmission);
            this.pSearchParam.Controls.Add(this.mcbModel);
            this.pSearchParam.Controls.Add(this.mlYearTo);
            this.pSearchParam.Controls.Add(this.mcbMark);
            this.pSearchParam.Controls.Add(this.mlYearFrom);
            this.pSearchParam.Controls.Add(this.mlPriceTo);
            this.pSearchParam.Controls.Add(this.mlPriceFrom);
            this.pSearchParam.Controls.Add(this.mlYear);
            this.pSearchParam.Controls.Add(this.mlBodyType);
            this.pSearchParam.Controls.Add(this.mlPrice);
            this.pSearchParam.Controls.Add(this.mlTransmission);
            this.pSearchParam.Controls.Add(this.mlCountry);
            this.pSearchParam.Controls.Add(this.mlModel);
            this.pSearchParam.Controls.Add(this.mlMark);
            this.pSearchParam.Location = new System.Drawing.Point(3, 3);
            this.pSearchParam.Name = "pSearchParam";
            this.pSearchParam.Size = new System.Drawing.Size(204, 691);
            this.pSearchParam.TabIndex = 1;
            // 
            // mtbPriceTo
            // 
            // 
            // 
            // 
            this.mtbPriceTo.CustomButton.Image = null;
            this.mtbPriceTo.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtbPriceTo.CustomButton.Name = "";
            this.mtbPriceTo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbPriceTo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbPriceTo.CustomButton.TabIndex = 1;
            this.mtbPriceTo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbPriceTo.CustomButton.UseSelectable = true;
            this.mtbPriceTo.CustomButton.Visible = false;
            this.mtbPriceTo.Lines = new string[0];
            this.mtbPriceTo.Location = new System.Drawing.Point(62, 322);
            this.mtbPriceTo.MaxLength = 32767;
            this.mtbPriceTo.Name = "mtbPriceTo";
            this.mtbPriceTo.PasswordChar = '\0';
            this.mtbPriceTo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbPriceTo.SelectedText = "";
            this.mtbPriceTo.SelectionLength = 0;
            this.mtbPriceTo.SelectionStart = 0;
            this.mtbPriceTo.ShortcutsEnabled = true;
            this.mtbPriceTo.Size = new System.Drawing.Size(121, 23);
            this.mtbPriceTo.TabIndex = 2;
            this.mtbPriceTo.UseSelectable = true;
            this.mtbPriceTo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbPriceTo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbPriceFrom
            // 
            // 
            // 
            // 
            this.mtbPriceFrom.CustomButton.Image = null;
            this.mtbPriceFrom.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtbPriceFrom.CustomButton.Name = "";
            this.mtbPriceFrom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbPriceFrom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbPriceFrom.CustomButton.TabIndex = 1;
            this.mtbPriceFrom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbPriceFrom.CustomButton.UseSelectable = true;
            this.mtbPriceFrom.CustomButton.Visible = false;
            this.mtbPriceFrom.Lines = new string[0];
            this.mtbPriceFrom.Location = new System.Drawing.Point(62, 287);
            this.mtbPriceFrom.MaxLength = 32767;
            this.mtbPriceFrom.Name = "mtbPriceFrom";
            this.mtbPriceFrom.PasswordChar = '\0';
            this.mtbPriceFrom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbPriceFrom.SelectedText = "";
            this.mtbPriceFrom.SelectionLength = 0;
            this.mtbPriceFrom.SelectionStart = 0;
            this.mtbPriceFrom.ShortcutsEnabled = true;
            this.mtbPriceFrom.Size = new System.Drawing.Size(121, 23);
            this.mtbPriceFrom.TabIndex = 2;
            this.mtbPriceFrom.UseSelectable = true;
            this.mtbPriceFrom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbPriceFrom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mcbYearTo
            // 
            this.mcbYearTo.FormattingEnabled = true;
            this.mcbYearTo.ItemHeight = 23;
            this.mcbYearTo.Location = new System.Drawing.Point(62, 436);
            this.mcbYearTo.Name = "mcbYearTo";
            this.mcbYearTo.Size = new System.Drawing.Size(121, 29);
            this.mcbYearTo.TabIndex = 1;
            this.mcbYearTo.UseSelectable = true;
            // 
            // mcbYearFrom
            // 
            this.mcbYearFrom.FormattingEnabled = true;
            this.mcbYearFrom.ItemHeight = 23;
            this.mcbYearFrom.Location = new System.Drawing.Point(62, 401);
            this.mcbYearFrom.Name = "mcbYearFrom";
            this.mcbYearFrom.Size = new System.Drawing.Size(121, 29);
            this.mcbYearFrom.TabIndex = 1;
            this.mcbYearFrom.UseSelectable = true;
            // 
            // mcbCountry
            // 
            this.mcbCountry.DataSource = this.countryBindingSource;
            this.mcbCountry.DisplayMember = "Country1";
            this.mcbCountry.FormattingEnabled = true;
            this.mcbCountry.ItemHeight = 23;
            this.mcbCountry.Location = new System.Drawing.Point(15, 190);
            this.mcbCountry.Name = "mcbCountry";
            this.mcbCountry.Size = new System.Drawing.Size(168, 29);
            this.mcbCountry.TabIndex = 1;
            this.mcbCountry.UseSelectable = true;
            this.mcbCountry.ValueMember = "Id";
            // 
            // countryBindingSource
            // 
            this.countryBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Country);
            // 
            // mcbModel
            // 
            this.mcbModel.FormattingEnabled = true;
            this.mcbModel.ItemHeight = 23;
            this.mcbModel.Location = new System.Drawing.Point(15, 114);
            this.mcbModel.Name = "mcbModel";
            this.mcbModel.Size = new System.Drawing.Size(168, 29);
            this.mcbModel.TabIndex = 1;
            this.mcbModel.UseSelectable = true;
            // 
            // mlYearTo
            // 
            this.mlYearTo.AutoSize = true;
            this.mlYearTo.Location = new System.Drawing.Point(15, 446);
            this.mlYearTo.Name = "mlYearTo";
            this.mlYearTo.Size = new System.Drawing.Size(22, 19);
            this.mlYearTo.TabIndex = 0;
            this.mlYearTo.Text = "To";
            // 
            // mcbMark
            // 
            this.mcbMark.DataSource = this.markBindingSource;
            this.mcbMark.DisplayMember = "Mark1";
            this.mcbMark.FormattingEnabled = true;
            this.mcbMark.ItemHeight = 23;
            this.mcbMark.Location = new System.Drawing.Point(15, 39);
            this.mcbMark.Name = "mcbMark";
            this.mcbMark.Size = new System.Drawing.Size(168, 29);
            this.mcbMark.TabIndex = 1;
            this.mcbMark.UseSelectable = true;
            this.mcbMark.ValueMember = "Id";
            this.mcbMark.SelectedIndexChanged += new System.EventHandler(this.MarkComboBox_SelectedIndexChanged);
            // 
            // markBindingSource
            // 
            this.markBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Mark);
            // 
            // mlYearFrom
            // 
            this.mlYearFrom.AutoSize = true;
            this.mlYearFrom.Location = new System.Drawing.Point(15, 411);
            this.mlYearFrom.Name = "mlYearFrom";
            this.mlYearFrom.Size = new System.Drawing.Size(41, 19);
            this.mlYearFrom.TabIndex = 0;
            this.mlYearFrom.Text = "From";
            // 
            // mlPriceTo
            // 
            this.mlPriceTo.AutoSize = true;
            this.mlPriceTo.Location = new System.Drawing.Point(15, 322);
            this.mlPriceTo.Name = "mlPriceTo";
            this.mlPriceTo.Size = new System.Drawing.Size(22, 19);
            this.mlPriceTo.TabIndex = 0;
            this.mlPriceTo.Text = "To";
            // 
            // mlPriceFrom
            // 
            this.mlPriceFrom.AutoSize = true;
            this.mlPriceFrom.Location = new System.Drawing.Point(15, 287);
            this.mlPriceFrom.Name = "mlPriceFrom";
            this.mlPriceFrom.Size = new System.Drawing.Size(41, 19);
            this.mlPriceFrom.TabIndex = 0;
            this.mlPriceFrom.Text = "From";
            // 
            // mlYear
            // 
            this.mlYear.AutoSize = true;
            this.mlYear.Location = new System.Drawing.Point(15, 368);
            this.mlYear.Name = "mlYear";
            this.mlYear.Size = new System.Drawing.Size(72, 19);
            this.mlYear.TabIndex = 0;
            this.mlYear.Text = "Select year";
            // 
            // mlPrice
            // 
            this.mlPrice.AutoSize = true;
            this.mlPrice.Location = new System.Drawing.Point(15, 245);
            this.mlPrice.Name = "mlPrice";
            this.mlPrice.Size = new System.Drawing.Size(76, 19);
            this.mlPrice.TabIndex = 0;
            this.mlPrice.Text = "Price range";
            // 
            // mlCountry
            // 
            this.mlCountry.AutoSize = true;
            this.mlCountry.Location = new System.Drawing.Point(15, 168);
            this.mlCountry.Name = "mlCountry";
            this.mlCountry.Size = new System.Drawing.Size(94, 19);
            this.mlCountry.TabIndex = 0;
            this.mlCountry.Text = "Select country:";
            // 
            // mlModel
            // 
            this.mlModel.AutoSize = true;
            this.mlModel.Location = new System.Drawing.Point(15, 92);
            this.mlModel.Name = "mlModel";
            this.mlModel.Size = new System.Drawing.Size(88, 19);
            this.mlModel.TabIndex = 0;
            this.mlModel.Text = "Select model:";
            // 
            // mlMark
            // 
            this.mlMark.AutoSize = true;
            this.mlMark.Location = new System.Drawing.Point(15, 17);
            this.mlMark.Name = "mlMark";
            this.mlMark.Size = new System.Drawing.Size(80, 19);
            this.mlMark.TabIndex = 0;
            this.mlMark.Text = "Select mark:";
            // 
            // dgvCars
            // 
            this.dgvCars.AllowUserToAddRows = false;
            this.dgvCars.AllowUserToDeleteRows = false;
            this.dgvCars.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCars.BackgroundColor = System.Drawing.Color.White;
            this.dgvCars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCars.Location = new System.Drawing.Point(213, 3);
            this.dgvCars.Name = "dgvCars";
            this.dgvCars.ReadOnly = true;
            this.dgvCars.Size = new System.Drawing.Size(736, 691);
            this.dgvCars.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.mtFind);
            this.panel1.Controls.Add(this.mtHome);
            this.panel1.Controls.Add(this.mtClear);
            this.panel1.Location = new System.Drawing.Point(213, 700);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(736, 90);
            this.panel1.TabIndex = 3;
            // 
            // mtFind
            // 
            this.mtFind.ActiveControl = null;
            this.mtFind.Location = new System.Drawing.Point(3, 3);
            this.mtFind.Name = "mtFind";
            this.mtFind.Size = new System.Drawing.Size(87, 84);
            this.mtFind.TabIndex = 2;
            this.mtFind.Text = "Find it!";
            this.mtFind.TileImage = ((System.Drawing.Image)(resources.GetObject("mtFind.TileImage")));
            this.mtFind.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mtFind.UseSelectable = true;
            this.mtFind.UseTileImage = true;
            this.mtFind.Click += new System.EventHandler(this.mtFind_Click);
            // 
            // mtHome
            // 
            this.mtHome.ActiveControl = null;
            this.mtHome.Location = new System.Drawing.Point(191, 3);
            this.mtHome.Name = "mtHome";
            this.mtHome.Size = new System.Drawing.Size(89, 84);
            this.mtHome.TabIndex = 2;
            this.mtHome.Text = "Home";
            this.mtHome.TileImage = ((System.Drawing.Image)(resources.GetObject("mtHome.TileImage")));
            this.mtHome.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mtHome.UseSelectable = true;
            this.mtHome.UseTileImage = true;
            this.mtHome.Click += new System.EventHandler(this.mtHome_Click);
            // 
            // mtClear
            // 
            this.mtClear.ActiveControl = null;
            this.mtClear.Location = new System.Drawing.Point(96, 3);
            this.mtClear.Name = "mtClear";
            this.mtClear.Size = new System.Drawing.Size(89, 84);
            this.mtClear.TabIndex = 2;
            this.mtClear.Text = "Clear";
            this.mtClear.TileImage = ((System.Drawing.Image)(resources.GetObject("mtClear.TileImage")));
            this.mtClear.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mtClear.UseSelectable = true;
            this.mtClear.UseTileImage = true;
            this.mtClear.Click += new System.EventHandler(this.mtClear_Click);
            // 
            // mlTransmission
            // 
            this.mlTransmission.AutoSize = true;
            this.mlTransmission.Location = new System.Drawing.Point(15, 489);
            this.mlTransmission.Name = "mlTransmission";
            this.mlTransmission.Size = new System.Drawing.Size(84, 19);
            this.mlTransmission.TabIndex = 0;
            this.mlTransmission.Text = "Transmission:";
            // 
            // mlBodyType
            // 
            this.mlBodyType.AutoSize = true;
            this.mlBodyType.Location = new System.Drawing.Point(15, 565);
            this.mlBodyType.Name = "mlBodyType";
            this.mlBodyType.Size = new System.Drawing.Size(71, 19);
            this.mlBodyType.TabIndex = 0;
            this.mlBodyType.Text = "Body type:";
            // 
            // mcbTransmission
            // 
            this.mcbTransmission.DataSource = this.transmissionBindingSource;
            this.mcbTransmission.DisplayMember = "Transmission1";
            this.mcbTransmission.FormattingEnabled = true;
            this.mcbTransmission.ItemHeight = 23;
            this.mcbTransmission.Location = new System.Drawing.Point(15, 511);
            this.mcbTransmission.Name = "mcbTransmission";
            this.mcbTransmission.Size = new System.Drawing.Size(168, 29);
            this.mcbTransmission.TabIndex = 1;
            this.mcbTransmission.UseSelectable = true;
            this.mcbTransmission.ValueMember = "Id";
            // 
            // mcbBodyType
            // 
            this.mcbBodyType.DataSource = this.bodyBindingSource;
            this.mcbBodyType.DisplayMember = "BodyType";
            this.mcbBodyType.FormattingEnabled = true;
            this.mcbBodyType.ItemHeight = 23;
            this.mcbBodyType.Location = new System.Drawing.Point(15, 587);
            this.mcbBodyType.Name = "mcbBodyType";
            this.mcbBodyType.Size = new System.Drawing.Size(168, 29);
            this.mcbBodyType.TabIndex = 1;
            this.mcbBodyType.UseSelectable = true;
            this.mcbBodyType.ValueMember = "Id";
            // 
            // transmissionBindingSource
            // 
            this.transmissionBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Transmission);
            // 
            // bodyBindingSource
            // 
            this.bodyBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Body);
            // 
            // ucBuy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ucBuy";
            this.Size = new System.Drawing.Size(952, 793);
            this.Load += new System.EventHandler(this.ucBuy_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pSearchParam.ResumeLayout(false);
            this.pSearchParam.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.countryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.markBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCars)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.transmissionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bodyBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dgvCars;
        private System.Windows.Forms.Panel pSearchParam;
        private MetroFramework.Controls.MetroComboBox mcbYearTo;
        private MetroFramework.Controls.MetroComboBox mcbYearFrom;
        private MetroFramework.Controls.MetroComboBox mcbCountry;
        private MetroFramework.Controls.MetroComboBox mcbModel;
        private MetroFramework.Controls.MetroLabel mlYearTo;
        private MetroFramework.Controls.MetroComboBox mcbMark;
        private MetroFramework.Controls.MetroLabel mlYearFrom;
        private MetroFramework.Controls.MetroLabel mlPriceTo;
        private MetroFramework.Controls.MetroLabel mlPriceFrom;
        private MetroFramework.Controls.MetroLabel mlYear;
        private MetroFramework.Controls.MetroLabel mlPrice;
        private MetroFramework.Controls.MetroLabel mlCountry;
        private MetroFramework.Controls.MetroLabel mlModel;
        private MetroFramework.Controls.MetroLabel mlMark;
        private MetroFramework.Controls.MetroTile mtFind;
        private System.Windows.Forms.BindingSource countryBindingSource;
        private System.Windows.Forms.BindingSource markBindingSource;
        private MetroFramework.Controls.MetroTextBox mtbPriceTo;
        private MetroFramework.Controls.MetroTextBox mtbPriceFrom;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroTile mtClear;
        private MetroFramework.Controls.MetroTile mtHome;
        private MetroFramework.Controls.MetroComboBox mcbBodyType;
        private MetroFramework.Controls.MetroComboBox mcbTransmission;
        private MetroFramework.Controls.MetroLabel mlBodyType;
        private MetroFramework.Controls.MetroLabel mlTransmission;
        private System.Windows.Forms.BindingSource bodyBindingSource;
        private System.Windows.Forms.BindingSource transmissionBindingSource;
    }
}
