﻿using System;
using System.Windows.Forms;
using MetroFramework.Controls;
using Exadel.Trainings.MetroWinApp.UI.Forms;
using Exadel.Trainings.MetroWinApp.BLL.Concrete;
using MetroFramework;

namespace Exadel.Trainings.MetroWinApp.UI.Controls
{
    public partial class ucRegistration : MetroUserControl
    {
        public ucRegistration()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Returns user to the main menu.
        /// </summary>
        private void mbtnBack_Click(object sender, EventArgs e)
        {
            MainForm.Instance.MetroContainer.Controls["ucMenu"].BringToFront();
        }

        private void mbtnSignUp_Click(object sender, EventArgs e)
        {
            if (mtbUsername.Text == "" || mtbPassword.Text == "" || mtbComfirmPass.Text == "")
            {
                MetroMessageBox.Show(this, "Fill all required fields!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (mtbUsername.Text.Length < 6)
            {
                MetroMessageBox.Show(this, "Username must consist at least 6 symbols.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (mtbPassword.Text.Length < 8)
            {
                MetroMessageBox.Show(this, "Password must consist at least 8 symbols.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (mtbPassword.Text != mtbComfirmPass.Text)
            {
                MetroMessageBox.Show(this, "Passwords not equal", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                var auth = new DefaultAuthService();
                try
                {
                    var pass = Md5HashProvider.GenerateMd5Hash(mtbPassword.Text);

                    DateTime? bithDate = null;
                    if (mtbBirthdate.Text != "")
                    {
                        bithDate = DateTime.Parse(mtbBirthdate.Text);
                    }

                    var user = (new DAL.EntityFramework.User
                    {
                        Username = mtbUsername.Text,
                        Password = pass,
                        Bithdate = bithDate,
                        Phone = mtbPhone.Text,
                        Role = "user"
                    });
                    if (auth.Register(user))
                    {
                        MetroMessageBox.Show(this, "Registration successfully compleated.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        MainForm.Instance.MetroContainer.Controls["ucMenu"].BringToFront();
                    }
                    else
                    {
                        MetroMessageBox.Show(this, "User with such name is already exists.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                catch (InvalidCastException)
                {
                    MetroMessageBox.Show(this, "Your bithdate had an error and was ignored. You can insert it later in your profile.", "Message",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (Exception ex)
                {
                    MetroMessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }                   
            }
        }
    }
}
