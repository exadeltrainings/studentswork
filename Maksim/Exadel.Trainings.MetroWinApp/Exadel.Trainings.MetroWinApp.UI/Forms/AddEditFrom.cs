﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using Exadel.Trainings.MetroWinApp.DAL;
using Exadel.Trainings.MetroWinApp.DAL.Interfaces;
using MetroFramework;

namespace Exadel.Trainings.MetroWinApp.UI.Forms
{
    /// <summary>
    /// Used to add new car.
    /// </summary>
    public partial class AddEditFrom : MetroForm
    {
        //public Car CarInfo { get { return bindingSourceCars.Current as Car; } }

        private IUnitOfWork _uofw;

        public AddEditFrom()
        {
            InitializeComponent();
            //bindingSourceCars.DataSource = obj;

            _uofw = new UnitOfWork();
            BindBindingSources();
        }

        /// <summary>
        /// Binds all binding sources.
        /// </summary>
        private void BindBindingSources()
        {
            markBindingSource.DataSource = _uofw.Marks.GetAll();
            modelBindingSource.DataSource = _uofw.Models.GetAll();
            transmissionBindingSource.DataSource = _uofw.Transmissions.GetAll();
            bodyBindingSource.DataSource = _uofw.Bodies.GetAll();
            producerBindingSource.DataSource = _uofw.Producers.GetAll();
        }

        private void AddEditFrom_Load(object sender, EventArgs e)
        {
            salerTextBox.Text = CurrentUser.Username;
            salerTextBox.Enabled = false;
            carIdTextBox.Enabled = false;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (priceTextBox.Text != "" && yearTextBox.Text != "" && engineTextBox.Text != "" &&
                mileageTextBox.Text != "" && conditionTextBox.Text != "" && vinTextBox.Text != "")
            {
                try
                {
                
                    var car = new Car
                    {
                        ModelId = int.Parse(modelComboBox.SelectedValue.ToString()),
                        TransmissionId = int.Parse(transmissionComboBox.SelectedValue.ToString()),
                        BodyId = int.Parse(bodyComboBox.SelectedValue.ToString()),
                        SalerId = CurrentUser.Id,
                        Price = double.Parse(priceTextBox.Text),
                        Year = int.Parse(yearTextBox.Text),
                        Engine = engineTextBox.Text,
                        Mileage = int.Parse(mileageTextBox.Text),
                        Condition = conditionTextBox.Text,
                        VIN = vinTextBox.Text,
                        Description = descriptionTextBox.Text,
                        ProducerId = int.Parse(mcbProducer.SelectedValue.ToString())
                    };

                    _uofw.Cars.Add(car);
                    _uofw.Save();
                    MetroMessageBox.Show(this, "Car was added.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MetroMessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
            //bindingSourceCars.EndEdit();
            //DialogResult = DialogResult.OK;
        }
    }
}
