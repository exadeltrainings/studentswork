﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using Exadel.Trainings.MetroWinApp.DAL.Interfaces;
using Exadel.Trainings.MetroWinApp.DAL;

namespace Exadel.Trainings.MetroWinApp.UI.Forms
{
    /// <summary>
    /// Contains all admin posibility.
    /// </summary>
    public partial class AdminForm : MetroForm
    {
        public AdminForm()
        {
            InitializeComponent();
        }

        IUnitOfWork _uofw;

        private void MainForm_Load(object sender, EventArgs e)
        {
            _uofw = new UnitOfWork();

            BindBindingSources();
        }

        /// <summary>
        /// Binds all binding sources.
        /// </summary>
        private void BindBindingSources()
        {
            carBindingSource.DataSource = _uofw.Cars.GetAll();
            modelBindingSource.DataSource = _uofw.Models.GetAll();
            transmissionBindingSource.DataSource = _uofw.Transmissions.GetAll();
            bodyBindingSource.DataSource = _uofw.Bodies.GetAll();
            userBindingSource.DataSource = _uofw.Users.GetAll();
            producerBindingSource.DataSource = _uofw.Producers.GetAll();
        }

        private void mtSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to save the changes?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    carBindingSource.EndEdit();
                    _uofw.Save();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mtDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure want to delete this record?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var rows = dataGridView.RowCount;
                for (int i = rows - 1; i >= 0; i--)
                {
                    if (dataGridView.Rows[i].Selected)
                    {
                        _uofw.Cars.Remove(dataGridView.Rows[i].DataBoundItem as Car);
                        carBindingSource.RemoveAt(dataGridView.Rows[i].Index);
                    }
                }
            }
        }

        /// <summary>
        /// In the development!!
        /// </summary>
        private void mtEdit_Click(object sender, EventArgs e)
        {
            //var obj = carBindingSource.Current as Car;
            //if (obj != null)
            //{
            //    using (var editForm=new AddEditFrom(obj))
            //    {
            //        if (editForm.ShowDialog() == DialogResult.OK)
            //        {
            //            try
            //            {
            //                carBindingSource.EndEdit();
            //                _uofw.Save();
            //            }
            //            catch (Exception ex)
            //            {
            //                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            }
            //        }
            //    }
            //}
        }

        /// <summary>
        /// In the development!!
        /// </summary>
        private void mtAdd_Click(object sender, EventArgs e)
        {
            //using (var addForm = new AddEditFrom(new Car()))
            //{
            //    if (addForm.ShowDialog() == DialogResult.OK)
            //    {
            //        try
            //        {
            //            carBindingSource.Add(addForm.CarInfo);
            //            _uofw.Cars.Add(addForm.CarInfo);
            //            _uofw.Save();
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        }
            //    }
            //}
        }

        private void mtRefresh_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            BindBindingSources();

            Cursor.Current = Cursors.Default;
        }
    }
}
