﻿namespace Exadel.Trainings.MetroWinApp.UI.Forms
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.htmlToolTip1 = new MetroFramework.Drawing.Html.HtmlToolTip();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.modelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.producerIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.producerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transmissionIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.transmissionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bodyIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bodyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salerIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.engineDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mileageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conditionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vINDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mtSave = new MetroFramework.Controls.MetroTile();
            this.mtDelete = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.addMetroTitle = new MetroFramework.Controls.MetroTile();
            this.mtRefresh = new MetroFramework.Controls.MetroTile();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.producerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transmissionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bodyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // htmlToolTip1
            // 
            this.htmlToolTip1.OwnerDraw = true;
            // 
            // dataGridView
            // 
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.modelIdDataGridViewTextBoxColumn,
            this.producerIdDataGridViewTextBoxColumn,
            this.transmissionIdDataGridViewTextBoxColumn,
            this.bodyIdDataGridViewTextBoxColumn,
            this.salerIdDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.yearDataGridViewTextBoxColumn,
            this.engineDataGridViewTextBoxColumn,
            this.mileageDataGridViewTextBoxColumn,
            this.colorDataGridViewTextBoxColumn,
            this.conditionDataGridViewTextBoxColumn,
            this.vINDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn});
            this.dataGridView.DataSource = this.carBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView.Location = new System.Drawing.Point(41, 135);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(1071, 455);
            this.dataGridView.TabIndex = 5;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.FillWeight = 59.92218F;
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modelIdDataGridViewTextBoxColumn
            // 
            this.modelIdDataGridViewTextBoxColumn.DataPropertyName = "ModelId";
            this.modelIdDataGridViewTextBoxColumn.DataSource = this.modelBindingSource;
            this.modelIdDataGridViewTextBoxColumn.DisplayMember = "Model1";
            this.modelIdDataGridViewTextBoxColumn.FillWeight = 96.22662F;
            this.modelIdDataGridViewTextBoxColumn.HeaderText = "Model";
            this.modelIdDataGridViewTextBoxColumn.Name = "modelIdDataGridViewTextBoxColumn";
            this.modelIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.modelIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.modelIdDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // modelBindingSource
            // 
            this.modelBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Model);
            // 
            // producerIdDataGridViewTextBoxColumn
            // 
            this.producerIdDataGridViewTextBoxColumn.DataPropertyName = "ProducerId";
            this.producerIdDataGridViewTextBoxColumn.DataSource = this.producerBindingSource;
            this.producerIdDataGridViewTextBoxColumn.DisplayMember = "Producer1";
            this.producerIdDataGridViewTextBoxColumn.FillWeight = 133.3494F;
            this.producerIdDataGridViewTextBoxColumn.HeaderText = "Producer";
            this.producerIdDataGridViewTextBoxColumn.Name = "producerIdDataGridViewTextBoxColumn";
            this.producerIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.producerIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.producerIdDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // producerBindingSource
            // 
            this.producerBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Producer);
            // 
            // transmissionIdDataGridViewTextBoxColumn
            // 
            this.transmissionIdDataGridViewTextBoxColumn.DataPropertyName = "TransmissionId";
            this.transmissionIdDataGridViewTextBoxColumn.DataSource = this.transmissionBindingSource;
            this.transmissionIdDataGridViewTextBoxColumn.DisplayMember = "Transmission1";
            this.transmissionIdDataGridViewTextBoxColumn.FillWeight = 152.2376F;
            this.transmissionIdDataGridViewTextBoxColumn.HeaderText = "Transmission";
            this.transmissionIdDataGridViewTextBoxColumn.Name = "transmissionIdDataGridViewTextBoxColumn";
            this.transmissionIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.transmissionIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.transmissionIdDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // transmissionBindingSource
            // 
            this.transmissionBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Transmission);
            // 
            // bodyIdDataGridViewTextBoxColumn
            // 
            this.bodyIdDataGridViewTextBoxColumn.DataPropertyName = "BodyId";
            this.bodyIdDataGridViewTextBoxColumn.DataSource = this.bodyBindingSource;
            this.bodyIdDataGridViewTextBoxColumn.DisplayMember = "BodyType";
            this.bodyIdDataGridViewTextBoxColumn.FillWeight = 131.8932F;
            this.bodyIdDataGridViewTextBoxColumn.HeaderText = "Body Type";
            this.bodyIdDataGridViewTextBoxColumn.Name = "bodyIdDataGridViewTextBoxColumn";
            this.bodyIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bodyIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.bodyIdDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // bodyBindingSource
            // 
            this.bodyBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Body);
            // 
            // salerIdDataGridViewTextBoxColumn
            // 
            this.salerIdDataGridViewTextBoxColumn.DataPropertyName = "SalerId";
            this.salerIdDataGridViewTextBoxColumn.DataSource = this.userBindingSource;
            this.salerIdDataGridViewTextBoxColumn.DisplayMember = "Username";
            this.salerIdDataGridViewTextBoxColumn.FillWeight = 84.5552F;
            this.salerIdDataGridViewTextBoxColumn.HeaderText = "Saler";
            this.salerIdDataGridViewTextBoxColumn.Name = "salerIdDataGridViewTextBoxColumn";
            this.salerIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.salerIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.salerIdDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.User);
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.FillWeight = 82.44426F;
            this.priceDataGridViewTextBoxColumn.HeaderText = "Price";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            // 
            // yearDataGridViewTextBoxColumn
            // 
            this.yearDataGridViewTextBoxColumn.DataPropertyName = "Year";
            this.yearDataGridViewTextBoxColumn.FillWeight = 79.17559F;
            this.yearDataGridViewTextBoxColumn.HeaderText = "Year";
            this.yearDataGridViewTextBoxColumn.Name = "yearDataGridViewTextBoxColumn";
            // 
            // engineDataGridViewTextBoxColumn
            // 
            this.engineDataGridViewTextBoxColumn.DataPropertyName = "Engine";
            this.engineDataGridViewTextBoxColumn.FillWeight = 95.05566F;
            this.engineDataGridViewTextBoxColumn.HeaderText = "Engine";
            this.engineDataGridViewTextBoxColumn.Name = "engineDataGridViewTextBoxColumn";
            // 
            // mileageDataGridViewTextBoxColumn
            // 
            this.mileageDataGridViewTextBoxColumn.DataPropertyName = "Mileage";
            this.mileageDataGridViewTextBoxColumn.FillWeight = 103.0931F;
            this.mileageDataGridViewTextBoxColumn.HeaderText = "Mileage";
            this.mileageDataGridViewTextBoxColumn.Name = "mileageDataGridViewTextBoxColumn";
            // 
            // colorDataGridViewTextBoxColumn
            // 
            this.colorDataGridViewTextBoxColumn.DataPropertyName = "Color";
            this.colorDataGridViewTextBoxColumn.FillWeight = 81.02641F;
            this.colorDataGridViewTextBoxColumn.HeaderText = "Color";
            this.colorDataGridViewTextBoxColumn.Name = "colorDataGridViewTextBoxColumn";
            // 
            // conditionDataGridViewTextBoxColumn
            // 
            this.conditionDataGridViewTextBoxColumn.DataPropertyName = "Condition";
            this.conditionDataGridViewTextBoxColumn.FillWeight = 110.1845F;
            this.conditionDataGridViewTextBoxColumn.HeaderText = "Condition";
            this.conditionDataGridViewTextBoxColumn.Name = "conditionDataGridViewTextBoxColumn";
            // 
            // vINDataGridViewTextBoxColumn
            // 
            this.vINDataGridViewTextBoxColumn.DataPropertyName = "VIN";
            this.vINDataGridViewTextBoxColumn.FillWeight = 67.5253F;
            this.vINDataGridViewTextBoxColumn.HeaderText = "VIN";
            this.vINDataGridViewTextBoxColumn.Name = "vINDataGridViewTextBoxColumn";
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.FillWeight = 123.311F;
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            // 
            // carBindingSource
            // 
            this.carBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Car);
            // 
            // mtSave
            // 
            this.mtSave.ActiveControl = null;
            this.mtSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.mtSave.Location = new System.Drawing.Point(365, 63);
            this.mtSave.Name = "mtSave";
            this.mtSave.Size = new System.Drawing.Size(75, 66);
            this.mtSave.TabIndex = 4;
            this.mtSave.Text = "Save";
            this.mtSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mtSave.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.save;
            this.mtSave.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mtSave.UseCustomBackColor = true;
            this.mtSave.UseSelectable = true;
            this.mtSave.UseTileImage = true;
            this.mtSave.Click += new System.EventHandler(this.mtSave_Click);
            // 
            // mtDelete
            // 
            this.mtDelete.ActiveControl = null;
            this.mtDelete.Location = new System.Drawing.Point(284, 63);
            this.mtDelete.Name = "mtDelete";
            this.mtDelete.Size = new System.Drawing.Size(75, 66);
            this.mtDelete.TabIndex = 3;
            this.mtDelete.Text = "Delete";
            this.mtDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mtDelete.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.delete;
            this.mtDelete.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mtDelete.UseSelectable = true;
            this.mtDelete.UseTileImage = true;
            this.mtDelete.Click += new System.EventHandler(this.mtDelete_Click);
            // 
            // mtEdit
            // 
            this.mtEdit.ActiveControl = null;
            this.mtEdit.Enabled = false;
            this.mtEdit.Location = new System.Drawing.Point(203, 63);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(75, 66);
            this.mtEdit.TabIndex = 2;
            this.mtEdit.Text = "Edit";
            this.mtEdit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mtEdit.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.edit;
            this.mtEdit.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mtEdit.UseSelectable = true;
            this.mtEdit.UseTileImage = true;
            this.mtEdit.Visible = false;
            this.mtEdit.Click += new System.EventHandler(this.mtEdit_Click);
            // 
            // addMetroTitle
            // 
            this.addMetroTitle.ActiveControl = null;
            this.addMetroTitle.Enabled = false;
            this.addMetroTitle.Location = new System.Drawing.Point(122, 63);
            this.addMetroTitle.Name = "addMetroTitle";
            this.addMetroTitle.Size = new System.Drawing.Size(75, 66);
            this.addMetroTitle.TabIndex = 1;
            this.addMetroTitle.Text = "Add";
            this.addMetroTitle.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addMetroTitle.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.add;
            this.addMetroTitle.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.addMetroTitle.UseSelectable = true;
            this.addMetroTitle.UseTileImage = true;
            this.addMetroTitle.Visible = false;
            this.addMetroTitle.Click += new System.EventHandler(this.mtAdd_Click);
            // 
            // mtRefresh
            // 
            this.mtRefresh.ActiveControl = null;
            this.mtRefresh.Location = new System.Drawing.Point(41, 63);
            this.mtRefresh.Name = "mtRefresh";
            this.mtRefresh.Size = new System.Drawing.Size(75, 66);
            this.mtRefresh.TabIndex = 0;
            this.mtRefresh.Text = "Refresh";
            this.mtRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mtRefresh.TileImage = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.refresh;
            this.mtRefresh.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mtRefresh.UseSelectable = true;
            this.mtRefresh.UseTileImage = true;
            this.mtRefresh.Click += new System.EventHandler(this.mtRefresh_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 685);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.mtSave);
            this.Controls.Add(this.mtDelete);
            this.Controls.Add(this.mtEdit);
            this.Controls.Add(this.addMetroTitle);
            this.Controls.Add(this.mtRefresh);
            this.Name = "AdminForm";
            this.Text = "Cars";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.producerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transmissionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bodyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Drawing.Html.HtmlToolTip htmlToolTip1;
        private MetroFramework.Controls.MetroTile mtRefresh;
        private MetroFramework.Controls.MetroTile addMetroTitle;
        private MetroFramework.Controls.MetroTile mtEdit;
        private MetroFramework.Controls.MetroTile mtDelete;
        private MetroFramework.Controls.MetroTile mtSave;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.BindingSource modelBindingSource;
        private System.Windows.Forms.BindingSource carBindingSource;
        private System.Windows.Forms.BindingSource transmissionBindingSource;
        private System.Windows.Forms.BindingSource bodyBindingSource;
        private System.Windows.Forms.BindingSource userBindingSource;
        private System.Windows.Forms.BindingSource producerBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn modelIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn producerIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn transmissionIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn bodyIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn salerIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yearDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn engineDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mileageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conditionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vINDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
    }
}

