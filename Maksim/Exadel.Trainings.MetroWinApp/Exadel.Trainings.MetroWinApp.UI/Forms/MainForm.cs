﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Threading;
using MetroFramework.Controls;
using Exadel.Trainings.MetroWinApp.UI.Controls;

namespace Exadel.Trainings.MetroWinApp.UI.Forms
{
    /// <summary>
    /// App main form.
    /// </summary>
    public partial class MainForm : MetroForm
    {
        //Singleton pattern.
        private static MainForm _instance;

        /// <summary>
        /// Provides an access to the main form.
        /// </summary>
        public static MainForm Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MainForm();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Provides an access to the panel with different menu at the main form.
        /// </summary>
        public MetroPanel MetroContainer
        {
            get { return this.mPanel; }
            set { this.mPanel = value; }
        }

        public MainForm()
        {
            //Create new thread for the Splash screen.
            var th = new Thread(new ThreadStart(StartForm));
            th.Start();
            //Imitation of the long initialization process.
            Thread.Sleep(3000);
            //While is Initialization of the components take place, splash screen will be presented to the user.
            InitializeComponent();
            //When initialization process will be completed, splash screen will be disappeared.
            th.Abort();
        }

        /// <summary>
        /// Runs SplashScreen form.
        /// </summary>
        public void StartForm()
        {
            Application.Run(new SplashScreenForm());
        }

        private void mlExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //Creates user control with menu
            var uc = new ucMenu();
            uc.Dock = DockStyle.Fill;
            mPanel.Controls.Add(uc);
            //and represents.
            _instance = this;
        }

        private void mlLogin_Click(object sender, EventArgs e)
        {
            if (mlLogin.Text == "Login")
            {
                //Creates user control with login menu
                var uc = new ucLogin();
                uc.Dock = DockStyle.Fill;
                //adds to the panel with different menu
                mPanel.Controls.Add(uc);
                //represents to the user.
                Instance.MetroContainer.Controls["ucLogin"].BringToFront();
            }
            else if (mlLogin.Text == "logout")
            {
                mlLogin.Text = "Login";
                mlLogin.Image = Image.FromFile(@"..\..\Resources\appbar.cabinet.in.png");
                MainForm.Instance.MetroContainer.Controls["ucMenu"].Controls["tableLayoutPanel"].Controls["mtProfile"].Enabled = false;
                CurrentUser.Logout();
                MainForm.Instance.MetroContainer.Controls["ucMenu"].Controls["tableLayoutPanel"].Controls["mtAdminPanel"].Visible = false;
                MainForm.Instance.MetroContainer.Controls["ucMenu"].BringToFront();
            }
        }

        private void mlSignUp_Click(object sender, EventArgs e)
        {
            //Creates user control with registration menu
            var uc = new ucRegistration();
            uc.Dock = DockStyle.Fill;
            //adds to the panel with different menu
            mPanel.Controls.Add(uc);
            //represents to the user.
            Instance.MetroContainer.Controls["ucRegistration"].BringToFront();
        }
    }
}
