﻿namespace Exadel.Trainings.MetroWinApp.UI.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mPanel = new MetroFramework.Controls.MetroPanel();
            this.mlSignUp = new MetroFramework.Controls.MetroLink();
            this.mlLogin = new MetroFramework.Controls.MetroLink();
            this.metroLink2 = new MetroFramework.Controls.MetroLink();
            this.mlExit = new MetroFramework.Controls.MetroLink();
            this.SuspendLayout();
            // 
            // mPanel
            // 
            this.mPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mPanel.HorizontalScrollbarBarColor = true;
            this.mPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.mPanel.HorizontalScrollbarSize = 10;
            this.mPanel.Location = new System.Drawing.Point(23, 66);
            this.mPanel.Name = "mPanel";
            this.mPanel.Size = new System.Drawing.Size(718, 360);
            this.mPanel.TabIndex = 3;
            this.mPanel.VerticalScrollbarBarColor = true;
            this.mPanel.VerticalScrollbarHighlightOnWheel = false;
            this.mPanel.VerticalScrollbarSize = 10;
            // 
            // mlSignUp
            // 
            this.mlSignUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mlSignUp.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.mlSignUp.Image = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_paw;
            this.mlSignUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mlSignUp.ImageSize = 50;
            this.mlSignUp.Location = new System.Drawing.Point(470, 20);
            this.mlSignUp.Name = "mlSignUp";
            this.mlSignUp.Size = new System.Drawing.Size(109, 40);
            this.mlSignUp.TabIndex = 2;
            this.mlSignUp.TabStop = false;
            this.mlSignUp.Text = "Sign Up";
            this.mlSignUp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.mlSignUp.UseSelectable = true;
            this.mlSignUp.Click += new System.EventHandler(this.mlSignUp_Click);
            // 
            // mlLogin
            // 
            this.mlLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mlLogin.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.mlLogin.Image = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.appbar_cabinet_in;
            this.mlLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mlLogin.ImageSize = 50;
            this.mlLogin.Location = new System.Drawing.Point(369, 20);
            this.mlLogin.Name = "mlLogin";
            this.mlLogin.Size = new System.Drawing.Size(95, 40);
            this.mlLogin.TabIndex = 2;
            this.mlLogin.TabStop = false;
            this.mlLogin.Text = "Login";
            this.mlLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.mlLogin.UseSelectable = true;
            this.mlLogin.Click += new System.EventHandler(this.mlLogin_Click);
            // 
            // metroLink2
            // 
            this.metroLink2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLink2.Image = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.settings;
            this.metroLink2.ImageSize = 50;
            this.metroLink2.Location = new System.Drawing.Point(691, 17);
            this.metroLink2.Name = "metroLink2";
            this.metroLink2.Size = new System.Drawing.Size(50, 43);
            this.metroLink2.TabIndex = 0;
            this.metroLink2.TabStop = false;
            this.metroLink2.UseSelectable = true;
            // 
            // mlExit
            // 
            this.mlExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mlExit.Image = global::Exadel.Trainings.MetroWinApp.UI.Properties.Resources.cancel;
            this.mlExit.ImageSize = 50;
            this.mlExit.Location = new System.Drawing.Point(635, 17);
            this.mlExit.Name = "mlExit";
            this.mlExit.Size = new System.Drawing.Size(50, 43);
            this.mlExit.TabIndex = 0;
            this.mlExit.TabStop = false;
            this.mlExit.UseSelectable = true;
            this.mlExit.Click += new System.EventHandler(this.mlExit_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 451);
            this.ControlBox = false;
            this.Controls.Add(this.mPanel);
            this.Controls.Add(this.mlSignUp);
            this.Controls.Add(this.mlLogin);
            this.Controls.Add(this.metroLink2);
            this.Controls.Add(this.mlExit);
            this.MinimumSize = new System.Drawing.Size(764, 451);
            this.Name = "MainForm";
            this.Text = "DIVINE DETAILS";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLink mlExit;
        private MetroFramework.Controls.MetroLink metroLink2;
        private MetroFramework.Controls.MetroLink mlLogin;
        private MetroFramework.Controls.MetroLink mlSignUp;
        private MetroFramework.Controls.MetroPanel mPanel;
    }
}