﻿namespace Exadel.Trainings.MetroWinApp.UI.Forms
{
    partial class AddEditFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.carIdTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.markComboBox = new MetroFramework.Controls.MetroComboBox();
            this.modelComboBox = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.priceTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.yearTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.descriptionTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.saveButton = new MetroFramework.Controls.MetroButton();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.transmissionComboBox = new MetroFramework.Controls.MetroComboBox();
            this.bodyComboBox = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.salerTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.mileageTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.engineTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.conditionTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.vinTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.markBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.modelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transmissionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bodyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mlProducer = new MetroFramework.Controls.MetroLabel();
            this.mcbProducer = new MetroFramework.Controls.MetroComboBox();
            this.producerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.markBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transmissionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bodyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.producerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(49, 77);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(49, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Car ID:";
            // 
            // carIdTextBox
            // 
            this.carIdTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.carIdTextBox.CustomButton.Image = null;
            this.carIdTextBox.CustomButton.Location = new System.Drawing.Point(140, 1);
            this.carIdTextBox.CustomButton.Name = "";
            this.carIdTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.carIdTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.carIdTextBox.CustomButton.TabIndex = 1;
            this.carIdTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.carIdTextBox.CustomButton.UseSelectable = true;
            this.carIdTextBox.CustomButton.Visible = false;
            this.carIdTextBox.Lines = new string[0];
            this.carIdTextBox.Location = new System.Drawing.Point(166, 73);
            this.carIdTextBox.MaxLength = 32767;
            this.carIdTextBox.Name = "carIdTextBox";
            this.carIdTextBox.PasswordChar = '\0';
            this.carIdTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.carIdTextBox.SelectedText = "";
            this.carIdTextBox.SelectionLength = 0;
            this.carIdTextBox.SelectionStart = 0;
            this.carIdTextBox.ShortcutsEnabled = true;
            this.carIdTextBox.Size = new System.Drawing.Size(162, 23);
            this.carIdTextBox.TabIndex = 1;
            this.carIdTextBox.UseCustomBackColor = true;
            this.carIdTextBox.UseSelectable = true;
            this.carIdTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.carIdTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(49, 112);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(42, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Mark:";
            // 
            // markComboBox
            // 
            this.markComboBox.DataSource = this.markBindingSource;
            this.markComboBox.DisplayMember = "Mark1";
            this.markComboBox.FormattingEnabled = true;
            this.markComboBox.ItemHeight = 23;
            this.markComboBox.Location = new System.Drawing.Point(166, 102);
            this.markComboBox.Name = "markComboBox";
            this.markComboBox.Size = new System.Drawing.Size(162, 29);
            this.markComboBox.TabIndex = 13;
            this.markComboBox.UseSelectable = true;
            this.markComboBox.ValueMember = "Id";
            // 
            // modelComboBox
            // 
            this.modelComboBox.DataSource = this.modelBindingSource;
            this.modelComboBox.DisplayMember = "Model1";
            this.modelComboBox.FormattingEnabled = true;
            this.modelComboBox.ItemHeight = 23;
            this.modelComboBox.Location = new System.Drawing.Point(166, 137);
            this.modelComboBox.Name = "modelComboBox";
            this.modelComboBox.Size = new System.Drawing.Size(162, 29);
            this.modelComboBox.TabIndex = 15;
            this.modelComboBox.UseSelectable = true;
            this.modelComboBox.ValueMember = "Id";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(49, 147);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(50, 19);
            this.metroLabel3.TabIndex = 14;
            this.metroLabel3.Text = "Model:";
            // 
            // priceTextBox
            // 
            this.priceTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.priceTextBox.CustomButton.Image = null;
            this.priceTextBox.CustomButton.Location = new System.Drawing.Point(140, 1);
            this.priceTextBox.CustomButton.Name = "";
            this.priceTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.priceTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.priceTextBox.CustomButton.TabIndex = 1;
            this.priceTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.priceTextBox.CustomButton.UseSelectable = true;
            this.priceTextBox.CustomButton.Visible = false;
            this.priceTextBox.Lines = new string[0];
            this.priceTextBox.Location = new System.Drawing.Point(166, 307);
            this.priceTextBox.MaxLength = 32767;
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.PasswordChar = '\0';
            this.priceTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.priceTextBox.SelectedText = "";
            this.priceTextBox.SelectionLength = 0;
            this.priceTextBox.SelectionStart = 0;
            this.priceTextBox.ShortcutsEnabled = true;
            this.priceTextBox.Size = new System.Drawing.Size(162, 23);
            this.priceTextBox.TabIndex = 17;
            this.priceTextBox.UseCustomBackColor = true;
            this.priceTextBox.UseSelectable = true;
            this.priceTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.priceTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(49, 311);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(41, 19);
            this.metroLabel4.TabIndex = 16;
            this.metroLabel4.Text = "Price:";
            // 
            // yearTextBox
            // 
            this.yearTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.yearTextBox.CustomButton.Image = null;
            this.yearTextBox.CustomButton.Location = new System.Drawing.Point(140, 1);
            this.yearTextBox.CustomButton.Name = "";
            this.yearTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.yearTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.yearTextBox.CustomButton.TabIndex = 1;
            this.yearTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.yearTextBox.CustomButton.UseSelectable = true;
            this.yearTextBox.CustomButton.Visible = false;
            this.yearTextBox.Lines = new string[0];
            this.yearTextBox.Location = new System.Drawing.Point(166, 336);
            this.yearTextBox.MaxLength = 32767;
            this.yearTextBox.Name = "yearTextBox";
            this.yearTextBox.PasswordChar = '\0';
            this.yearTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.yearTextBox.SelectedText = "";
            this.yearTextBox.SelectionLength = 0;
            this.yearTextBox.SelectionStart = 0;
            this.yearTextBox.ShortcutsEnabled = true;
            this.yearTextBox.Size = new System.Drawing.Size(162, 23);
            this.yearTextBox.TabIndex = 19;
            this.yearTextBox.UseCustomBackColor = true;
            this.yearTextBox.UseSelectable = true;
            this.yearTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.yearTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(49, 340);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(38, 19);
            this.metroLabel5.TabIndex = 18;
            this.metroLabel5.Text = "Year:";
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.descriptionTextBox.CustomButton.Image = null;
            this.descriptionTextBox.CustomButton.Location = new System.Drawing.Point(160, 1);
            this.descriptionTextBox.CustomButton.Name = "";
            this.descriptionTextBox.CustomButton.Size = new System.Drawing.Size(75, 75);
            this.descriptionTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.descriptionTextBox.CustomButton.TabIndex = 1;
            this.descriptionTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.descriptionTextBox.CustomButton.UseSelectable = true;
            this.descriptionTextBox.CustomButton.Visible = false;
            this.descriptionTextBox.Lines = new string[0];
            this.descriptionTextBox.Location = new System.Drawing.Point(166, 481);
            this.descriptionTextBox.MaxLength = 32767;
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.PasswordChar = '\0';
            this.descriptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.descriptionTextBox.SelectedText = "";
            this.descriptionTextBox.SelectionLength = 0;
            this.descriptionTextBox.SelectionStart = 0;
            this.descriptionTextBox.ShortcutsEnabled = true;
            this.descriptionTextBox.Size = new System.Drawing.Size(236, 77);
            this.descriptionTextBox.TabIndex = 25;
            this.descriptionTextBox.UseCustomBackColor = true;
            this.descriptionTextBox.UseSelectable = true;
            this.descriptionTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.descriptionTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(49, 485);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(77, 19);
            this.metroLabel8.TabIndex = 24;
            this.metroLabel8.Text = "Description:";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(327, 564);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 26;
            this.saveButton.Text = "&Save";
            this.saveButton.UseSelectable = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(49, 176);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(84, 19);
            this.metroLabel9.TabIndex = 27;
            this.metroLabel9.Text = "Transmission:";
            // 
            // transmissionComboBox
            // 
            this.transmissionComboBox.DataSource = this.transmissionBindingSource;
            this.transmissionComboBox.DisplayMember = "Transmission1";
            this.transmissionComboBox.FormattingEnabled = true;
            this.transmissionComboBox.ItemHeight = 23;
            this.transmissionComboBox.Location = new System.Drawing.Point(166, 172);
            this.transmissionComboBox.Name = "transmissionComboBox";
            this.transmissionComboBox.Size = new System.Drawing.Size(162, 29);
            this.transmissionComboBox.TabIndex = 28;
            this.transmissionComboBox.UseSelectable = true;
            this.transmissionComboBox.ValueMember = "Id";
            // 
            // bodyComboBox
            // 
            this.bodyComboBox.DataSource = this.bodyBindingSource;
            this.bodyComboBox.DisplayMember = "BodyType";
            this.bodyComboBox.FormattingEnabled = true;
            this.bodyComboBox.ItemHeight = 23;
            this.bodyComboBox.Location = new System.Drawing.Point(166, 207);
            this.bodyComboBox.Name = "bodyComboBox";
            this.bodyComboBox.Size = new System.Drawing.Size(162, 29);
            this.bodyComboBox.TabIndex = 30;
            this.bodyComboBox.UseSelectable = true;
            this.bodyComboBox.ValueMember = "Id";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(49, 211);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(73, 19);
            this.metroLabel10.TabIndex = 29;
            this.metroLabel10.Text = "Body Type:";
            // 
            // salerTextBox
            // 
            this.salerTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.salerTextBox.CustomButton.Image = null;
            this.salerTextBox.CustomButton.Location = new System.Drawing.Point(140, 1);
            this.salerTextBox.CustomButton.Name = "";
            this.salerTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.salerTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.salerTextBox.CustomButton.TabIndex = 1;
            this.salerTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.salerTextBox.CustomButton.UseSelectable = true;
            this.salerTextBox.CustomButton.Visible = false;
            this.salerTextBox.Lines = new string[0];
            this.salerTextBox.Location = new System.Drawing.Point(166, 278);
            this.salerTextBox.MaxLength = 32767;
            this.salerTextBox.Name = "salerTextBox";
            this.salerTextBox.PasswordChar = '\0';
            this.salerTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.salerTextBox.SelectedText = "";
            this.salerTextBox.SelectionLength = 0;
            this.salerTextBox.SelectionStart = 0;
            this.salerTextBox.ShortcutsEnabled = true;
            this.salerTextBox.Size = new System.Drawing.Size(162, 23);
            this.salerTextBox.TabIndex = 32;
            this.salerTextBox.UseCustomBackColor = true;
            this.salerTextBox.UseSelectable = true;
            this.salerTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.salerTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(49, 282);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(42, 19);
            this.metroLabel11.TabIndex = 31;
            this.metroLabel11.Text = "Saler:";
            // 
            // mileageTextBox
            // 
            this.mileageTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.mileageTextBox.CustomButton.Image = null;
            this.mileageTextBox.CustomButton.Location = new System.Drawing.Point(140, 1);
            this.mileageTextBox.CustomButton.Name = "";
            this.mileageTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mileageTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mileageTextBox.CustomButton.TabIndex = 1;
            this.mileageTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mileageTextBox.CustomButton.UseSelectable = true;
            this.mileageTextBox.CustomButton.Visible = false;
            this.mileageTextBox.Lines = new string[0];
            this.mileageTextBox.Location = new System.Drawing.Point(166, 394);
            this.mileageTextBox.MaxLength = 32767;
            this.mileageTextBox.Name = "mileageTextBox";
            this.mileageTextBox.PasswordChar = '\0';
            this.mileageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mileageTextBox.SelectedText = "";
            this.mileageTextBox.SelectionLength = 0;
            this.mileageTextBox.SelectionStart = 0;
            this.mileageTextBox.ShortcutsEnabled = true;
            this.mileageTextBox.Size = new System.Drawing.Size(162, 23);
            this.mileageTextBox.TabIndex = 36;
            this.mileageTextBox.UseCustomBackColor = true;
            this.mileageTextBox.UseSelectable = true;
            this.mileageTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mileageTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(49, 398);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(59, 19);
            this.metroLabel12.TabIndex = 35;
            this.metroLabel12.Text = "Mileage:";
            // 
            // engineTextBox
            // 
            this.engineTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.engineTextBox.CustomButton.Image = null;
            this.engineTextBox.CustomButton.Location = new System.Drawing.Point(140, 1);
            this.engineTextBox.CustomButton.Name = "";
            this.engineTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.engineTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.engineTextBox.CustomButton.TabIndex = 1;
            this.engineTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.engineTextBox.CustomButton.UseSelectable = true;
            this.engineTextBox.CustomButton.Visible = false;
            this.engineTextBox.Lines = new string[0];
            this.engineTextBox.Location = new System.Drawing.Point(166, 365);
            this.engineTextBox.MaxLength = 32767;
            this.engineTextBox.Name = "engineTextBox";
            this.engineTextBox.PasswordChar = '\0';
            this.engineTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.engineTextBox.SelectedText = "";
            this.engineTextBox.SelectionLength = 0;
            this.engineTextBox.SelectionStart = 0;
            this.engineTextBox.ShortcutsEnabled = true;
            this.engineTextBox.Size = new System.Drawing.Size(162, 23);
            this.engineTextBox.TabIndex = 34;
            this.engineTextBox.UseCustomBackColor = true;
            this.engineTextBox.UseSelectable = true;
            this.engineTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.engineTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(49, 369);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(51, 19);
            this.metroLabel13.TabIndex = 33;
            this.metroLabel13.Text = "Engine:";
            // 
            // conditionTextBox
            // 
            this.conditionTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.conditionTextBox.CustomButton.Image = null;
            this.conditionTextBox.CustomButton.Location = new System.Drawing.Point(140, 1);
            this.conditionTextBox.CustomButton.Name = "";
            this.conditionTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.conditionTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.conditionTextBox.CustomButton.TabIndex = 1;
            this.conditionTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.conditionTextBox.CustomButton.UseSelectable = true;
            this.conditionTextBox.CustomButton.Visible = false;
            this.conditionTextBox.Lines = new string[0];
            this.conditionTextBox.Location = new System.Drawing.Point(166, 423);
            this.conditionTextBox.MaxLength = 32767;
            this.conditionTextBox.Name = "conditionTextBox";
            this.conditionTextBox.PasswordChar = '\0';
            this.conditionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.conditionTextBox.SelectedText = "";
            this.conditionTextBox.SelectionLength = 0;
            this.conditionTextBox.SelectionStart = 0;
            this.conditionTextBox.ShortcutsEnabled = true;
            this.conditionTextBox.Size = new System.Drawing.Size(162, 23);
            this.conditionTextBox.TabIndex = 38;
            this.conditionTextBox.UseCustomBackColor = true;
            this.conditionTextBox.UseSelectable = true;
            this.conditionTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.conditionTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(49, 427);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(69, 19);
            this.metroLabel14.TabIndex = 37;
            this.metroLabel14.Text = "Condition:";
            // 
            // vinTextBox
            // 
            this.vinTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            this.vinTextBox.CustomButton.Image = null;
            this.vinTextBox.CustomButton.Location = new System.Drawing.Point(140, 1);
            this.vinTextBox.CustomButton.Name = "";
            this.vinTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.vinTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.vinTextBox.CustomButton.TabIndex = 1;
            this.vinTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.vinTextBox.CustomButton.UseSelectable = true;
            this.vinTextBox.CustomButton.Visible = false;
            this.vinTextBox.Lines = new string[0];
            this.vinTextBox.Location = new System.Drawing.Point(166, 452);
            this.vinTextBox.MaxLength = 32767;
            this.vinTextBox.Name = "vinTextBox";
            this.vinTextBox.PasswordChar = '\0';
            this.vinTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.vinTextBox.SelectedText = "";
            this.vinTextBox.SelectionLength = 0;
            this.vinTextBox.SelectionStart = 0;
            this.vinTextBox.ShortcutsEnabled = true;
            this.vinTextBox.Size = new System.Drawing.Size(162, 23);
            this.vinTextBox.TabIndex = 40;
            this.vinTextBox.UseCustomBackColor = true;
            this.vinTextBox.UseSelectable = true;
            this.vinTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.vinTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(49, 456);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(33, 19);
            this.metroLabel15.TabIndex = 39;
            this.metroLabel15.Text = "VIN:";
            // 
            // markBindingSource
            // 
            this.markBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Mark);
            // 
            // modelBindingSource
            // 
            this.modelBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Model);
            // 
            // transmissionBindingSource
            // 
            this.transmissionBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Transmission);
            // 
            // bodyBindingSource
            // 
            this.bodyBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Body);
            // 
            // mlProducer
            // 
            this.mlProducer.AutoSize = true;
            this.mlProducer.Location = new System.Drawing.Point(49, 246);
            this.mlProducer.Name = "mlProducer";
            this.mlProducer.Size = new System.Drawing.Size(67, 19);
            this.mlProducer.TabIndex = 29;
            this.mlProducer.Text = "Producer:";
            // 
            // mcbProducer
            // 
            this.mcbProducer.DataSource = this.producerBindingSource;
            this.mcbProducer.DisplayMember = "Producer1";
            this.mcbProducer.FormattingEnabled = true;
            this.mcbProducer.ItemHeight = 23;
            this.mcbProducer.Location = new System.Drawing.Point(166, 242);
            this.mcbProducer.Name = "mcbProducer";
            this.mcbProducer.Size = new System.Drawing.Size(162, 29);
            this.mcbProducer.TabIndex = 30;
            this.mcbProducer.UseSelectable = true;
            this.mcbProducer.ValueMember = "Id";
            // 
            // producerBindingSource
            // 
            this.producerBindingSource.DataSource = typeof(Exadel.Trainings.MetroWinApp.DAL.EntityFramework.Producer);
            // 
            // AddEditFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 607);
            this.Controls.Add(this.vinTextBox);
            this.Controls.Add(this.metroLabel15);
            this.Controls.Add(this.conditionTextBox);
            this.Controls.Add(this.metroLabel14);
            this.Controls.Add(this.mileageTextBox);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.engineTextBox);
            this.Controls.Add(this.metroLabel13);
            this.Controls.Add(this.salerTextBox);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.mcbProducer);
            this.Controls.Add(this.mlProducer);
            this.Controls.Add(this.bodyComboBox);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.transmissionComboBox);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.yearTextBox);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.modelComboBox);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.markComboBox);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.carIdTextBox);
            this.Controls.Add(this.metroLabel1);
            this.MaximizeBox = false;
            this.Name = "AddEditFrom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Car Info";
            this.Load += new System.EventHandler(this.AddEditFrom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.markBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transmissionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bodyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.producerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox carIdTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox markComboBox;
        private MetroFramework.Controls.MetroComboBox modelComboBox;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox priceTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox yearTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox descriptionTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroButton saveButton;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox transmissionComboBox;
        private MetroFramework.Controls.MetroComboBox bodyComboBox;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroTextBox salerTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox mileageTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroTextBox engineTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroTextBox conditionTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroTextBox vinTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private System.Windows.Forms.BindingSource markBindingSource;
        private System.Windows.Forms.BindingSource modelBindingSource;
        private System.Windows.Forms.BindingSource transmissionBindingSource;
        private System.Windows.Forms.BindingSource bodyBindingSource;
        private MetroFramework.Controls.MetroLabel mlProducer;
        private MetroFramework.Controls.MetroComboBox mcbProducer;
        private System.Windows.Forms.BindingSource producerBindingSource;
    }
}