﻿using MetroFramework.Controls;
using System.Windows.Forms;

namespace Exadel.Trainings.MetroWinApp.UI.Helpers
{
    /// <summary>
    /// Class represents methods that used in different places in UI.
    /// </summary>
    public static class ControlsHelper
    {
        /// <summary>
        /// Method used to generate DataGridView column headers.
        /// </summary>
        /// <param name="dgv">DataGridView for which will be generated headers.</param>
        public static void GenerateDataGridHeaders(DataGridView dgv)
        {
            dgv.Columns.Add("Mark", "Mark");
            dgv.Columns.Add("Model", "Model");
            dgv.Columns.Add("Country", "Country");
            dgv.Columns.Add("Transmission", "Transmission");
            dgv.Columns.Add("BodyType", "Body type");
            dgv.Columns.Add("Saler", "Saler");
            dgv.Columns.Add("Price", "Price");
            dgv.Columns.Add("Year", "Year");
            dgv.Columns.Add("Engine", "Engine");
            dgv.Columns.Add("Mileage", "Mileage");
            dgv.Columns.Add("Color", "Color");
            dgv.Columns.Add("Condition", "Condition");
            dgv.Columns.Add("Vin", "Vin");
            dgv.Columns.Add("Description", "Description");
        }

        /// <summary>
        /// Method used to generate years range in MetroComboBox.
        /// </summary>
        /// <param name="mcb">MetroComboBox for which will be generated years range.</param>
        public static void GenerateYears(MetroComboBox mcb)
        {
            for (int i = 1980; i < 2018; i++)
            {
                mcb.Items.Add(i);
            }
        }
    }
}
