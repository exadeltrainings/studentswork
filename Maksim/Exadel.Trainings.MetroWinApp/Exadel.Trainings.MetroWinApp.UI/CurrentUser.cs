﻿using Exadel.Trainings.MetroWinApp.DAL.EntityFramework;
using System;
using System.Collections.Generic;

namespace Exadel.Trainings.MetroWinApp.UI
{
    /// <summary>
    /// Represents user, that was login into system. Session analogue.
    /// </summary>
    public static class CurrentUser 
    {
        /// <summary>
        /// User's id.
        /// </summary>
        public static int Id { get; set; }

        /// <summary>
        /// User's uniq name.
        /// </summary>
        public static string Username { get; set; }

        /// <summary>
        /// User's password.
        /// </summary>
        public static string Password { get; set; }

        /// <summary>
        /// User's date of bith.
        /// </summary>
        public static DateTime? Birthdate { get; set; }

        /// <summary>
        /// User's mobile phone.
        /// </summary>
        public static string Phone { get; set; }

        /// <summary>
        /// User's role. It will disappear in future.
        /// </summary>
        public static string Role { get; set; }

        /// <summary>
        /// User's adds.
        /// </summary>
        public static ICollection<Car> Cars { get; set; }

        /// <summary>
        /// Represent user logout.
        /// </summary>
        public static void Logout()
        {
            Id = 0;
            Username = null;
            Password = null;
            Birthdate = null;
            Phone = null;
            Role = null;
            Cars = null;
        }

        /// <summary>
        /// Represent user login.
        /// </summary>
        public static void Login(User u)
        {
            Id = u.Id;
            Password = u.Password;
            Phone = u.Phone;
            Role = u.Role;
            Username = u.Username;
            Birthdate = u.Bithdate;
            Cars = u.Cars;
        }
    }
}
