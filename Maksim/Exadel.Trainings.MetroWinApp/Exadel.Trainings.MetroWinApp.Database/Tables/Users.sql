﻿CREATE TABLE [dbo].[Users]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Username] NVARCHAR(50) NOT NULL, 
    [Password] NVARCHAR(50) NOT NULL, 
    [Bithdate] DATETIME NULL, 
    [Phone] NVARCHAR(50) NULL, 
    [Role] NVARCHAR(50) NOT NULL
)
