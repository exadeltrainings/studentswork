﻿CREATE TABLE [dbo].[Transmissions]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Transmission] NVARCHAR(50) NOT NULL
)
