﻿CREATE TABLE [dbo].[Countries]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Country] NVARCHAR(50) NOT NULL
)
