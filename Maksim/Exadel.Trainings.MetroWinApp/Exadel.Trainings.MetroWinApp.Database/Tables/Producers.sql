﻿CREATE TABLE [dbo].[Producers]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[CountryId] INT NOT NULL, 
    [Producer] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_Producers_ToCountries] FOREIGN KEY ([CountryId]) REFERENCES [Countries]([Id])
)
