﻿CREATE TABLE [dbo].[Cars]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ModelId] INT NOT NULL, 
    [ProducerId] INT NOT NULL, 
    [TransmissionId] INT NOT NULL, 
    [BodyId] INT NOT NULL, 
    [SalerId] INT NOT NULL,
	[Price] FLOAT NOT NULL, 
    [Year] INT NOT NULL, 
    [Engine] NVARCHAR(50) NULL, 
    [Mileage] INT NULL, 
    [Color] NVARCHAR(50) NULL, 
    [Condition] NVARCHAR(50) NOT NULL, 
    [VIN] NVARCHAR(50) NOT NULL, 
    [Description] NVARCHAR(200) NULL, 
    CONSTRAINT [FK_Cars_ToModels] FOREIGN KEY ([ModelId]) REFERENCES [Models]([Id]), 
    CONSTRAINT [FK_Cars_ToTransmissions] FOREIGN KEY ([TransmissionId]) REFERENCES [Transmissions]([Id]), 
    CONSTRAINT [FK_Cars_ToBodies] FOREIGN KEY ([BodyId]) REFERENCES [Bodies]([Id]), 
    CONSTRAINT [FK_Cars_ToProducers] FOREIGN KEY ([ProducerId]) REFERENCES [Producers]([Id]), 
    CONSTRAINT [FK_Cars_ToUsers] FOREIGN KEY ([SalerId]) REFERENCES [Users]([Id])
)
