Task

Implement linked and doubly linked list. A possibility of adding an element to the end of the list, in the beginning and in the middle, the ability to delete any item from a list (first, last or middle) must be realized.
It must be possible to know whether a certain element is contained in the list.
You also need to implement the methods of a list cleaning, sorting and rearranging (reverse).
Implement tests for validation of the application.
It must be possible to use console to work with the application.

Implementation

1. Linked list

Linked list is a set of connected nodes, each of which stores the actual data and a link to the next node.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedList.png) 

Thus, the node class was determined. It represents the actual object in the list.
Node<T> class is generic, can store any type of data. To store a reference to the next node we use Next property for data storage - Data.
3 method were realized for adding elements:
- Add (add the element produced in the bottom of the list);

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListAdd.png) 
 
The complexity of this algorithm is O (1).
- AddFirstElement (add the element in the beginning of the list);

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListAddFirstElement.png) 

- AddAfterElement (add the element after any existing element).

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListAddAfterElement.png) 


Method Remove helps to remove elements from:
- the beginning of the list

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListRemoveFirstElement.png)  

- end

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListRemoveLastElement.png)  

- middle

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListRemoveElementFromCenter.png)  

Remove element algorithm steps:
    1.	Find element in the list (go through all the elements);
    2.	Install Next property of the previous node on the next (from deleted).

The complexity of the algorithm is O(n).

To check is there a certain element in the list was created method Contains.
The complexity of the algorithm is O(n).

To clear the list you can use method Clear.

Rearrange of the list (reverse)
In this method we rearrange elements of the list. The complexity of the algorithm is O(n).


Sorting
In this method we sort list elements using the bubble algorithm. The complexity of the algorithm is O(n).

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/BubbleSorting.png) 


2. Doubly linked list
Doubly linked list also represent a sequence of connected nodes, but now each node stores a link to the next and previous elements.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedList.png) 

Bi-directional of the list has to be taken into account when adding or removing elements, since in addition to the reference to the next element should be installed on the previous link.
But at the same time, we are able to go through the list from the first to the last item, and vice versa - from the last to the first element. The rest of the doubly linked list is no different from the linked list.

NodeDouble<T> class is generic, can store any type of data. To store a reference to the next node we use Next property for data storage - Data, to store a reference to the previous node we use Previous property.

3 method were realized for adding elements:
- Add (add the element produced in the bottom of the list);

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedListAdd.png) 

The complexity of this algorithm is O (1).

- AddFirstElement (add the element in the beginning of the list);

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedListAddFirstElement.png) 

- AddAfterElement (add the element after any existing element).

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedListAddElementAfter.png) 

Remove of elements:
Method Remove helps to remove elements.
When deleting you must first find the deleted item. Then, in the general case, two links have to be reinstalled.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedListRemove.png) 

If you delete first and last elements, you should just reinstall variables firstElement and lastElement.
The complexity of this algorithm is O(n).

To check is there a certain element in the list was created method Contains.
The complexity of the algorithm is O(n).

To clear the list you can use method Clear.

Rearrange of the list (reverse)
In this method we rearrange elements of the list. The complexity of the algorithm is O(n).

Sorting
In this method we sort list elements using the bubble algorithm. The complexity of the algorithm is O(n).

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/BubbleSorting.png) 

Console:
To work with the application from the console class ConsoleHelper was created.
Also in this class, a custom menu for the convenience of the user experience has been realized.

Common interface:
To avoid the problem of code redundancy, the interface ICustomList<T> was implemented to the program. 
This interface defines the methods that are implemented in the classes MySimplyConnectedList and MyDoubleConnectedList. 

Tests:
A number of tests have been developed. They allow to test the correctness of the program.

