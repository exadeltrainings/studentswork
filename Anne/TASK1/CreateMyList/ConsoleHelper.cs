﻿using System;

namespace CreateMyList
{
    public static class ConsoleHelper 
    {
        public static void CreateMyListOutput()
        {
            Console.WriteLine("1 --- Work with simply connected list");
            Console.WriteLine("2 --- Work with double connected list");
            Console.WriteLine("3 --- Exit");

            var menuItem = int.Parse(Console.ReadLine());

            switch (menuItem)
            {
                case 1:
                    Console.WriteLine();
                    MySimplyConnectedList<int> list = new MySimplyConnectedList<int>();
                    list.Add(1);
                    list.Add(2);
                    list.Add(7);
                    list.Add(4);
                    list.Add(9);

                    list.AddAfterElement(2, 8);

                    Console.WriteLine("List elements: ");
                    list.Output();

                    Console.ReadKey();
                    break;
                case 2:
                    Console.WriteLine();
                    MyDoubleConnectedList<int> doubleList = new MyDoubleConnectedList<int>();

                    doubleList.Add(5);
                    doubleList.Add(7);
                    doubleList.Add(9);
                    doubleList.AddFirstElement(3);

                    doubleList.AddAfterElement(5, 10);

                    doubleList.Remove(9);

                    Console.WriteLine("List elements: ");
                    doubleList.Output();

                    Console.ReadKey();
                    break;
                case 3:
                    Console.Clear();
                    break;
                default:
                    Console.WriteLine();
                    Console.WriteLine("No such menu item.");
                    break;
            }
        }
    }
}
