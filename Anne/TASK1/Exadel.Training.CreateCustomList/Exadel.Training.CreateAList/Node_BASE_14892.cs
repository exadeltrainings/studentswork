﻿namespace CreateMyList
{
    /// <summary>
    /// Node class. It represents a single object in the list.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Node<T>
    {
        /// <summary>
        /// For data storage.
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Reference to the next node.
        /// </summary>
        public Node<T> Next { get; set; }

        public Node(T data)
        {
            Data = data;
        }
        
    }
}
