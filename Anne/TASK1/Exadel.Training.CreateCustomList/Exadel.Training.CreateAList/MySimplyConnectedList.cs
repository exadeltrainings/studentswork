﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Exadel.Training.CreateAList
{
    /// <summary>
    /// Simply connected list.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MySimplyConnectedList<T> : ICustomList<T> where T : IComparable
    {
        /// <summary>
        /// Amount of elements in the list.
        /// </summary>
        int count;
        public int Count { get { return count; } }

        Node<T> firstElement;
        Node<T> lastElement;

        /// <summary>
        /// Add an element to the end of the list.
        /// </summary>
        /// <param name="data"></param>
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);

            if (firstElement == null)
            {
                firstElement = node;
            }
            else
            {
                lastElement.Next = node;
            }
            lastElement = node;
            count++;
        }

        /// <summary>
        /// Add element to the begining of the list.
        /// </summary>
        /// <param name="data"></param>
        public void AddFirstElement(T data)
        {
            Node<T> node = new Node<T>(data);
            node.Next = firstElement;
            firstElement = node;
            if (count == 0)
                lastElement = firstElement;
            count++;
        }

        /// <summary>
        /// If you want to add new element not to the end and not to the begining, but after any other element.
        /// </summary>
        /// <param name="aterThisElement"></param>
        /// <param name="data"></param>
        public void AddAfterElement(T aterThisElement, T data)
        {
            Node<T> currentElement = firstElement;
            Node<T> node = new Node<T>(data);

            while (currentElement != null)
            {
                if (currentElement.Data.Equals(aterThisElement))
                {
                    node.Next = currentElement.Next;
                    currentElement.Next = node;
                    count++;
                }
                currentElement = currentElement.Next;
            }
        }

        /// <summary>
        /// Check out if there is a certain element in the list.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Contains(T data)
        {
            Node<T> currentElement = firstElement;
            while (currentElement != null)
            {
                if (currentElement.Data.Equals(data))
                {
                    return true;
                }
                currentElement = currentElement.Next;
            }
            return false;
        }

        /// <summary>
        /// Remove any elemt of the list.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Remove(T data)
        {
            Node<T> prevoiusElement = null;
            Node<T> currentElement = firstElement;

            while (currentElement != null)
            {
                if (currentElement.Data.Equals(data))
                {
                    // If the element, you wanted to remove is first.
                    if (prevoiusElement == null)
                    {
                        firstElement = firstElement.Next;
                    }
                    else
                    {
                        // If the element, you wanted to remove is not first and last.
                        prevoiusElement.Next = currentElement.Next;

                        // If the element, you wanted to remove is last.
                        if (currentElement.Next == null)
                        {
                            lastElement = prevoiusElement;
                        }
                    }
                    count--;
                    return true;
                }
                prevoiusElement = currentElement;
                currentElement = currentElement.Next;
            }
            return false;
        }

        /// <summary>
        /// Indexator.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T this[int key]
        {
            get
            {
                var current = firstElement;
                for (int i = 0; i < key; i++)
                {
                    current = current.Next;
                }
                return current.Data;
            }
            set
            {
                var current = firstElement;
                for (int i = 0; i < key; i++)
                {
                    current = current.Next;
                }
                current.Data = value;
            }
        }

        /// <summary>
        /// Clear all the list.
        /// </summary>
        public void Clear()
        {
            firstElement = null;
            lastElement = null;
            count = 0;
        }

        /// <summary>
        /// Reverse of elements in the list.
        /// </summary>
        public void Reverse()
        {
            if (firstElement == null || firstElement.Next == null) return;
            Node<T> previousElement = null;
            while (firstElement.Next != null)
            {
                Node<T> next = firstElement.Next;
                firstElement.Next = previousElement;
                previousElement = firstElement;
                firstElement = next;
            }
            firstElement.Next = previousElement;
        }

        /// <summary>
        /// Sorting of list elements.
        /// </summary>
        public void Sort()
        {
            bool flag = true;
            while (flag)
            {
                flag = false;
                for (var current = firstElement; current.Next != null; current = current.Next)
                {
                    if (current.CompareTo(current.Next) > 0) continue;
                    var tmp = current.Data;
                    current.Data = current.Next.Data;
                    current.Next.Data = tmp;
                    flag = true;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = firstElement;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }
    }
}
