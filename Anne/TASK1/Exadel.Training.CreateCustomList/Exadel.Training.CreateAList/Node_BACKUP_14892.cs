﻿using System;

namespace Exadel.Training.CreateAList
{
    /// <summary>
    /// Node class. It represents a single object in the list.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Node<T> where T : IComparable
    {
        /// <summary>
        /// For data storage.
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Reference to the next node.
        /// </summary>
        public Node<T> Next { get; set; }

        public Node(T data)
        {
            Data = data;
        }
<<<<<<< HEAD:Anne/Task1/CreateMyList/Node.cs
=======

        public int CompareTo(Node<T> obj)
        {
            if (Data.CompareTo(obj.Data) < 0)
            {
                return 1;
            }
            else if (Data.CompareTo(obj.Data) == 0)
            {
                return 0;
            }
            return -1;
        }
>>>>>>> anne-repo/master:Anne/TASK1/Exadel.Training.CreateCustomList/Exadel.Training.CreateAList/Node.cs
    }
}
