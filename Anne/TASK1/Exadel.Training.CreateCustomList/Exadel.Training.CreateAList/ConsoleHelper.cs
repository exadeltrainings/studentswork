﻿using System;

namespace Exadel.Training.CreateAList
{
    public static class ConsoleHelper
    {
        public static void CustomListOutput()
        {
            int menuItem;
            int secondMenuItem;
            ICustomList<int> list = null;

                Console.WriteLine("1 --- Work with simply connected INTEGER list");
                Console.WriteLine("2 --- Work with double connected INTEGER list");
                Console.WriteLine("3 --- Exit");
                menuItem = int.Parse(Console.ReadLine());

                switch (menuItem)
                {
                    case 1:
                        list = new MySimplyConnectedList<int>();
                        Console.Clear();
                        Console.WriteLine("We gonna work with Simply connected List.");
                        break;
                    case 2:
                        list = new MyDoubleConnectedList<int>();
                        Console.Clear();
                        Console.WriteLine("We gonna work with Double connected List.");
                        break;
                    case 3:
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Wrong menu item.");
                        break;
                }

            do
            {
                Console.WriteLine("1 --- Add element to the end of the existed list");
                Console.WriteLine("2 --- Add element after some element to the existed list");
                Console.WriteLine("3 --- Add first element to the existed list");
                Console.WriteLine("4 --- Remove element from the existed list");
                Console.WriteLine("5 --- Reverse elements");
                Console.WriteLine("6 --- Sort list elements");
                Console.WriteLine("7 --- Show the existed list");
                Console.WriteLine("8 --- Exit");
                secondMenuItem = int.Parse(Console.ReadLine());

                switch (secondMenuItem)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Enter the element you want to add --->");
                        var element = int.Parse(Console.ReadLine());
                        list.Add(element);
                        Console.WriteLine("Element was added.");
                        break;

                    case 2:
                        Console.Clear();
                        Console.WriteLine("Enter the element you want to add --->");
                        var newElement = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter the element you want to add new element after --->");
                        var afterElement = int.Parse(Console.ReadLine());
                        list.AddAfterElement(afterElement, newElement);
                        Console.WriteLine("Element was added.");
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Enter the element you want to add --->");
                        var firstElement = int.Parse(Console.ReadLine());
                        list.AddFirstElement(firstElement);
                        Console.WriteLine("Element was added.");
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Enter the element you want to remove --->");
                        var removeElement = int.Parse(Console.ReadLine());
                        list.Remove(removeElement);
                        Console.WriteLine("Element was removed.");
                        break;
                    case 5:
                        Console.Clear();
                        list.Reverse();
                        Console.WriteLine("Elements reverse was done.");
                        break;
                    case 6:
                        Console.Clear();
                        list.Sort();
                        Console.WriteLine("Sort of elements was done.");
                        break;
                    case 7:
                        Console.Clear();
                        Console.WriteLine("Existed list: ");
                        foreach (var l in list)
                        {
                            Console.WriteLine(l);
                        }
                        break;
                    case 8:
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("You choose smth wrong...");
                        break;
                }
            }
            while (secondMenuItem != 8);
        }
    }
}
