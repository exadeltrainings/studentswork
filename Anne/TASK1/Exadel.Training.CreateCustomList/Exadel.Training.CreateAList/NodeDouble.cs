﻿using System;

namespace Exadel.Training.CreateAList
{
    public class NodeDouble<T> where T : IComparable
    {
        /// <summary>
        /// For data storage.
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Reference to the previous node.
        /// </summary>
        public NodeDouble<T> Previous { get; set; }

        /// <summary>
        /// Reference to the next node.
        /// </summary>
        public NodeDouble<T> Next { get; set; }

        public NodeDouble(T data)
        {
            Data = data;
        }
        
        public int CompareTo(NodeDouble<T> obj)
        {
            if (Data.CompareTo(obj.Data) < 0)
            {
                return 1;
            }
            else if (Data.CompareTo(obj.Data) == 0)
            {
                return 0;
            }
            return -1;
        }
    }
}
