﻿using Exadel.Training.CreateAList;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exadel.Training.Tests
{
    [TestClass]
    public class TestingOfCustomLists
    {
        //Simple connected list TESTS
        [TestMethod]
        public void SimplyConnectedList_Add_2_Objects_ReturnedAmount_2()
        {
            // arrange
            int expextedAmount = 2;
            var list = new MySimplyConnectedList<int>();

            // act
            list.Add(5);
            list.Add(10);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.AreEqual(list[0], 5);
        }

        [TestMethod]
        public void SimplyConnectedList_AddFirstElement_17_ReturnedAmount_3_Contains_17()
        {
            // arrange
            var list = new MySimplyConnectedList<int> { 5, 2 };
            int expectedAmount = 3;

            // act
            list.AddFirstElement(17);

            // assert
            Assert.AreEqual(list[0], 17);
            Assert.AreEqual(list[1], 5);
            Assert.AreEqual(list.Count, expectedAmount);
            Assert.IsTrue(list.Contains(17));
        }

        [TestMethod]
        public void SimplyConnectedList_AddAfterElement_5_Element_13_ReturnedAmount_3_Contains_13()
        {
            // arrange
            int expextedAmount = 3;
            var list = new MySimplyConnectedList<int> { 5, 7 };

            // act
            list.AddAfterElement(5, 13);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.IsTrue(list.Contains(13));
            Assert.AreEqual(list[0], 5);
            Assert.AreEqual(list[1], 13);
        }

        [TestMethod]
        public void SimplyConnectedList_Remove_First_Element_ReturnedAmount_2()
        {
            // arrange
            int expextedAmount = 2;
            var list = new MySimplyConnectedList<int> {5, 10, 13};

            // act
            list.Remove(5);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.IsFalse(list.Contains(5));
            Assert.AreNotEqual(list[0], 5);
        }

        [TestMethod]
        public void SimplyConnectedList_Remove_Center_Element_ReturnedAmount_2()
        {
            // arrange
            int expextedAmount = 2;
            var list = new MySimplyConnectedList<int> { 5, 10, 13 };

            // act
            list.Remove(10);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.AreNotEqual(list[1], 10);
            Assert.IsFalse(list.Contains(10));
        }

        [TestMethod]
        public void SimplyConnectedList_Remove_Last_Element_ReturnedAmount_2()
        {
            // arrange
            int expextedAmount = 2;
            var list = new MySimplyConnectedList<int> { 5, 10, 13 };

            // act
            list.Remove(13);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.IsFalse(list.Contains(13));
        }

        [TestMethod]
        public void SimplyConnectedList_Contains_7_Element_DontContains_10()
        {
            // arrange
            var list = new MySimplyConnectedList<int> { 5, 7, 13 };

            // act

            // assert
            Assert.IsTrue(list.Contains(7));
            Assert.IsFalse(list.Contains(10));
        }

        [TestMethod]
        public void SimplyConnectedList_Clear_List()
        {
            // arrange
            var list = new MySimplyConnectedList<int> { 5, 7, 13 };
            int expextedAmount = 0;
            
            // act

            list.Clear();

            // assert
            Assert.IsFalse(list.Contains(5));
            Assert.IsFalse(list.Contains(7));
            Assert.IsFalse(list.Contains(13));
            Assert.AreEqual(list.Count, expextedAmount);
        }

        [TestMethod]
        public void SimplyConnectedList_Reverse()
        {
            // arrange
            var list = new MySimplyConnectedList<int> { 5, 7, 13 };

            // act

            list.Reverse();

            // assert
            Assert.AreEqual(list[0], 13);
            Assert.AreNotEqual(list[0], 5);
            Assert.AreEqual(list[1], 7);
            Assert.AreEqual(list[2], 5);
            Assert.AreNotEqual(list[2], 13);
        }

        [TestMethod]
        public void SimplyConnectedList_Sort()
        {
            // arrange
            var list = new MySimplyConnectedList<int> { 13, 5, 7 };

            // act

            list.Sort();

            // assert
            Assert.AreEqual(list[0], 5);
            Assert.AreNotEqual(list[0], 13);
            Assert.AreEqual(list[1], 7);
            Assert.AreNotEqual(list[1], 5);
            Assert.AreEqual(list[2], 13);
            Assert.AreNotEqual(list[2], 7);
        }

        //Double connected list TESTS
        [TestMethod]
        public void DoubleConnectedList_Add_2_Objects_ReturnedAmount_2()
        {
            // arrange
            int expextedAmount = 2;
            var list = new MyDoubleConnectedList<int>();

            // act
            list.Add(5);
            list.Add(10);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.AreEqual(list[0], 5);
        }

        [TestMethod]
        public void DoubleConnectedList_AddFirstElement_17_ReturnedAmount_3_Contains_17()
        {
            // arrange
            var list = new MyDoubleConnectedList<int> { 5, 2 }; 
            int expectedAmount = 3;

            // act
            list.AddFirstElement(17);

            // assert
            Assert.AreEqual(list[0], 17);
            Assert.AreEqual(list[1], 5);
            Assert.AreEqual(list.Count, expectedAmount);
            Assert.IsTrue(list.Contains(17));
        }

        [TestMethod]
        public void DoubleConnectedList_AddAfterElement_5_Element_13_ReturnedAmount_3_Contains_13()
        {
            // arrange
            int expextedAmount = 3;
            var list = new MyDoubleConnectedList<int> { 5, 7 };

            // act
            list.AddAfterElement(5, 13);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.IsTrue(list.Contains(13));
            Assert.AreEqual(list[0], 5);
            Assert.AreEqual(list[1], 13);
        }

        [TestMethod]
        public void DoubleConnectedList_Remove_First_Element_ReturnedAmount_2()
        {
            // arrange
            int expextedAmount = 2;
            var list = new MyDoubleConnectedList<int> { 5, 10, 13 };

            // act
            list.Remove(5);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.IsFalse(list.Contains(5));
            Assert.AreNotEqual(list[0], 5);
        }

        [TestMethod]
        public void DoubleConnectedList_Remove_Center_Element_ReturnedAmount_2()
        {
            // arrange
            int expextedAmount = 2;
            var list = new MyDoubleConnectedList<int> { 5, 10, 13 };

            // act
            list.Remove(10);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.AreNotEqual(list[1], 10);
            Assert.IsFalse(list.Contains(10));
        }

        [TestMethod]
        public void DoubleConnectedList_Remove_Last_Element_ReturnedAmount_2()
        {
            // arrange
            int expextedAmount = 2;
            var list = new MyDoubleConnectedList<int> { 5, 10, 13 };

            // act
            list.Remove(13);

            // assert
            Assert.AreEqual(list.Count, expextedAmount);
            Assert.IsFalse(list.Contains(13));
        }

        [TestMethod]
        public void DoubleConnectedList_Contains_7_Element_DontContains_10()
        {
            // arrange
            var list = new MyDoubleConnectedList<int> { 5, 7, 13 };

            // act

            // assert
            Assert.IsTrue(list.Contains(7));
            Assert.IsFalse(list.Contains(10));
        }

        [TestMethod]
        public void DoubleConnectedList_Clear_List()
        {
            // arrange
            var list = new MyDoubleConnectedList<int> { 5, 7, 13 };
            int expextedAmount = 0;

            // act

            list.Clear();

            // assert
            Assert.IsFalse(list.Contains(5));
            Assert.IsFalse(list.Contains(7));
            Assert.IsFalse(list.Contains(13));
            Assert.AreEqual(list.Count, expextedAmount);
        }

        [TestMethod]
        public void DoubleConnectedList_Reverse()
        {
            // arrange
            var list = new MyDoubleConnectedList<int> { 5, 7, 13 };

            // act

            list.Reverse();

            // assert
            Assert.AreEqual(list[0], 13);
            Assert.AreNotEqual(list[0], 5);
            Assert.AreEqual(list[1], 7);
            Assert.AreEqual(list[2], 5);
            Assert.AreNotEqual(list[2], 13);
        }

        [TestMethod]
        public void DoubleConnectedList_Sort()
        {
            // arrange
            var list = new MyDoubleConnectedList<int> { 13, 5, 7 };

            // act

            list.Sort();

            // assert
            Assert.AreEqual(list[0], 5);
            Assert.AreNotEqual(list[0], 13);
            Assert.AreEqual(list[1], 7);
            Assert.AreNotEqual(list[1], 5);
            Assert.AreEqual(list[2], 13);
            Assert.AreNotEqual(list[2], 7);
        }
    }
}
