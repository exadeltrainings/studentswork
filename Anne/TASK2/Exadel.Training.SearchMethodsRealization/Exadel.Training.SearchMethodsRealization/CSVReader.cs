﻿using System.Collections.Generic;
using System.IO;

namespace Exadel.Training.SearchMethodsRealization
{
    public class CSVReader
    {
        public string Phone { get; set; }
        public string Name { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            Phone = parts[0];
            Name = parts[1];
        }

        /// <summary>
        /// Read data from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<CSVReader> ReadFile(string filename)
        {
            List<CSVReader> res = new List<CSVReader>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    CSVReader p = new CSVReader();
                    p.piece(line);
                    res.Add(p);
                }
            }
            return res;
        }
    }
}
