﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exadel.Training.SearchMethodsRealization
{
    public class HashTable<K, V>
    {
        public LinkedList<Tuple<K, V>>[] items;
        public int fillFactor = 3;
        public int size;
        public int count;
        public double Percent = 0.0;

        public HashTable()
        {
            size = 20;
            items = new LinkedList<Tuple<K, V>>[size];
        }

        /// <summary>
        /// Add new item to hash table.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(K key, V value)
        {
            var pos = GetPosition(key, items.Length);

            count++;

            if (CheckSize())
            {
                ChangeSize();
            }

            if (items[pos] == null)
            {
                items[pos] = new LinkedList<Tuple<K, V>>();
            }

            items[pos].AddFirst(new Tuple<K, V>(key, value));
        }

        /// <summary>
        /// Remove item from hash table.
        /// </summary>
        /// <param name="key"></param>
        public void Remove(K key)
        {
            var pos = GetPosition(key, items.Length);
            if (items[pos] != null)
            {
                var objToRemove = items[pos].FirstOrDefault(item => item.Item1.Equals(key));
                if (objToRemove == null) return;
                items[pos].Remove(objToRemove);
                count--;
            }
            else
            {
                throw new Exception("Value not in HashTable.");
            }
        }

        /// <summary>
        /// Find item in hash table.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public V Get(K key)
        {
            var pos = GetPosition(key, items.Length);
            foreach (var item in items[pos].Where(item => item.Item1.Equals(key)))
            {
                return item.Item2;
            }
            throw new Exception("Key does not exist in HashTable.");
        }

        /// <summary>
        /// Change size of the hash table.
        /// </summary>
        private void ChangeSize()
        {
            size = size * 2;
            var newItems = new LinkedList<Tuple<K, V>>[size];
            foreach (var item in items.Where(x => x != null))
            {
                foreach (var value in item)
                {
                    var pos = GetPosition(value.Item1, newItems.Length);
                    if (newItems[pos] == null)
                    {
                        newItems[pos] = new LinkedList<Tuple<K, V>>();
                    }
                    newItems[pos].AddFirst(new Tuple<K, V>(value.Item1, value.Item2));
                }
            }
            items = newItems;
        }

        /// <summary>
        /// Get/create position for the item.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private int GetPosition(K key, int length)
        {
            var hash = key.GetHashCode();
            var pos = Math.Abs(hash % length);
            return pos;
        }

        /// <summary>
        /// Check if size is normal.
        /// </summary>
        /// <returns></returns>
        public bool CheckSize()
        {
            Percent = (double)count / (double)size;
            if (Percent >= 0.75)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
