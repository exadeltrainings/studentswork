﻿using System;
using System.Collections.Generic;

namespace Exadel.Training.SearchMethodsRealization
{
    public class BinarySearchTree<Tkey, Tvalue> where Tkey : IComparable<Tkey>
    {
        public BinaryKeyValueNode<Tkey, Tvalue> Root { get; set; }
        public int Count { get; protected set; }

        public BinarySearchTree()
        {
            Root = null;
            Count = 0;
        }

        /// <summary>
        /// Insert element to the tree.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Insert(Tkey key, Tvalue value)
        {
            BinaryKeyValueNode<Tkey, Tvalue> parent = null;
            BinaryKeyValueNode<Tkey, Tvalue> current = Root;
            int compare = 0;
            while (current != null)
            {
                parent = current;
                compare = current.Key.CompareTo(key);
                current = compare < 0 ? current.RightChild : current.LeftChild;
            }
            BinaryKeyValueNode<Tkey, Tvalue> newNode = new BinaryKeyValueNode<Tkey, Tvalue>(key, value);
            if (parent != null)
                if (compare < 0)
                    parent.RightChild = newNode;
                else
                    parent.LeftChild = newNode;
            else
                Root = newNode;
            newNode.Parent = parent;
            Count++;
        }

        /// <summary>
        /// Find element by key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Tvalue Find(Tkey key)
        {
            return FindNode(key).Value;
        }

        /// <summary>
        /// Find the node. Used to find element by key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="ExceptionIfKeyNotFound"></param>
        /// <returns></returns>
        public BinaryKeyValueNode<Tkey, Tvalue> FindNode(Tkey key, bool ExceptionIfKeyNotFound = true)
        {
            BinaryKeyValueNode<Tkey, Tvalue> current = Root;
            while (current != null)
            {
                int compare = current.Key.CompareTo(key);
                if (compare == 0)
                    return current;
                if (compare < 0)
                    current = current.RightChild;
                else
                    current = current.LeftChild;
            }
            if (ExceptionIfKeyNotFound)
                throw new KeyNotFoundException();
            else
                return null;
        }

    }
}
