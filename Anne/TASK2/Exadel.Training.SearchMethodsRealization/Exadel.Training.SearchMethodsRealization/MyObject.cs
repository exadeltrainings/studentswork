﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exadel.Training.SearchMethodsRealization
{
    public class MyObject : IComparable<MyObject>
    {
        public string Number { get; set; }
        public string Name { get; set; }

        public MyObject()
        {
        }

        public MyObject(string number, string name)
        {
            Number = number;
            Name = name;
        }

        public int CompareTo(MyObject obj)
        {
            if (Number.GetHashCode() > obj.Number.GetHashCode())
            {
                return 1;
            }
            else if (Number.GetHashCode() < obj.Number.GetHashCode())
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
