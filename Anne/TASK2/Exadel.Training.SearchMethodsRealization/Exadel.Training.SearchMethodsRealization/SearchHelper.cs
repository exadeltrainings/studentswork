﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exadel.Training.SearchMethodsRealization
{
    public static class SearchHelper
    {
        /// <summary>
        /// Get full hash table.
        /// </summary>
        /// <returns></returns>
        public static HashTable<string, string> GetHashTable(string fileName)
        {
            HashTable<string, string> ht = new HashTable<string, string>();

            List<CSVReader> infoList = new List<CSVReader>();
            infoList = CSVReader.ReadFile(fileName);

            for (int i = 0; i < infoList.Count; i++)
            {
                ht.Add(infoList[i].Phone, infoList[i].Name);
            }

            return ht;
        }

        /// <summary>
        /// Get full binary search tree.
        /// </summary>
        /// <returns></returns>
        /*public static BinarySearchTree<string, string> GetBinaryTree(string fileName)
        {
            BinarySearchTree<string, string> bst = new BinarySearchTree<string, string>();

            List<CSVReader> infoList = new List<CSVReader>();
            infoList = CSVReader.ReadFile(fileName);

            for (int i = 0; i < infoList.Count; i++)
            {
                bst.Insert(infoList[i].Phone, infoList[i].Name);
            }

            return bst;
        }*/

        public static BTree GetBinaryTree(string fileName)
        {
            BTree bst = new BTree();

            List<CSVReader> infoList = new List<CSVReader>();
            infoList = CSVReader.ReadFile(fileName);

            for (int i = 0; i < infoList.Count; i++)
            {
                bst.Add(new MyObject(infoList[i].Phone, infoList[i].Name));
            }

            return bst;
        }
    }
}
