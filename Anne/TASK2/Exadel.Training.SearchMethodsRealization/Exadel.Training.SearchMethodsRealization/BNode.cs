﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exadel.Training.SearchMethodsRealization
{
    public class BNode
    {
        public MyObject item;
        public BNode right;
        public BNode left;
        public BNode parent;

        public BNode(MyObject item)
        {
            this.item = item;
        }
    }
}
