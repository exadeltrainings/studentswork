﻿namespace Exadel.Training.SearchMethodsRealization.Forms
{
    partial class FileСontents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileСontents));
            this.BackToMainForm = new System.Windows.Forms.Button();
            this.FillTableButton = new System.Windows.Forms.Button();
            this.SimpleSearch = new System.Windows.Forms.Button();
            this.SearchText = new System.Windows.Forms.TextBox();
            this.ClearPriceList = new System.Windows.Forms.Button();
            this.MainList = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.MainList)).BeginInit();
            this.SuspendLayout();
            // 
            // BackToMainForm
            // 
            this.BackToMainForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BackToMainForm.Font = new System.Drawing.Font("Script MT Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackToMainForm.Location = new System.Drawing.Point(12, 391);
            this.BackToMainForm.Name = "BackToMainForm";
            this.BackToMainForm.Size = new System.Drawing.Size(87, 25);
            this.BackToMainForm.TabIndex = 1;
            this.BackToMainForm.Text = "Back";
            this.BackToMainForm.UseVisualStyleBackColor = true;
            this.BackToMainForm.Click += new System.EventHandler(this.BackToMainForm_Click);
            // 
            // FillTableButton
            // 
            this.FillTableButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FillTableButton.Font = new System.Drawing.Font("Script MT Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FillTableButton.Location = new System.Drawing.Point(632, 391);
            this.FillTableButton.Name = "FillTableButton";
            this.FillTableButton.Size = new System.Drawing.Size(87, 25);
            this.FillTableButton.TabIndex = 3;
            this.FillTableButton.Text = "Show";
            this.FillTableButton.UseVisualStyleBackColor = true;
            this.FillTableButton.Click += new System.EventHandler(this.FillTableButton_Click);
            // 
            // SimpleSearch
            // 
            this.SimpleSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SimpleSearch.Font = new System.Drawing.Font("Script MT Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SimpleSearch.Location = new System.Drawing.Point(323, 391);
            this.SimpleSearch.Name = "SimpleSearch";
            this.SimpleSearch.Size = new System.Drawing.Size(129, 25);
            this.SimpleSearch.TabIndex = 5;
            this.SimpleSearch.Text = "Simple search";
            this.SimpleSearch.UseVisualStyleBackColor = true;
            this.SimpleSearch.Visible = false;
            this.SimpleSearch.Click += new System.EventHandler(this.button1_Click);
            // 
            // SearchText
            // 
            this.SearchText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchText.Location = new System.Drawing.Point(199, 391);
            this.SearchText.Name = "SearchText";
            this.SearchText.Size = new System.Drawing.Size(116, 22);
            this.SearchText.TabIndex = 6;
            this.SearchText.Visible = false;
            // 
            // ClearPriceList
            // 
            this.ClearPriceList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearPriceList.Font = new System.Drawing.Font("Script MT Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearPriceList.Location = new System.Drawing.Point(727, 391);
            this.ClearPriceList.Name = "ClearPriceList";
            this.ClearPriceList.Size = new System.Drawing.Size(87, 25);
            this.ClearPriceList.TabIndex = 7;
            this.ClearPriceList.Text = "Clear";
            this.ClearPriceList.UseVisualStyleBackColor = true;
            this.ClearPriceList.Click += new System.EventHandler(this.ClearPriceList_Click);
            // 
            // MainList
            // 
            this.MainList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MainList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Name});
            this.MainList.Location = new System.Drawing.Point(12, 12);
            this.MainList.Name = "MainList";
            this.MainList.Size = new System.Drawing.Size(800, 362);
            this.MainList.TabIndex = 8;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            // 
            // Name
            // 
            this.Name.HeaderText = "Name";
            this.Name.Name = "Name";
            this.Name.Width = 900;
            // 
            // FileСontents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 429);
            this.Controls.Add(this.MainList);
            this.Controls.Add(this.ClearPriceList);
            this.Controls.Add(this.SearchText);
            this.Controls.Add(this.SimpleSearch);
            this.Controls.Add(this.FillTableButton);
            this.Controls.Add(this.BackToMainForm);
            this.Font = new System.Drawing.Font("Script MT Bold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Text = "FileСontents";
            ((System.ComponentModel.ISupportInitialize)(this.MainList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BackToMainForm;
        private System.Windows.Forms.Button FillTableButton;
        private System.Windows.Forms.Button SimpleSearch;
        private System.Windows.Forms.TextBox SearchText;
        private System.Windows.Forms.Button ClearPriceList;
        private System.Windows.Forms.DataGridView MainList;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
    }
}