﻿namespace Exadel.Training.SearchMethodsRealization.Forms
{
    partial class SearchUsingHashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchUsingHashForm));
            this.Main = new System.Windows.Forms.Label();
            this.PhoneEnter = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.FindButton = new System.Windows.Forms.Button();
            this.NameResult = new System.Windows.Forms.TextBox();
            this.Name = new System.Windows.Forms.Label();
            this.SpecialInfoLabel = new System.Windows.Forms.Label();
            this.SpecialInfoButton = new System.Windows.Forms.Button();
            this.SpecialInfoListBox = new System.Windows.Forms.ListBox();
            this.BackButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Main
            // 
            this.Main.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Main.AutoSize = true;
            this.Main.BackColor = System.Drawing.Color.Transparent;
            this.Main.Font = new System.Drawing.Font("Script MT Bold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Main.Location = new System.Drawing.Point(118, 9);
            this.Main.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Main.Name = "Main";
            this.Main.Size = new System.Drawing.Size(408, 23);
            this.Main.TabIndex = 0;
            this.Main.Text = "Here you can try to find person by his phone number";
            // 
            // PhoneEnter
            // 
            this.PhoneEnter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.PhoneEnter.Location = new System.Drawing.Point(18, 81);
            this.PhoneEnter.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PhoneEnter.Name = "PhoneEnter";
            this.PhoneEnter.Size = new System.Drawing.Size(188, 27);
            this.PhoneEnter.TabIndex = 1;
            // 
            // Phone
            // 
            this.Phone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Phone.AutoSize = true;
            this.Phone.BackColor = System.Drawing.Color.Transparent;
            this.Phone.Location = new System.Drawing.Point(14, 58);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(139, 19);
            this.Phone.TabIndex = 2;
            this.Phone.Text = "Enter the phone here:";
            // 
            // FindButton
            // 
            this.FindButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FindButton.Location = new System.Drawing.Point(18, 125);
            this.FindButton.Name = "FindButton";
            this.FindButton.Size = new System.Drawing.Size(88, 29);
            this.FindButton.TabIndex = 3;
            this.FindButton.Text = "Find";
            this.FindButton.UseVisualStyleBackColor = true;
            this.FindButton.Click += new System.EventHandler(this.FindButton_Click);
            // 
            // NameResult
            // 
            this.NameResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameResult.Location = new System.Drawing.Point(399, 81);
            this.NameResult.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NameResult.Name = "NameResult";
            this.NameResult.Size = new System.Drawing.Size(188, 27);
            this.NameResult.TabIndex = 4;
            // 
            // Name
            // 
            this.Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Name.AutoSize = true;
            this.Name.BackColor = System.Drawing.Color.Transparent;
            this.Name.Location = new System.Drawing.Point(395, 58);
            this.Name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Name.Name = "Name";
            this.Name.Size = new System.Drawing.Size(259, 19);
            this.Name.TabIndex = 5;
            this.Name.Text = "Name of the person with entered phone:";
            // 
            // SpecialInfoLabel
            // 
            this.SpecialInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SpecialInfoLabel.AutoSize = true;
            this.SpecialInfoLabel.BackColor = System.Drawing.Color.Transparent;
            this.SpecialInfoLabel.Location = new System.Drawing.Point(395, 115);
            this.SpecialInfoLabel.Name = "SpecialInfoLabel";
            this.SpecialInfoLabel.Size = new System.Drawing.Size(158, 19);
            this.SpecialInfoLabel.TabIndex = 6;
            this.SpecialInfoLabel.Text = "And some special Info:";
            this.SpecialInfoLabel.Visible = false;
            // 
            // SpecialInfoButton
            // 
            this.SpecialInfoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SpecialInfoButton.Enabled = false;
            this.SpecialInfoButton.Location = new System.Drawing.Point(18, 160);
            this.SpecialInfoButton.Name = "SpecialInfoButton";
            this.SpecialInfoButton.Size = new System.Drawing.Size(88, 28);
            this.SpecialInfoButton.TabIndex = 8;
            this.SpecialInfoButton.Text = "Special";
            this.SpecialInfoButton.UseVisualStyleBackColor = true;
            this.SpecialInfoButton.Click += new System.EventHandler(this.SpecialInfoButton_Click);
            // 
            // SpecialInfoListBox
            // 
            this.SpecialInfoListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SpecialInfoListBox.FormattingEnabled = true;
            this.SpecialInfoListBox.ItemHeight = 19;
            this.SpecialInfoListBox.Location = new System.Drawing.Point(399, 138);
            this.SpecialInfoListBox.Name = "SpecialInfoListBox";
            this.SpecialInfoListBox.Size = new System.Drawing.Size(188, 80);
            this.SpecialInfoListBox.TabIndex = 9;
            this.SpecialInfoListBox.Visible = false;
            // 
            // BackButton
            // 
            this.BackButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BackButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BackButton.Location = new System.Drawing.Point(18, 194);
            this.BackButton.Name = "BackButton";
            this.BackButton.Size = new System.Drawing.Size(88, 28);
            this.BackButton.TabIndex = 10;
            this.BackButton.Text = "Back";
            this.BackButton.UseVisualStyleBackColor = true;
            this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // SearchUsingHashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(659, 231);
            this.Controls.Add(this.BackButton);
            this.Controls.Add(this.SpecialInfoListBox);
            this.Controls.Add(this.SpecialInfoButton);
            this.Controls.Add(this.SpecialInfoLabel);
            this.Controls.Add(this.Name);
            this.Controls.Add(this.NameResult);
            this.Controls.Add(this.FindButton);
            this.Controls.Add(this.Phone);
            this.Controls.Add(this.PhoneEnter);
            this.Controls.Add(this.Main);
            this.Font = new System.Drawing.Font("Script MT Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Text = "SearchUsingHashForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Main;
        private System.Windows.Forms.TextBox PhoneEnter;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.Button FindButton;
        private System.Windows.Forms.TextBox NameResult;
        private System.Windows.Forms.Label Name;
        private System.Windows.Forms.Label SpecialInfoLabel;
        private System.Windows.Forms.Button SpecialInfoButton;
        private System.Windows.Forms.ListBox SpecialInfoListBox;
        private System.Windows.Forms.Button BackButton;
    }
}