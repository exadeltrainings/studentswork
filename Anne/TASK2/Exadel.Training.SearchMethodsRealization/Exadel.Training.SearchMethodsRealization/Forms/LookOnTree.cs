﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exadel.Training.SearchMethodsRealization.Forms
{
    public partial class LookOnTree : Form
    {
        public LookOnTree()
        {
            InitializeComponent();
        }

        public void displayTree(BNode root, TreeNode main, bool flag)
        {
            //bool flag = false;

            BNode temp;
            temp = root;

            if (temp == null) return;

            TreeNode newNode = new TreeNode(temp.item.Name);

            displayTree(temp.left, newNode, true);

            if (newNode != null)
            {
                if (!flag)
                {
                    Tree.Nodes.Add(newNode);
                }
                else
                {
                    main.Nodes.Add(newNode);
                }

            }

            displayTree(temp.right, newNode, true);
        }

        public void SearchNode(TreeNode startNode, string key)
        {
            while (startNode != null)
            {
                if (startNode.Text == key)
                {
                    startNode.BackColor = Color.Yellow;
                }
                else
                {
                    startNode.BackColor = Color.Transparent;
                }

                if (startNode.Nodes.Count != 0)
                {
                    SearchNode(startNode.Nodes[0], key);
                }
                startNode = startNode.NextNode;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string fileName = @"..\..\DataFiles\inner.csv";
            BTree bst = SearchHelper.GetBinaryTree(fileName);

            TreeNode main = new TreeNode(bst.root.item.Name);
            
            displayTree(bst.root, main, false);
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string fileName = @"..\..\DataFiles\inner.csv";
            BTree bst = SearchHelper.GetBinaryTree(fileName);
            Stopwatch testStopwatch = new Stopwatch();
            string key = textBox1.Text;
            string toFind = bst.Find(key);

            TreeNode startNode = Tree.Nodes[0];
            Tree.ExpandAll();
            testStopwatch.Start();
            SearchNode(startNode, toFind);
            testStopwatch.Stop();

            listBox1.Items.Add("Speed: " + testStopwatch.ElapsedMilliseconds.ToString() + "mls\n");
            listBox1.Items.Add("");
            listBox1.Items.Add("Personal info:");
            listBox1.Items.Add("Number  --  " + key);
            listBox1.Items.Add("Name     --  " + toFind);
            listBox1.Items.Add("");
        }
    }
}
