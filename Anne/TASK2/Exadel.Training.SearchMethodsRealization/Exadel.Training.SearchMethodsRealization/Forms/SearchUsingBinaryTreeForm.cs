﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Exadel.Training.SearchMethodsRealization.Forms
{
    public partial class SearchUsingBinaryTreeForm : Form
    {
        private OpenFileDialog treeOpenDialog = new OpenFileDialog();
        private DialogResult dialogResult;

        public SearchUsingBinaryTreeForm()
        {
            InitializeComponent();

            treeOpenDialog.Filter = "CSV files(*.csv)|*.csv|All files(*.*)|*.*";
        }

        private void FindButton_Click(object sender, EventArgs e)
        {
            string fileName = treeOpenDialog.FileName;

            BTree bst = SearchHelper.GetBinaryTree(fileName);
            string key = SearchTextBox.Text;

            Stopwatch testStopwatch = new Stopwatch();

            try
            {
                testStopwatch.Start();
                ResultTextBox.Text = bst.Find(key);
                testStopwatch.Stop();

                SearchSpeedTextBox.Visible = true;
                SearchLabel.Visible = true;
            }
            catch
            {
                ResultTextBox.Text = "Impossible...";
            }

            SearchSpeedTextBox.Text = testStopwatch.ElapsedMilliseconds.ToString() + "mls";
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dialogResult = treeOpenDialog.ShowDialog();
            FindButton.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LookOnTree toShow = new LookOnTree();
            toShow.Show();
        }
    }
}
