﻿using System;
using System.Windows.Forms;

namespace Exadel.Training.SearchMethodsRealization.Forms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close MainForm.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Exit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Showl FileContents Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void downloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileСontents toShow = new FileСontents();    
            toShow.Show();
        }

        /// <summary>
        /// Show SearchUsingHash Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchUsingHashToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchUsingHashForm toShow = new SearchUsingHashForm();
            toShow.Show();

            
        }

        /// <summary>
        /// Show HashTableInfo Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void hashTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HashTableInfoForm toShow = new HashTableInfoForm();
            toShow.Show();
        }

        private void binarySearchTreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BinarySearchTreeInfoForm toShow = new BinarySearchTreeInfoForm();
            toShow.Show(); 
        }

        private void bynarySearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchUsingBinaryTreeForm toShow = new SearchUsingBinaryTreeForm();
            toShow.Show();
        }
    }
}
