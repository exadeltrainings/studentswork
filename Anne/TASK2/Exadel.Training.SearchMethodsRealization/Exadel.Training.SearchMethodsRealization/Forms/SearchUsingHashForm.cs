﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Exadel.Training.SearchMethodsRealization.Forms
{
    public partial class SearchUsingHashForm : Form
    {
        public SearchUsingHashForm()
        {
            InitializeComponent();
        }

        private void FindButton_Click(object sender, EventArgs e)
        {
            HashTable<string, string> ht = SearchUsingHash.GetHashTable();
            string key = PhoneEnter.Text;

            Stopwatch testStopwatch = new Stopwatch();

            try
            {  
                testStopwatch.Start();
                NameResult.Text = ht.Get(key);
                testStopwatch.Stop();

                SpecialInfoButton.Enabled = true;
            }
            catch
            {
                NameResult.Text = "Impossible...";
            }
            SpecialInfoListBox.Items.Add("Search time:    " + testStopwatch.ElapsedMilliseconds + "mls");
        }

        private void SpecialInfoButton_Click(object sender, EventArgs e)
        {
            HashTable<string, string> ht = SearchUsingHash.GetHashTable();
            SpecialInfoLabel.Visible = true;
            SpecialInfoListBox.Visible = true;
            SpecialInfoListBox.Items.Add("Actual size:    " + ht.size);
            SpecialInfoListBox.Items.Add("Occupancy:      " + (ht.Percent * 100) + "%");   
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
