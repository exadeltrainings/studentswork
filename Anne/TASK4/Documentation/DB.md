TASK

Create a database with a set of tables associated with each other on the basis of subject area, as well as create a Windows application for organiza- user interface to work with the newly created database. The application must allow to carry out basic operations within the domain. Consequently, also need to provide a detailed description of all aspects of the problem, more precisely, what data is and what you can do with them, in particular, specify the user roles - making the user in its respective role. Thus, it is necessary to create a database, and an application to organize application work with the database - that is, at least 3 sub-tasks that require time and resources. It is worth remembering about the practical design - if properly design the base layer and work with the data, they can be used later in other types of applications with minimal changes. Therefore, even as the task - is to create a layer of working with data with the future and not rigidly tied to Kaki or components of the system, but to the data access technology.

REALIZATION

Application consists of 6 forms. Some of them are used to are used to display data from DB tables.

Working with ADO.NET


ADO.NET is an object-oriented set of libraries that allows you to interact with data sources. Commonly, the data source is a database, but it could also be a text file, an Excel spreadsheet, or an XML file. For the purposes of this tutorial, we will look at ADO.NET as a way to interact with a data base.

As you are probably aware, there are many different types of databases available. For example, there is Microsoft SQL Server, Microsoft Access, Oracle, Borland Interbase, and IBM DB2, just to name a few. To further refine the scope of this tutorial, all of the examples will use SQL Server.




-  C# ADO.NET Connection

The Connection Object is a part of ADO.NET Data Provider and it is a unique session with the Data Source. The Connection Object is Handling the part of physical communication between the C# application and the Data Source.

The Connection Object connect to the specified Data Source and open a connection between the C# application and the Data Source, depends on the parameter specified in the Connection String . When the connection is established, SQL Commands will execute with the help of the Connection Object and retrieve or manipulate data in the Data Source.

Once the Database activity is over , Connection should be closed and release the Data Source resources .

![alt tag](http://csharp.net-informations.com/data-providers/img/csharp-connection.JPG)



-  C# ADO.NET Command

The Command Object in ADO.NET executes SQL statements and Stored Procedures against the data source specified in the C# Connection Object. The Command Object requires an instance of a C# Connection Object for executing the SQL statements .

In order to retrieve a resultset or execute an SQL statement against a Data Source , first you have to create a Connection Object and open a connection to the Data Source specified in the connection string. Next step is to assign the open connection to the connection property of the Command Object . Then the Command Object can execute the SQL statements. After the execution of the SQl statement, the Command Object will return a result set . We can retrieve the result set using a Data Reader .

![alt tag](http://csharp.net-informations.com/data-providers/img/csharp-command.JPG)



-  C# ADO.NET DataReader

DataReader Object in ADO.NET is a stream-based , forward-only, read-only retrieval of query results from the Data Sources , which do not update the data. The DataReader cannot be created directly from code, they can created only by calling the ExecuteReader method of a Command Object.

![alt tag](http://csharp.net-informations.com/data-providers/img/csharp-datareader.JPG)



-  C# ADO.NET DataAdapter

DataAdapter is a part of the ADO.NET Data Provider. DataAdapter provides the communication between the Dataset and the Datasource. We can use the DataAdapter in combination with the DataSet Object. DataAdapter provides this combination by mapping Fill method, which changes the data in the DataSet to match the data in the data source, and Update, which changes the data in the data source to match the data in the DataSet. That is, these two objects combine to enable both data access and data manipulation capabilities.

The DataAdapter can perform Select , Insert , Update and Delete SQL operations in the Data Source. The Insert , Update and Delete SQL operations , we are using the continuation of the Select command perform by the DataAdapter. The SelectCommand property of the DataAdapter is a Command Object that retrieves data from the data source. The InsertCommand , UpdateCommand , and DeleteCommand properties of the DataAdapter are Command objects that manage updates to the data in the data source according to modifications made to the data in the DataSet. From the following links describes how to use SqlDataAdapter and OleDbDataAdapter in detail.

![alt tag](http://csharp.net-informations.com/data-providers/img/csharp-dataadapter.JPG)


ADO.NET is the .NET technology for interacting with data sources. You have several Data Providers, which allow communication with different data sources, depending on the protocols they use or what the database is. Regardless, of which Data Provider used, you’ll use a similar set of objects to interact with a data source. The SqlConnection object lets you manage a connection to a data source. SqlCommand objects allow you to talk to a data source and send commands to it. To have fast forward-only read access to data, use the SqlDataReader. If you want to work with disconnected data, use a DataSet and implement reading and writing to/from the data source with a SqlDataAdapter.