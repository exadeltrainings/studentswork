﻿CREATE TABLE [dbo].[Hotel]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Rating] INT NOT NULL, 
    [TypeOfDiet] NVARCHAR(50) NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [ResortId] INT NOT NULL, 
    CONSTRAINT [FK_Hotel_Resort] FOREIGN KEY ([ResortId]) REFERENCES [dbo].[Resort]([Id])
)
