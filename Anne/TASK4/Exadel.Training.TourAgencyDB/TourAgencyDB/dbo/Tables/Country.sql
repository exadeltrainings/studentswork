﻿CREATE TABLE [dbo].[Country]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [VisaCost] MONEY NULL, 
    [Currency] NVARCHAR(50) NULL, 
    [Language] NCHAR(10) NOT NULL

)
