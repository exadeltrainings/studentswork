﻿CREATE TABLE [dbo].[Transport]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Class] NVARCHAR(50) NULL
)
