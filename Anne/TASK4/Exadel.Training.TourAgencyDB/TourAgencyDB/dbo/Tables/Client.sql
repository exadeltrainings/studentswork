﻿CREATE TABLE [dbo].[Client]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [PassportNumber] NVARCHAR(50) NOT NULL, 
    [Address] NVARCHAR(50) NULL, 
    [PhoneNumber] NVARCHAR(50) NOT NULL, 
    [Password] NVARCHAR(50) NOT NULL
)
