﻿CREATE TABLE [dbo].[Order]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Insurance] BIT NOT NULL, 
    [RegistrationDate] DATE NOT NULL, 
    [ClientId] INT NOT NULL, 
    [TourId] INT NOT NULL, 
    [EmployeeId] INT NOT NULL, 
    CONSTRAINT [FK_Order_Client] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Client]([Id]), 
    CONSTRAINT [FK_Order_Tour] FOREIGN KEY ([TourId]) REFERENCES [dbo].[Tour]([Id]), 
    CONSTRAINT [FK_Order_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee]([Id])
)
