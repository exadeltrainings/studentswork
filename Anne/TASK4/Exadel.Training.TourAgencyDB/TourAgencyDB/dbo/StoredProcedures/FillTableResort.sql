﻿CREATE PROCEDURE [dbo].[FillTableResort]
	@Name NVarChar(50),
	@Airport Bit,
	@Description NVarChar(MAX),
	@CountryId Int
AS
	INSERT INTO Resort(Name, Airport, Description, CountryId) VALUES (@Name, @Airport, @Description, @CountryId)
