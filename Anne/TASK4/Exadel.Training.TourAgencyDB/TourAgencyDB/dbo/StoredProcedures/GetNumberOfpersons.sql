﻿CREATE PROCEDURE [dbo].[GetNumberOfpersons]
	@checkedHotel NVarChar(50),
	@checkedResort NVarChar(50)
AS
	SELECT DISTINCT NumberOfPersons FROM Tour WHERE HotelId IN ( SELECT Id FROM Hotel WHERE  Name = @checkedHotel AND ResortId IN ( SELECT Id FROM Resort WHERE Name = @checkedResort ) )
