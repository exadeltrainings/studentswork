﻿CREATE PROCEDURE [dbo].[FillTableHotel]
	@Name NVarChar(50),
	@Rating int,
	@TypeOfDiet NVarChar(50),
	@Description NVarChar(MAX),
	@ResortId int
AS
	INSERT INTO Hotel(Name, Rating, TypeOfDiet, Description, ResortId) VALUES (@Name, @Rating, @TypeOfDiet, @Description, @ResortId)
