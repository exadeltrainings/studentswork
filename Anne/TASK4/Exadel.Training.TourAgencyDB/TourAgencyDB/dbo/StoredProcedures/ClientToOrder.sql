﻿CREATE PROCEDURE [dbo].[ClientToOrder]
	@ClientName NVarChar(50),
	@out int output
AS
	SELECT @out = Id FROM Client WHERE Name = @ClientName
