﻿CREATE PROCEDURE [dbo].[FillTableCountries]
	@Name NVarChar(50),
	@VisaCost Money,
	@Currency NVarChar(50),
	@Language NVarChar(50)
AS
	INSERT INTO Country (Name, VisaCost, Currency, Language) VALUES (@Name, @VisaCost, @Currency, @Language)
