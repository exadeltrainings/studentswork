﻿CREATE PROCEDURE [dbo].[ClientData]
	@personId int,
	@Name NVarChar(50) output,
	@PassportNumber NVarChar(50) output,
	@PhoneNumber NVarChar(50) output
AS
	SELECT @Name = Name, @PassportNumber = PassportNumber, @PhoneNumber = PhoneNumber FROM Client WHERE Id = @personId
