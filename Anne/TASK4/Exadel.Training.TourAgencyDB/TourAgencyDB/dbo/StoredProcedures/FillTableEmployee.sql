﻿CREATE PROCEDURE [dbo].[FillTableEmployee]
	@Name NVarChar(50),
	@Post NVarChar(50),
	@PhoneNumber NVarChar(50),
	@DateOfBirth Date,
	@Password NVarChar(50)
AS
	INSERT INTO Employee(Name, Post, PhoneNumber, DateOfBirth, Password) VALUES (@Name, @Post, @PhoneNumber, @DateOfBirth, @Password)
