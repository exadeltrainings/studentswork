﻿CREATE PROCEDURE [dbo].[FillTableOrder]
	@Insurance bit,
	@RegistrationDate Date,
	@ClientId int,
	@TourId int,
	@EmployeeId int
AS
	INSERT INTO  [dbo].[Order](Insurance, RegistrationDate, ClientId, TourId, EmployeeId) VALUES (@Insurance, @RegistrationDate, @ClientId, @TourId, @EmployeeId)
