﻿CREATE PROCEDURE [dbo].[SuitableTours]
	@hotel NVarChar(50),
	@persons int,
	@days int,
	@transport NVarChar(50)
AS
	SELECT TourType, DepartureDate, Cost, Hotel.Name, Resort.Name, NumberOfPersons, NumberOfDays, Transport.Name, Tour.Id FROM Tour, Hotel, Resort, Transport WHERE HotelId = Hotel.Id AND ResortId = Resort.Id AND Transport.Id = TransportId AND Hotel.Name = @hotel AND NumberOfPersons = @persons AND NumberOfDays = @days AND Transport.Name = @transport
