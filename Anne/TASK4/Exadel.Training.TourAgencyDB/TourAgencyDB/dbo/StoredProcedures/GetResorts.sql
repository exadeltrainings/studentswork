﻿CREATE PROCEDURE [dbo].[GetResorts]
	@selectedCountry NVarChar(50)
AS
	SELECT Name FROM Resort WHERE CountryId IN ( SELECT Id FROM Country WHERE Name = @selectedCountry )
