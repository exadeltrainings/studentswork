﻿CREATE PROCEDURE [dbo].[FillTableTour]
	@TourType NVarChar(50),
	@Cost Money,
	@NumberOfPersons int,
	@NumberOfDays int,
	@DepartureDate Date,
	@TransportId int,
	@HotelId int
AS
	INSERT INTO Tour(TourType, Cost, NumberOfPersons, NumberOfDays, DepartureDate, TransportId, HotelId) VALUES (@TourType, @Cost, @NumberOfPersons, @NumberOfDays, @DepartureDate, @TransportId, @HotelId)
