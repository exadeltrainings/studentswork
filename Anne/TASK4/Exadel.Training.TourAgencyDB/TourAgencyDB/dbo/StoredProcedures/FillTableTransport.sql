﻿CREATE PROCEDURE [dbo].[FillTableTransport]
	@Name NVarChar(50),
	@Class NVarChar(50)
AS
	INSERT INTO Transport(Name, Class) VALUES (@Name, @Class)
