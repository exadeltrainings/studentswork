﻿CREATE PROCEDURE [dbo].[RegistrationOfNewClient]
	@Name NVarChar(50),
	@PassportNumber NVarChar(50),
	@Address NVarChar(50),
	@PhoneNumber NVarChar(50),
	@Password NVarChar(50)
AS
	INSERT INTO Client (Name, PassportNumber, Address, PhoneNumber, Password) VALUES (@Name, @PassportNumber, @Address, @PhoneNumber, @Password)
