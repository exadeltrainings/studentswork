﻿using System;

namespace Exadel.Training.TourAgencyDB.Core
{
    public class Tour
    {
        public string TourType { get; set; }
        public int Cost { get; set; }
        public int NumberOfPersons { get; set; }
        public int NumberOfDays { get; set; }
        public DateTime DepartureDate { get; set; }
        public int TransportId { get; set; }
        public int HotelId { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            TourType = parts[0];
            Cost = Int32.Parse(parts[1]);
            NumberOfPersons = Int32.Parse(parts[2]);
            NumberOfDays = Int32.Parse(parts[3]);
            DepartureDate = DateTime.Parse(parts[4]);
            TransportId = Int32.Parse(parts[5]);
            HotelId = Int32.Parse(parts[6]);
        }
    }
}
