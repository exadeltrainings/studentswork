﻿using System;

namespace Exadel.Training.TourAgencyDB.Core
{
    public class Country
    {
        public string Name { get; set; }
        public int VisaCost { get; set; }
        public string Currency { get; set; }
        public string Language { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            Name = parts[0];
            VisaCost = Int32.Parse(parts[1]);
            Currency = parts[2];
            Language = parts[3];
        }
    }
}
