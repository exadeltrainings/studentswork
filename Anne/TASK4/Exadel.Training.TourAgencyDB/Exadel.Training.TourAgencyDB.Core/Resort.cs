﻿using System;

namespace Exadel.Training.TourAgencyDB.Core
{
    public class Resort
    {
        public string Name { get; set; }
        public bool Airport { get; set; }
        public string Description { get; set; }
        public int CountryId { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            Name = parts[0];
            Airport = Boolean.Parse(parts[1]);
            Description = parts[2];
            CountryId = Int32.Parse(parts[3]);
        }
    }
}
