﻿namespace Exadel.Training.TourAgencyDB.Core
{
    public class Transport
    {
        public string Name { get; set; }
        public string Class { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            Name = parts[0];
            Class = parts[1];
        }
    }
}
