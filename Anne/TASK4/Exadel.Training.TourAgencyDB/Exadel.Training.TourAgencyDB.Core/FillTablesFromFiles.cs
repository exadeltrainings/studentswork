﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Exadel.Training.TourAgencyDB.Core
{
    public static class FillTablesFromFiles
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TourAgencyConnectionString"].ConnectionString;

        public static void ClientRegistration(string name, string passport, string address, string number, string password)
        {
            // STORED PROCEDURE -- RegistrationOfNewClient

            SqlConnection connection = new SqlConnection(connectionString);
            using (connection)
            {
                connection.Open();

                var sqlCmd = new SqlCommand("RegistrationOfNewClient", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@Name";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = name;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@PassportNumber";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = passport;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Address";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = address;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@PhoneNumber";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = number;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Password";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = password;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        public static void FillTables()
        {
            List<Country> infoListCountry = new List<Country>();
            infoListCountry = FileReader.ReadFileCountry(@"..\..\DataFiles\country.csv");
            List<Client> infoListClient = new List<Client>();
            infoListClient = FileReader.ReadFileClient(@"..\..\DataFiles\client.csv");
            List<Employee> infoListEmployee = new List<Employee>();
            infoListEmployee = FileReader.ReadFileEmployee(@"..\..\DataFiles\employee.csv");
            List<Hotel> infoListHotel = new List<Hotel>();
            infoListHotel = FileReader.ReadFileHotel(@"..\..\DataFiles\hotel.csv");
            List<Resort> infoListResort = new List<Resort>();
            infoListResort = FileReader.ReadFileResort(@"..\..\DataFiles\resort.csv");
            List<Order> infoListOrder = new List<Order>();
            infoListOrder = FileReader.ReadFileOrder(@"..\..\DataFiles\order.csv");
            List<Tour> infoListTour = new List<Tour>();
            infoListTour = FileReader.ReadFileTour(@"..\..\DataFiles\tour.csv");
            List<Transport> infoListTransport = new List<Transport>();
            infoListTransport = FileReader.ReadFileTransport(@"..\..\DataFiles\transport.csv");

            // Fill COUNTRY table
            /*for (int i = 0; i < infoListCountry.Count; i++)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("FillTableCountries", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListCountry[i].Name;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@VisaCost";
                    param.SqlDbType = SqlDbType.Money;
                    param.Value = infoListCountry[i].VisaCost;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Currency";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListCountry[i].Currency;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Language";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListCountry[i].Language;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }
            }
            */
            // Fill RESORT table -- FillTableResort

            for (int i = 0; i < infoListResort.Count; i++)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("FillTableResort", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListResort[i].Name;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Airport";
                    param.SqlDbType = SqlDbType.Bit;
                    param.Value = infoListResort[i].Airport;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Description";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 250;
                    param.Value = infoListResort[i].Description;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@CountryId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListResort[i].CountryId;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }
            }

            // Fill HOTEL table -- FillTableHotel

            for (int i = 0; i < infoListHotel.Count; i++)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("FillTableHotel", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListHotel[i].Name;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Rating";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListHotel[i].Rating;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@TypeOfDiet";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListHotel[i].TypeOfDiet;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Description";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 250;
                    param.Value = infoListHotel[i].Descriprion;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@ResortId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListHotel[i].ResortId;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }
            }

            // Fill TRANSPORT table -- FillTableTransport

            for (int i = 0; i < infoListTransport.Count; i++)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("FillTableTransport", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListTransport[i].Name;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Class";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListTransport[i].Class;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }
            }

            // Fill TOUR table -- FillTableTour

            for (int i = 0; i < infoListTour.Count; i++)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("FillTableTour", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@TourType";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListTour[i].TourType;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Cost";
                    param.SqlDbType = SqlDbType.Money;
                    param.Value = infoListTour[i].Cost;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@NumberOfPersons";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListTour[i].NumberOfPersons;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@NumberOfDays";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListTour[i].NumberOfDays;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@DepartureDate";
                    param.SqlDbType = SqlDbType.Date;
                    param.Value = infoListTour[i].DepartureDate;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@TransportId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListTour[i].TransportId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@HotelId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListTour[i].HotelId;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }
            }

            // Fill CLIENT table -- RegistrationOfNewClient

            for (int i = 0; i < infoListClient.Count; i++)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("RegistrationOfNewClient", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListClient[i].Name;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@PassportNumber";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListClient[i].Passport;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Address";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListClient[i].Address;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@PhoneNumber";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListClient[i].Phone;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Password";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListClient[i].Password;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }
            }

            // Fill EMPLOYEE table -- FillTableEmployee

            for (int i = 0; i < infoListEmployee.Count; i++)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("FillTableEmployee", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListEmployee[i].Name;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Post";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListEmployee[i].Post;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@PhoneNumber";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListEmployee[i].Phone;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@DateOfBirth";
                    param.SqlDbType = SqlDbType.Date;
                    param.Value = infoListEmployee[i].DateOfBirth;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Password";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = infoListEmployee[i].Password;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }
            }

            // Fill ORDER table --FillTableOrder

            for (int i = 0; i < infoListOrder.Count; i++)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("FillTableOrder", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Insurance";
                    param.SqlDbType = SqlDbType.Bit;
                    param.Value = infoListOrder[i].Insurance;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@RegistrationDate";
                    param.SqlDbType = SqlDbType.Date;
                    param.Value = infoListOrder[i].RegistrationDate;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@ClientId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListOrder[i].ClientId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@TourId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListOrder[i].TourId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@EmployeeId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = infoListOrder[i].EmployeeId;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }
            }

        }

    }
}
