﻿using System;

namespace Exadel.Training.TourAgencyDB.Core
{
    public class Order
    {
        public bool Insurance { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int ClientId { get; set; }
        public int TourId { get; set; }
        public int EmployeeId { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            Insurance = Boolean.Parse(parts[0]);
            RegistrationDate = DateTime.Parse(parts[1]);
            ClientId = Int32.Parse(parts[2]);
            TourId = Int32.Parse(parts[3]);
            EmployeeId = Int32.Parse(parts[4]);
        }
    }
}
