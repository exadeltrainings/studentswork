﻿using System.Collections.Generic;
using System.IO;

namespace Exadel.Training.TourAgencyDB.Core
{
    public class FileReader
    {
        /// <summary>
        /// Read data about countries from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<Country> ReadFileCountry(string filename)
        {
            List<Country> res = new List<Country>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Country obj = new Country();
                    obj.piece(line);
                    res.Add(obj);
                }
            }
            return res;
        }

        /// <summary>
        /// Read data about clients from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<Client> ReadFileClient(string filename)
        {
            List<Client> res = new List<Client>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Client p = new Client();
                    p.piece(line);
                    res.Add(p);
                }
            }
            return res;
        }

        /// <summary>
        /// Read data about employees from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<Employee> ReadFileEmployee(string filename)
        {
            List<Employee> res = new List<Employee>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Employee p = new Employee();
                    p.piece(line);
                    res.Add(p);
                }
            }
            return res;
        }

        /// <summary>
        /// Read data about hotels from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<Hotel> ReadFileHotel(string filename)
        {
            List<Hotel> res = new List<Hotel>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Hotel obj = new Hotel();
                    obj.piece(line);
                    res.Add(obj);
                }
            }
            return res;
        }

        /// <summary>
        /// Read data about orders from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<Order> ReadFileOrder(string filename)
        {
            List<Order> res = new List<Order>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Order p = new Order();
                    p.piece(line);
                    res.Add(p);
                }
            }
            return res;
        }


        /// <summary>
        /// Read data about resorts from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<Resort> ReadFileResort(string filename)
        {
            List<Resort> res = new List<Resort>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Resort p = new Resort();
                    p.piece(line);
                    res.Add(p);
                }
            }
            return res;
        }

        /// <summary>
        /// Read data about tours from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<Tour> ReadFileTour(string filename)
        {
            List<Tour> res = new List<Tour>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Tour p = new Tour();
                    p.piece(line);
                    res.Add(p);
                }
            }
            return res;
        }

        /// <summary>
        /// Read data about transport from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<Transport> ReadFileTransport(string filename)
        {
            List<Transport> res = new List<Transport>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Transport p = new Transport();
                    p.piece(line);
                    res.Add(p);
                }
            }
            return res;
        }
    }
}
