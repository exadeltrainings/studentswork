﻿using System;

namespace Exadel.Training.TourAgencyDB.Core
{
    public class Employee
    {
        public string Name { get; set; }
        public string Post { get; set; }
        public string Phone { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Password { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            Name = parts[0];
            Post = parts[1];
            Phone = parts[2];
            DateOfBirth = DateTime.Parse(parts[3]);
            Password = parts[4];
        }
    }
}
