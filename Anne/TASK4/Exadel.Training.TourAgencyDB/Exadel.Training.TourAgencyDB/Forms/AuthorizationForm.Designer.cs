﻿namespace Exadel.Training.TourAgencyDB
{
    partial class AuthorizationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthorizationForm));
            this.tourAgencyDataSet = new Exadel.Training.TourAgencyDB.TourAgencyDataSet();
            this.label6 = new System.Windows.Forms.Label();
            this.RegistrationSubmitButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PhoneTextBox = new System.Windows.Forms.TextBox();
            this.AddressTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PassportTextBox = new System.Windows.Forms.TextBox();
            this.RegistrationButton = new System.Windows.Forms.Button();
            this.EmployeeSubmitButton = new System.Windows.Forms.Button();
            this.ClientSubmitButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.EmployeeButton = new System.Windows.Forms.Button();
            this.ClientButton = new System.Windows.Forms.Button();
            this.RestartButton = new System.Windows.Forms.Button();
            this.LoginButton = new System.Windows.Forms.Button();
            this.JustLookButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tourAgencyDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // tourAgencyDataSet
            // 
            this.tourAgencyDataSet.DataSetName = "TourAgencyDataSet";
            this.tourAgencyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(222, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 26);
            this.label6.TabIndex = 42;
            this.label6.Text = "Welcome";
            // 
            // RegistrationSubmitButton
            // 
            this.RegistrationSubmitButton.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RegistrationSubmitButton.Location = new System.Drawing.Point(227, 219);
            this.RegistrationSubmitButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.RegistrationSubmitButton.Name = "RegistrationSubmitButton";
            this.RegistrationSubmitButton.Size = new System.Drawing.Size(80, 28);
            this.RegistrationSubmitButton.TabIndex = 41;
            this.RegistrationSubmitButton.Text = "Submit";
            this.RegistrationSubmitButton.UseVisualStyleBackColor = true;
            this.RegistrationSubmitButton.Visible = false;
            this.RegistrationSubmitButton.Click += new System.EventHandler(this.RegistrationSubmitButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(71, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 23);
            this.label5.TabIndex = 40;
            this.label5.Text = "Phone";
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(378, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 23);
            this.label4.TabIndex = 39;
            this.label4.Text = "Address";
            this.label4.Visible = false;
            // 
            // PhoneTextBox
            // 
            this.PhoneTextBox.Location = new System.Drawing.Point(75, 187);
            this.PhoneTextBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PhoneTextBox.Name = "PhoneTextBox";
            this.PhoneTextBox.Size = new System.Drawing.Size(131, 22);
            this.PhoneTextBox.TabIndex = 38;
            this.PhoneTextBox.Visible = false;
            // 
            // AddressTextBox
            // 
            this.AddressTextBox.Location = new System.Drawing.Point(382, 125);
            this.AddressTextBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.AddressTextBox.Name = "AddressTextBox";
            this.AddressTextBox.Size = new System.Drawing.Size(131, 22);
            this.AddressTextBox.TabIndex = 37;
            this.AddressTextBox.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(71, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 23);
            this.label3.TabIndex = 36;
            this.label3.Text = "Passport";
            this.label3.Visible = false;
            // 
            // PassportTextBox
            // 
            this.PassportTextBox.Location = new System.Drawing.Point(75, 125);
            this.PassportTextBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PassportTextBox.Name = "PassportTextBox";
            this.PassportTextBox.Size = new System.Drawing.Size(131, 22);
            this.PassportTextBox.TabIndex = 35;
            this.PassportTextBox.Visible = false;
            // 
            // RegistrationButton
            // 
            this.RegistrationButton.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RegistrationButton.Location = new System.Drawing.Point(200, 96);
            this.RegistrationButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.RegistrationButton.Name = "RegistrationButton";
            this.RegistrationButton.Size = new System.Drawing.Size(131, 28);
            this.RegistrationButton.TabIndex = 34;
            this.RegistrationButton.Text = "Registration";
            this.RegistrationButton.UseVisualStyleBackColor = true;
            this.RegistrationButton.Click += new System.EventHandler(this.RegistrationButton_Click);
            // 
            // EmployeeSubmitButton
            // 
            this.EmployeeSubmitButton.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EmployeeSubmitButton.Location = new System.Drawing.Point(227, 219);
            this.EmployeeSubmitButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.EmployeeSubmitButton.Name = "EmployeeSubmitButton";
            this.EmployeeSubmitButton.Size = new System.Drawing.Size(80, 28);
            this.EmployeeSubmitButton.TabIndex = 33;
            this.EmployeeSubmitButton.Text = "Submit";
            this.EmployeeSubmitButton.UseVisualStyleBackColor = true;
            this.EmployeeSubmitButton.Visible = false;
            this.EmployeeSubmitButton.Click += new System.EventHandler(this.EmployeeSubmitButton_Click);
            // 
            // ClientSubmitButton
            // 
            this.ClientSubmitButton.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientSubmitButton.Location = new System.Drawing.Point(227, 219);
            this.ClientSubmitButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ClientSubmitButton.Name = "ClientSubmitButton";
            this.ClientSubmitButton.Size = new System.Drawing.Size(80, 28);
            this.ClientSubmitButton.TabIndex = 32;
            this.ClientSubmitButton.Text = "Submit";
            this.ClientSubmitButton.UseVisualStyleBackColor = true;
            this.ClientSubmitButton.Visible = false;
            this.ClientSubmitButton.Click += new System.EventHandler(this.ClientSubmitButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(223, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 23);
            this.label2.TabIndex = 31;
            this.label2.Text = "Password";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(223, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 23);
            this.label1.TabIndex = 30;
            this.label1.Text = "Name";
            this.label1.Visible = false;
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Location = new System.Drawing.Point(227, 187);
            this.PasswordTextBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.Size = new System.Drawing.Size(131, 22);
            this.PasswordTextBox.TabIndex = 29;
            this.PasswordTextBox.Visible = false;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(227, 125);
            this.NameTextBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(131, 22);
            this.NameTextBox.TabIndex = 28;
            this.NameTextBox.Visible = false;
            // 
            // EmployeeButton
            // 
            this.EmployeeButton.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EmployeeButton.Location = new System.Drawing.Point(132, 57);
            this.EmployeeButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.EmployeeButton.Name = "EmployeeButton";
            this.EmployeeButton.Size = new System.Drawing.Size(120, 28);
            this.EmployeeButton.TabIndex = 27;
            this.EmployeeButton.Text = "I\'m an employee";
            this.EmployeeButton.UseVisualStyleBackColor = true;
            this.EmployeeButton.Visible = false;
            this.EmployeeButton.Click += new System.EventHandler(this.EmployeeButton_Click);
            // 
            // ClientButton
            // 
            this.ClientButton.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientButton.Location = new System.Drawing.Point(281, 57);
            this.ClientButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ClientButton.Name = "ClientButton";
            this.ClientButton.Size = new System.Drawing.Size(120, 29);
            this.ClientButton.TabIndex = 26;
            this.ClientButton.Text = "I\'m a client";
            this.ClientButton.UseVisualStyleBackColor = true;
            this.ClientButton.Visible = false;
            this.ClientButton.Click += new System.EventHandler(this.ClientButton_Click);
            // 
            // RestartButton
            // 
            this.RestartButton.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestartButton.Location = new System.Drawing.Point(493, 219);
            this.RestartButton.Name = "RestartButton";
            this.RestartButton.Size = new System.Drawing.Size(51, 30);
            this.RestartButton.TabIndex = 43;
            this.RestartButton.Text = "Restart";
            this.RestartButton.UseVisualStyleBackColor = true;
            this.RestartButton.Click += new System.EventHandler(this.RestartButton_Click);
            // 
            // LoginButton
            // 
            this.LoginButton.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginButton.Location = new System.Drawing.Point(200, 57);
            this.LoginButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(131, 28);
            this.LoginButton.TabIndex = 44;
            this.LoginButton.Text = "Log In";
            this.LoginButton.UseVisualStyleBackColor = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // JustLookButton
            // 
            this.JustLookButton.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.JustLookButton.Location = new System.Drawing.Point(200, 134);
            this.JustLookButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.JustLookButton.Name = "JustLookButton";
            this.JustLookButton.Size = new System.Drawing.Size(131, 28);
            this.JustLookButton.TabIndex = 45;
            this.JustLookButton.Text = "Just look";
            this.JustLookButton.UseVisualStyleBackColor = true;
            this.JustLookButton.Click += new System.EventHandler(this.JustLookButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(152, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(249, 26);
            this.label7.TabIndex = 46;
            this.label7.Text = "To the registration process";
            this.label7.Visible = false;
            // 
            // AuthorizationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 259);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.JustLookButton);
            this.Controls.Add(this.LoginButton);
            this.Controls.Add(this.RestartButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.RegistrationSubmitButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PhoneTextBox);
            this.Controls.Add(this.AddressTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PassportTextBox);
            this.Controls.Add(this.RegistrationButton);
            this.Controls.Add(this.EmployeeSubmitButton);
            this.Controls.Add(this.ClientSubmitButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PasswordTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.EmployeeButton);
            this.Controls.Add(this.ClientButton);
            this.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AuthorizationForm";
            this.Text = "AuthorizationForm";
            this.Load += new System.EventHandler(this.AuthorizationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tourAgencyDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private TourAgencyDataSet tourAgencyDataSet;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button RegistrationSubmitButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox PhoneTextBox;
        private System.Windows.Forms.TextBox AddressTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PassportTextBox;
        private System.Windows.Forms.Button RegistrationButton;
        private System.Windows.Forms.Button EmployeeSubmitButton;
        private System.Windows.Forms.Button ClientSubmitButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Button EmployeeButton;
        private System.Windows.Forms.Button ClientButton;
        private System.Windows.Forms.Button RestartButton;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.Button JustLookButton;
        private System.Windows.Forms.Label label7;
    }
}

