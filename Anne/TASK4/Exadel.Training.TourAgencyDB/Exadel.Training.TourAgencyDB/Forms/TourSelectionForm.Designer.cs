﻿namespace Exadel.Training.TourAgencyDB.Forms
{
    partial class TourSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TourSelectionForm));
            this.InfoButton = new System.Windows.Forms.Button();
            this.ShowAvailableToursButton = new System.Windows.Forms.Button();
            this.ShowTourInfo = new System.Windows.Forms.Button();
            this.TypeOfDietComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.RatingComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.NumberOfDaysComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.NumberOfPersonsComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TransportComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.HotelGroupBox = new System.Windows.Forms.GroupBox();
            this.HotelPanel = new System.Windows.Forms.Panel();
            this.ShowHotelsButton = new System.Windows.Forms.Button();
            this.ResortGroupBox = new System.Windows.Forms.GroupBox();
            this.ResortPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.CountryComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.HotelGroupBox.SuspendLayout();
            this.ResortGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // InfoButton
            // 
            this.InfoButton.Enabled = false;
            this.InfoButton.Location = new System.Drawing.Point(612, 316);
            this.InfoButton.Name = "InfoButton";
            this.InfoButton.Size = new System.Drawing.Size(75, 25);
            this.InfoButton.TabIndex = 40;
            this.InfoButton.Text = "Info";
            this.InfoButton.UseVisualStyleBackColor = true;
            this.InfoButton.Click += new System.EventHandler(this.InfoButton_Click);
            // 
            // ShowAvailableToursButton
            // 
            this.ShowAvailableToursButton.Enabled = false;
            this.ShowAvailableToursButton.Location = new System.Drawing.Point(28, 318);
            this.ShowAvailableToursButton.Name = "ShowAvailableToursButton";
            this.ShowAvailableToursButton.Size = new System.Drawing.Size(75, 25);
            this.ShowAvailableToursButton.TabIndex = 39;
            this.ShowAvailableToursButton.Text = "Show";
            this.ShowAvailableToursButton.UseVisualStyleBackColor = true;
            this.ShowAvailableToursButton.Click += new System.EventHandler(this.ShowAvailableToursButton_Click);
            // 
            // ShowTourInfo
            // 
            this.ShowTourInfo.Enabled = false;
            this.ShowTourInfo.Location = new System.Drawing.Point(612, 278);
            this.ShowTourInfo.Name = "ShowTourInfo";
            this.ShowTourInfo.Size = new System.Drawing.Size(75, 25);
            this.ShowTourInfo.TabIndex = 38;
            this.ShowTourInfo.Text = "Tour";
            this.ShowTourInfo.UseVisualStyleBackColor = true;
            this.ShowTourInfo.Click += new System.EventHandler(this.ShowTourInfo_Click);
            // 
            // TypeOfDietComboBox
            // 
            this.TypeOfDietComboBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TypeOfDietComboBox.FormattingEnabled = true;
            this.TypeOfDietComboBox.Location = new System.Drawing.Point(531, 230);
            this.TypeOfDietComboBox.Name = "TypeOfDietComboBox";
            this.TypeOfDietComboBox.Size = new System.Drawing.Size(152, 26);
            this.TypeOfDietComboBox.TabIndex = 37;
            this.TypeOfDietComboBox.Text = "Select hotel\'s type of diet";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(527, 206);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 21);
            this.label9.TabIndex = 36;
            this.label9.Text = "Type of diet:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(527, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 21);
            this.label8.TabIndex = 35;
            this.label8.Text = "Rating:";
            // 
            // RatingComboBox
            // 
            this.RatingComboBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RatingComboBox.FormattingEnabled = true;
            this.RatingComboBox.Location = new System.Drawing.Point(531, 172);
            this.RatingComboBox.Name = "RatingComboBox";
            this.RatingComboBox.Size = new System.Drawing.Size(152, 26);
            this.RatingComboBox.TabIndex = 34;
            this.RatingComboBox.Text = "Select hotel\'s rating";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(527, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 21);
            this.label7.TabIndex = 33;
            this.label7.Text = "Number of days:";
            // 
            // NumberOfDaysComboBox
            // 
            this.NumberOfDaysComboBox.Enabled = false;
            this.NumberOfDaysComboBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumberOfDaysComboBox.FormattingEnabled = true;
            this.NumberOfDaysComboBox.Location = new System.Drawing.Point(531, 75);
            this.NumberOfDaysComboBox.Name = "NumberOfDaysComboBox";
            this.NumberOfDaysComboBox.Size = new System.Drawing.Size(152, 26);
            this.NumberOfDaysComboBox.TabIndex = 32;
            this.NumberOfDaysComboBox.Text = "Select number of days";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(349, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 21);
            this.label6.TabIndex = 31;
            this.label6.Text = "Number of persons:";
            // 
            // NumberOfPersonsComboBox
            // 
            this.NumberOfPersonsComboBox.Enabled = false;
            this.NumberOfPersonsComboBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumberOfPersonsComboBox.FormattingEnabled = true;
            this.NumberOfPersonsComboBox.Location = new System.Drawing.Point(353, 75);
            this.NumberOfPersonsComboBox.Name = "NumberOfPersonsComboBox";
            this.NumberOfPersonsComboBox.Size = new System.Drawing.Size(152, 26);
            this.NumberOfPersonsComboBox.TabIndex = 30;
            this.NumberOfPersonsComboBox.Text = "Select number of persons";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(185, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 21);
            this.label5.TabIndex = 29;
            this.label5.Text = "Transport:";
            // 
            // TransportComboBox
            // 
            this.TransportComboBox.Enabled = false;
            this.TransportComboBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TransportComboBox.FormattingEnabled = true;
            this.TransportComboBox.Location = new System.Drawing.Point(189, 75);
            this.TransportComboBox.Name = "TransportComboBox";
            this.TransportComboBox.Size = new System.Drawing.Size(137, 26);
            this.TransportComboBox.TabIndex = 28;
            this.TransportComboBox.Text = "Select transport";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(278, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 21);
            this.label4.TabIndex = 27;
            this.label4.Text = "Hotel:";
            // 
            // HotelGroupBox
            // 
            this.HotelGroupBox.Controls.Add(this.HotelPanel);
            this.HotelGroupBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HotelGroupBox.Location = new System.Drawing.Point(282, 145);
            this.HotelGroupBox.Name = "HotelGroupBox";
            this.HotelGroupBox.Size = new System.Drawing.Size(229, 158);
            this.HotelGroupBox.TabIndex = 26;
            this.HotelGroupBox.TabStop = false;
            this.HotelGroupBox.Text = "Select hotel";
            // 
            // HotelPanel
            // 
            this.HotelPanel.AutoScroll = true;
            this.HotelPanel.Location = new System.Drawing.Point(6, 17);
            this.HotelPanel.Name = "HotelPanel";
            this.HotelPanel.Size = new System.Drawing.Size(217, 135);
            this.HotelPanel.TabIndex = 0;
            // 
            // ShowHotelsButton
            // 
            this.ShowHotelsButton.Location = new System.Drawing.Point(531, 278);
            this.ShowHotelsButton.Name = "ShowHotelsButton";
            this.ShowHotelsButton.Size = new System.Drawing.Size(75, 25);
            this.ShowHotelsButton.TabIndex = 25;
            this.ShowHotelsButton.Text = "Hotels";
            this.ShowHotelsButton.UseVisualStyleBackColor = true;
            this.ShowHotelsButton.Click += new System.EventHandler(this.ShowHotelsButton_Click);
            // 
            // ResortGroupBox
            // 
            this.ResortGroupBox.Controls.Add(this.ResortPanel);
            this.ResortGroupBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResortGroupBox.Location = new System.Drawing.Point(28, 145);
            this.ResortGroupBox.Name = "ResortGroupBox";
            this.ResortGroupBox.Size = new System.Drawing.Size(229, 158);
            this.ResortGroupBox.TabIndex = 24;
            this.ResortGroupBox.TabStop = false;
            this.ResortGroupBox.Text = "Select resort";
            // 
            // ResortPanel
            // 
            this.ResortPanel.AutoScroll = true;
            this.ResortPanel.Location = new System.Drawing.Point(6, 17);
            this.ResortPanel.Name = "ResortPanel";
            this.ResortPanel.Size = new System.Drawing.Size(217, 135);
            this.ResortPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(24, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 21);
            this.label3.TabIndex = 23;
            this.label3.Text = "Resort:";
            // 
            // CountryComboBox
            // 
            this.CountryComboBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CountryComboBox.FormattingEnabled = true;
            this.CountryComboBox.Location = new System.Drawing.Point(27, 75);
            this.CountryComboBox.Name = "CountryComboBox";
            this.CountryComboBox.Size = new System.Drawing.Size(137, 26);
            this.CountryComboBox.TabIndex = 22;
            this.CountryComboBox.Text = "Select country";
            this.CountryComboBox.SelectedIndexChanged += new System.EventHandler(this.CountryComboBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(24, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 21);
            this.label2.TabIndex = 21;
            this.label2.Text = "Country:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(22, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 27);
            this.label1.TabIndex = 20;
            this.label1.Text = "Tour selection";
            // 
            // TourSelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 353);
            this.Controls.Add(this.InfoButton);
            this.Controls.Add(this.ShowAvailableToursButton);
            this.Controls.Add(this.ShowTourInfo);
            this.Controls.Add(this.TypeOfDietComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.RatingComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.NumberOfDaysComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.NumberOfPersonsComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TransportComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.HotelGroupBox);
            this.Controls.Add(this.ShowHotelsButton);
            this.Controls.Add(this.ResortGroupBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CountryComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "TourSelectionForm";
            this.Text = "TourSelectionForm";
            this.Load += new System.EventHandler(this.TourSelectionForm_Load);
            this.HotelGroupBox.ResumeLayout(false);
            this.ResortGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button InfoButton;
        private System.Windows.Forms.Button ShowAvailableToursButton;
        private System.Windows.Forms.Button ShowTourInfo;
        private System.Windows.Forms.ComboBox TypeOfDietComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox RatingComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox NumberOfDaysComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox NumberOfPersonsComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox TransportComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox HotelGroupBox;
        private System.Windows.Forms.Panel HotelPanel;
        private System.Windows.Forms.Button ShowHotelsButton;
        private System.Windows.Forms.GroupBox ResortGroupBox;
        private System.Windows.Forms.Panel ResortPanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CountryComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}