﻿namespace Exadel.Training.TourAgencyDB.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.tourSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderMakingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OrdersTextBox = new System.Windows.Forms.TextBox();
            this.PhoneTextBox = new System.Windows.Forms.TextBox();
            this.PassportOrPostTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.OrdersLabel = new System.Windows.Forms.Label();
            this.PhoneLabel = new System.Windows.Forms.Label();
            this.PassportOrPostLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.PersonalDataLabel = new System.Windows.Forms.Label();
            this.GuestInfoLabel = new System.Windows.Forms.Label();
            this.RegisterButton = new System.Windows.Forms.Button();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.BackColor = System.Drawing.Color.SeaShell;
            this.MainMenu.Font = new System.Drawing.Font("Palatino Linotype", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tourSelectionToolStripMenuItem,
            this.orderMakingToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Padding = new System.Windows.Forms.Padding(10, 3, 0, 3);
            this.MainMenu.Size = new System.Drawing.Size(390, 33);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "menuStrip1";
            // 
            // tourSelectionToolStripMenuItem
            // 
            this.tourSelectionToolStripMenuItem.Name = "tourSelectionToolStripMenuItem";
            this.tourSelectionToolStripMenuItem.Size = new System.Drawing.Size(130, 27);
            this.tourSelectionToolStripMenuItem.Text = "Tour selection";
            this.tourSelectionToolStripMenuItem.Click += new System.EventHandler(this.tourSelectionToolStripMenuItem_Click);
            // 
            // orderMakingToolStripMenuItem
            // 
            this.orderMakingToolStripMenuItem.Name = "orderMakingToolStripMenuItem";
            this.orderMakingToolStripMenuItem.Size = new System.Drawing.Size(169, 27);
            this.orderMakingToolStripMenuItem.Text = "Making of an order";
            this.orderMakingToolStripMenuItem.Click += new System.EventHandler(this.orderMakingToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(52, 27);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // OrdersTextBox
            // 
            this.OrdersTextBox.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OrdersTextBox.Location = new System.Drawing.Point(216, 174);
            this.OrdersTextBox.Name = "OrdersTextBox";
            this.OrdersTextBox.Size = new System.Drawing.Size(162, 24);
            this.OrdersTextBox.TabIndex = 20;
            // 
            // PhoneTextBox
            // 
            this.PhoneTextBox.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhoneTextBox.Location = new System.Drawing.Point(216, 144);
            this.PhoneTextBox.Name = "PhoneTextBox";
            this.PhoneTextBox.Size = new System.Drawing.Size(162, 24);
            this.PhoneTextBox.TabIndex = 19;
            // 
            // PassportOrPostTextBox
            // 
            this.PassportOrPostTextBox.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PassportOrPostTextBox.Location = new System.Drawing.Point(216, 114);
            this.PassportOrPostTextBox.Name = "PassportOrPostTextBox";
            this.PassportOrPostTextBox.Size = new System.Drawing.Size(162, 24);
            this.PassportOrPostTextBox.TabIndex = 18;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameTextBox.Location = new System.Drawing.Point(216, 82);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(162, 24);
            this.NameTextBox.TabIndex = 17;
            // 
            // OrdersLabel
            // 
            this.OrdersLabel.AutoSize = true;
            this.OrdersLabel.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OrdersLabel.Location = new System.Drawing.Point(12, 179);
            this.OrdersLabel.Name = "OrdersLabel";
            this.OrdersLabel.Size = new System.Drawing.Size(129, 23);
            this.OrdersLabel.TabIndex = 16;
            this.OrdersLabel.Text = "Amount of orders:";
            // 
            // PhoneLabel
            // 
            this.PhoneLabel.AutoSize = true;
            this.PhoneLabel.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhoneLabel.Location = new System.Drawing.Point(12, 147);
            this.PhoneLabel.Name = "PhoneLabel";
            this.PhoneLabel.Size = new System.Drawing.Size(108, 23);
            this.PhoneLabel.TabIndex = 15;
            this.PhoneLabel.Text = "Phone number:";
            // 
            // PassportOrPostLabel
            // 
            this.PassportOrPostLabel.AutoSize = true;
            this.PassportOrPostLabel.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PassportOrPostLabel.Location = new System.Drawing.Point(12, 115);
            this.PassportOrPostLabel.Name = "PassportOrPostLabel";
            this.PassportOrPostLabel.Size = new System.Drawing.Size(69, 23);
            this.PassportOrPostLabel.TabIndex = 14;
            this.PassportOrPostLabel.Text = "Passport:";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameLabel.Location = new System.Drawing.Point(12, 83);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(81, 23);
            this.NameLabel.TabIndex = 13;
            this.NameLabel.Text = "Full name:";
            // 
            // PersonalDataLabel
            // 
            this.PersonalDataLabel.AutoSize = true;
            this.PersonalDataLabel.Font = new System.Drawing.Font("Palatino Linotype", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PersonalDataLabel.Location = new System.Drawing.Point(12, 44);
            this.PersonalDataLabel.Name = "PersonalDataLabel";
            this.PersonalDataLabel.Size = new System.Drawing.Size(117, 23);
            this.PersonalDataLabel.TabIndex = 12;
            this.PersonalDataLabel.Text = "Personal data:";
            // 
            // GuestInfoLabel
            // 
            this.GuestInfoLabel.AutoSize = true;
            this.GuestInfoLabel.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GuestInfoLabel.Location = new System.Drawing.Point(12, 44);
            this.GuestInfoLabel.Name = "GuestInfoLabel";
            this.GuestInfoLabel.Size = new System.Drawing.Size(382, 138);
            this.GuestInfoLabel.TabIndex = 21;
            this.GuestInfoLabel.Text = resources.GetString("GuestInfoLabel.Text");
            this.GuestInfoLabel.Visible = false;
            // 
            // RegisterButton
            // 
            this.RegisterButton.Location = new System.Drawing.Point(12, 185);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.Size = new System.Drawing.Size(75, 23);
            this.RegisterButton.TabIndex = 22;
            this.RegisterButton.Text = "Register";
            this.RegisterButton.UseVisualStyleBackColor = true;
            this.RegisterButton.Visible = false;
            this.RegisterButton.Click += new System.EventHandler(this.RegisterButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 213);
            this.Controls.Add(this.RegisterButton);
            this.Controls.Add(this.GuestInfoLabel);
            this.Controls.Add(this.OrdersTextBox);
            this.Controls.Add(this.PhoneTextBox);
            this.Controls.Add(this.PassportOrPostTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.OrdersLabel);
            this.Controls.Add(this.PhoneLabel);
            this.Controls.Add(this.PassportOrPostLabel);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.PersonalDataLabel);
            this.Controls.Add(this.MainMenu);
            this.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem tourSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderMakingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox OrdersTextBox;
        private System.Windows.Forms.TextBox PhoneTextBox;
        private System.Windows.Forms.TextBox PassportOrPostTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label OrdersLabel;
        private System.Windows.Forms.Label PhoneLabel;
        private System.Windows.Forms.Label PassportOrPostLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label PersonalDataLabel;
        private System.Windows.Forms.Label GuestInfoLabel;
        private System.Windows.Forms.Button RegisterButton;
    }
}