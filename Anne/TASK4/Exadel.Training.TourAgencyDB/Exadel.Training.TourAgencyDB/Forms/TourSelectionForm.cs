﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Exadel.Training.TourAgencyDB.Forms
{
    public partial class TourSelectionForm : Form
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TourAgencyConnectionString"].ConnectionString;
        int personId;
        string who;

        string selectedCountry, checkedResort, selectedRating, selectedTypeOfDiet, checkedHotel, selectedTransport;
        int selectedNumberOfPersons, selectedNumberOdDays;

        private void CountryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            NumberOfPersonsComboBox.Items.Clear();
            NumberOfPersonsComboBox.Text = "Select number of persons";
            NumberOfPersonsComboBox.Enabled = false;
            NumberOfDaysComboBox.Items.Clear();
            NumberOfDaysComboBox.Text = "Select number of days";
            NumberOfDaysComboBox.Enabled = false;
            TransportComboBox.Items.Clear();
            TransportComboBox.Text = "Select transport";
            TransportComboBox.Enabled = false;

            HotelPanel.Controls.Clear();
            RatingComboBox.Text = "Select hotel's rating";
            TypeOfDietComboBox.Text = "Select hotel's type of diet";

            selectedCountry = CountryComboBox.Text.ToString();

            WorkWithResorts();
        }

        private void InfoButton_Click(object sender, EventArgs e)
        {
            GetResortAndHotelInfo();
        }

        private void ShowHotelsButton_Click(object sender, EventArgs e)
        {
            NumberOfPersonsComboBox.Items.Clear();
            NumberOfPersonsComboBox.Text = "Select number of persons";
            NumberOfPersonsComboBox.Enabled = false;
            NumberOfDaysComboBox.Items.Clear();
            NumberOfDaysComboBox.Text = "Select number of days";
            NumberOfDaysComboBox.Enabled = false;
            TransportComboBox.Items.Clear();
            TransportComboBox.Text = "Select transport";
            TransportComboBox.Enabled = false;

            selectedRating = RatingComboBox.Text.ToString();
            selectedTypeOfDiet = TypeOfDietComboBox.Text.ToString();

            WorkWithHotels();

            ShowTourInfo.Enabled = true;
            InfoButton.Enabled = true;
        }

        private void TourSelectionForm_Load(object sender, EventArgs e)
        {
            // Fill countries comboBox
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("GetCountriesNames", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        CountryComboBox.Items.Add(name);
                    }
                }
            }

            // Fill rating comboBox
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("GetHotelRating", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int rating = dr.GetInt32(0);
                        RatingComboBox.Items.Add(rating);
                    }
                }
            }

            // Fill type of diet comboBox
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("GetTypeOfDiet", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string typeOfDiet = dr.GetString(0);
                        TypeOfDietComboBox.Items.Add(typeOfDiet);
                    }
                }
            }
        }

        private void ShowTourInfo_Click(object sender, EventArgs e)
        {
            ShowAvailableToursButton.Enabled = false;
            NumberOfPersonsComboBox.Items.Clear();
            NumberOfPersonsComboBox.Text = "Select number of persons";
            NumberOfPersonsComboBox.Enabled = false;
            NumberOfDaysComboBox.Items.Clear();
            NumberOfDaysComboBox.Text = "Select number of days";
            NumberOfDaysComboBox.Enabled = false;
            TransportComboBox.Items.Clear();
            TransportComboBox.Text = "Select transport";
            TransportComboBox.Enabled = false;

            var checkedButton = HotelPanel.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            checkedHotel = checkedButton.Text;

            WorkWithTourInfo();
        }

        

        private void ShowAvailableToursButton_Click(object sender, EventArgs e)
        {
            selectedTransport = TransportComboBox.Text;
            selectedNumberOfPersons = Int32.Parse(NumberOfPersonsComboBox.Text);
            selectedNumberOdDays = Int32.Parse(NumberOfDaysComboBox.Text);

            AvailableToursForm toShow = new AvailableToursForm(selectedCountry, selectedTransport, selectedNumberOfPersons, selectedNumberOdDays, checkedHotel, personId, who);
            toShow.Show();
        }

        public TourSelectionForm(int id, string belongTo)
        {
            InitializeComponent();
            personId = id;
            who = belongTo;
        }

        private void WorkWithResorts()
        {
            ResortPanel.Controls.Clear();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                int count = 0;
                connection.Open();

                var sqlCmd = new SqlCommand("GetResorts", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@selectedCountry";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = selectedCountry;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        count++;
                        string name = dr.GetString(0);

                        RadioButton rdo = new RadioButton();
                        rdo.Text = name;
                        rdo.AutoSize = true;
                        rdo.Location = new Point(2, 20 * count);
                        ResortPanel.Controls.Add(rdo);
                    }
                }
            }
        }

        private void GetResortAndHotelInfo()
        {
            var checkedButton = HotelPanel.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            checkedHotel = checkedButton.Text;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("GetResortDescription", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@checkedResort";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = checkedResort;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Descriprion";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 250;
                param.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                MessageBox.Show("Resort:\n" + checkedResort + "\n\n" + ((string)sqlCmd.Parameters["@Descriprion"].Value).Trim());

                connection.Close();
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("GetHotelDescription", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@checkedHotel";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = checkedHotel;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Descriprion";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 250;
                param.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                MessageBox.Show("Hotel:\n" + checkedHotel + "\n\n" + ((string)sqlCmd.Parameters["@Descriprion"].Value).Trim());

                connection.Close();
            }
        }

        private void WorkWithHotels()
        {
            HotelPanel.Controls.Clear();

            var checkedButton = ResortPanel.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            checkedResort = checkedButton.Text;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                int count = 0;
                connection.Open();

                var sqlCmd = new SqlCommand("GetHotels", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@checkedResort";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = checkedResort;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@selectedRating";
                param.SqlDbType = SqlDbType.Int;
                param.Value = selectedRating;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@selectedTypeOfDiet";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = selectedTypeOfDiet;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            count++;
                            string name = dr.GetString(0);

                            RadioButton rdo = new RadioButton();
                            rdo.Text = name;
                            rdo.AutoSize = true;
                            rdo.Location = new Point(2, 20 * count);
                            HotelPanel.Controls.Add(rdo);
                        }
                    }
                    else
                    {
                        MessageBox.Show("There are no hotels with set parameters.");
                    }
                }
            }
        }

        private void WorkWithTourInfo()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("GetNumberOfpersons", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@checkedResort";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = checkedResort;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@checkedHotel";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = checkedHotel;
                    sqlCmd.Parameters.Add(param);

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                int number = dr.GetInt32(0);
                                NumberOfPersonsComboBox.Items.Add(number);
                            }
                            NumberOfPersonsComboBox.Enabled = true;
                            ShowAvailableToursButton.Enabled = true;
                        }
                        else
                        {
                            MessageBox.Show("There are no tours in the hotel, you chose.");
                        }
                    }
                }

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("GetNumberOfDays", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@checkedResort";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = checkedResort;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@checkedHotel";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = checkedHotel;
                    sqlCmd.Parameters.Add(param);

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                int number = dr.GetInt32(0);
                                NumberOfDaysComboBox.Items.Add(number);
                            }
                            NumberOfDaysComboBox.Enabled = true;
                        }
                    }
                }

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("GetTransport", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@checkedHotel";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = checkedHotel;
                    sqlCmd.Parameters.Add(param);

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                string name = dr.GetString(0);
                                TransportComboBox.Items.Add(name);
                            }
                            TransportComboBox.Enabled = true;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("There are no tours in the hotel, you chose.");
            }
            
        }
    }
}
