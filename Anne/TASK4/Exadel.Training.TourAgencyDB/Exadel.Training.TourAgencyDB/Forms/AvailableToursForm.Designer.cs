﻿namespace Exadel.Training.TourAgencyDB.Forms
{
    partial class AvailableToursForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AvailableToursForm));
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.OrderCreation = new System.Windows.Forms.Button();
            this.ToursDataGridView = new System.Windows.Forms.DataGridView();
            this.TourType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Departure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HotelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResortName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PersonsNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DaysNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransportName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdTour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ToursDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(405, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 28);
            this.label1.TabIndex = 2;
            this.label1.Text = "Suitable tours";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 297);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // OrderCreation
            // 
            this.OrderCreation.Location = new System.Drawing.Point(93, 297);
            this.OrderCreation.Name = "OrderCreation";
            this.OrderCreation.Size = new System.Drawing.Size(75, 23);
            this.OrderCreation.TabIndex = 4;
            this.OrderCreation.Text = "Order";
            this.OrderCreation.UseVisualStyleBackColor = true;
            this.OrderCreation.Click += new System.EventHandler(this.OrderCreation_Click);
            // 
            // ToursDataGridView
            // 
            this.ToursDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ToursDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TourType,
            this.Departure,
            this.Cost,
            this.HotelName,
            this.ResortName,
            this.PersonsNumber,
            this.DaysNumber,
            this.TransportName,
            this.IdTour});
            this.ToursDataGridView.Location = new System.Drawing.Point(12, 49);
            this.ToursDataGridView.Name = "ToursDataGridView";
            this.ToursDataGridView.Size = new System.Drawing.Size(936, 242);
            this.ToursDataGridView.TabIndex = 6;
            this.ToursDataGridView.SelectionChanged += new System.EventHandler(this.ToursDataGridView_SelectionChanged);
            // 
            // TourType
            // 
            this.TourType.HeaderText = "Tour type";
            this.TourType.Name = "TourType";
            // 
            // Departure
            // 
            this.Departure.HeaderText = "Departure date";
            this.Departure.Name = "Departure";
            // 
            // Cost
            // 
            this.Cost.HeaderText = "Cost";
            this.Cost.Name = "Cost";
            // 
            // HotelName
            // 
            this.HotelName.HeaderText = "Hotel";
            this.HotelName.Name = "HotelName";
            this.HotelName.Width = 140;
            // 
            // ResortName
            // 
            this.ResortName.HeaderText = "Resort";
            this.ResortName.Name = "ResortName";
            // 
            // PersonsNumber
            // 
            this.PersonsNumber.HeaderText = "Number of persons";
            this.PersonsNumber.Name = "PersonsNumber";
            // 
            // DaysNumber
            // 
            this.DaysNumber.HeaderText = "Number of days";
            this.DaysNumber.Name = "DaysNumber";
            // 
            // TransportName
            // 
            this.TransportName.HeaderText = "Transport";
            this.TransportName.Name = "TransportName";
            // 
            // IdTour
            // 
            this.IdTour.HeaderText = "TourId";
            this.IdTour.Name = "IdTour";
            this.IdTour.Width = 50;
            // 
            // AvailableToursForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 332);
            this.Controls.Add(this.ToursDataGridView);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.OrderCreation);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AvailableToursForm";
            this.Text = "AvailableToursForm";
            this.Load += new System.EventHandler(this.AvailableToursForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ToursDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button OrderCreation;
        private System.Windows.Forms.DataGridView ToursDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn TourType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Departure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn HotelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResortName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonsNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn DaysNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransportName;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdTour;
    }
}