﻿using Exadel.Training.TourAgencyDB.Core;
using System;
using System.Windows.Forms;

namespace Exadel.Training.TourAgencyDB.Forms
{
    public partial class RegistrationForm : Form
    {
        public RegistrationForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RegistrationSubmitButton_Click(object sender, EventArgs e)
        {
            if (NameTextBox.Text == "" || PassportTextBox.Text == "" || AddressTextBox.Text == "" || PhoneTextBox.Text == "" || PasswordTextBox.Text == "")
            {
                MessageBox.Show("Fill all the fileds, please.");
            }
            else
            {
                FillTablesFromFiles.ClientRegistration(NameTextBox.Text, PassportTextBox.Text, AddressTextBox.Text, PhoneTextBox.Text, PasswordTextBox.Text);
                MessageBox.Show("You will be redirected to the login page, where you can log in and use application as a client.");
                Application.Restart();
            }
        }

        private void RegistrationForm_Load(object sender, EventArgs e)
        {
            PasswordTextBox.PasswordChar = '*';
        }
    }
}
